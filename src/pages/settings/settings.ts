import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';

import { TranslateService } from '@ngx-translate/core';

import { RemoteProvider } from '../../providers/remote/remote';

import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  public languages: Array<{ name: string, code: string }>;
  public selectedLang: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public translate: TranslateService,
    public modalCtrl: ModalController,
    private remote: RemoteProvider,
    private toast: Toast) {
    this.languages = [{ name: 'English', code: 'en' }, { name: 'French', code: 'fr' }];
  }

  changeLanguage(selectedLanguage: any) {
    console.log('LANG:', selectedLanguage);
    this.translate.use(selectedLanguage);
    this.selectedLang = selectedLanguage;
  }

  openModalChangePassword() {
    var modalPage = this.modalCtrl.create('ChangePasswordPage');
    modalPage.onDidDismiss(data => {
      console.log('password changed', data);
      this.remote.changePassword(data).then((data) => {
        this.toast.show(`Password changed successfully`, '3000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
      }).catch(e => console.log(e));
    });
    modalPage.present();
  }

}
