import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';
import { UserProvider } from '../../providers/user/user';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  public registerForm: FormGroup;
  public email: string;
  public password: string;
  public passwordConfirmation: string;
  public firstname: string;
  public lastname: string;
  public birthdate: string;
  public gender: string;
  public language: string;
  public condition: string;

  constructor(public navCtrl: NavController,
    private sqlite: SQLite,
    public navParams: NavParams,
    public fb: FormBuilder,
    public dbProvider: DatabaseProvider,
    public userProvider: UserProvider,
    public toastCtrl: ToastController) {

    this.registerForm = fb.group({
      "email": [this.email, Validators.required],
      "password": [this.password, Validators.required],
      "passwordConfirmation": [this.passwordConfirmation, Validators.required],
      "firstname": [this.firstname, Validators.compose([Validators.minLength(2), Validators.required])],
      "lastname": [this.lastname, Validators.compose([Validators.minLength(2), Validators.required])],
      "birthdate": [this.birthdate, Validators.required],
      "gender": [this.gender, Validators.required],
      "language": [this.language, Validators.required],
      "condition": [this.condition, Validators.required]
    }, { validator: this.matchingPasswords('password', 'passwordConfirmation') });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegistrationPage');
  }

  register() {
    let user: any = {
      email: this.registerForm.controls["email"].value,
      password: this.registerForm.controls["password"].value,
      passwordConf: this.registerForm.controls["passwordConfirmation"].value,
      firstname: this.registerForm.controls["firstname"].value,
      lastname: this.registerForm.controls["lastname"].value,
      birthdate: this.registerForm.controls["birthdate"].value,
      gender: this.registerForm.controls["gender"].value,
      language: this.registerForm.controls["language"].value,
      condition: this.registerForm.controls["condition"].value
    }
    this.userProvider.addUser(user)
      .then(data => {
        //this.hideForm = true;
        //this.resetFields();
        this.sendNotification('You have been registered. Please login now.');
        this.navCtrl.popToRoot();
        //this.navCtrl.push('HomePage');
      }).catch((err) => {
        console.log(err);
        this.sendNotification('User could not be registered. Please try again!' + err);
      });

  }

  matchingPasswords(password: string, passwordConfirmation: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let passwordInput = group.controls[password];
      let passwordConfirmationInput = group.controls[passwordConfirmation];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return {
          mismatchedPasswords: true
        }
      }
    }
  }

  sendNotification(message): void {
    let notification = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    notification.present();
  }

}
