import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TermsofusePage } from '../termsofuse/termsofuse';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  private items: Array<any> = [];
  private version: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.setItems();
    this.version = '1.0.0';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  setItems() {
    this.items = [{name: 'Terms of use', page: TermsofusePage}, {name: 'Give feedback'}, {name: 'Rate Manzana'}];
  }

  openPage(page) {
    this.navCtrl.push(page);
  }

}
