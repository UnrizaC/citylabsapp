var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
var ObjectivePage = /** @class */ (function () {
    function ObjectivePage(navCtrl, navParams, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        if (navParams.get("key") && navParams.get("rev")) {
            this.objectiveType = navParams.get("value");
        }
        else {
            this.objectiveType = navParams.get("value");
        }
        this.form = fb.group({
            "minValue": ["", Validators.required],
            "maxValue": ["", Validators.required]
        });
    }
    ObjectivePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ObjectivePage');
    };
    ObjectivePage.prototype.saveObjective = function () {
    };
    ObjectivePage = __decorate([
        IonicPage({
            name: 'ObjectivePage',
            segment: 'objective/:value'
        }),
        Component({
            selector: 'page-objective',
            templateUrl: 'objective.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, FormBuilder])
    ], ObjectivePage);
    return ObjectivePage;
}());
export { ObjectivePage };
//# sourceMappingURL=objective.js.map