import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@IonicPage({
	name: 'ObjectivePage',
	segment: 'objective/:value'
})
@Component({
  selector: 'page-objective',
  templateUrl: 'objective.html',
})
export class ObjectivePage {
  public form: FormGroup;
  public objectiveType: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public fb: FormBuilder,) {
  	if(navParams.get("key") && navParams.get("rev"))
      {
      	this.objectiveType = navParams.get("value");
      } else {
      	this.objectiveType = navParams.get("value");
      }

  	this.form = fb.group({
         "minValue"            : ["", Validators.required],
         "maxValue"             : ["", Validators.required]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ObjectivePage');
  }

  saveObjective() {

  }

}
