import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ObjectivePage } from './objective';

@NgModule({
  declarations: [
    ObjectivePage,
  ],
  imports: [
    IonicPageModule.forChild(ObjectivePage),
  ],
  exports: [
  	ObjectivePage
  ]
})
export class ObjectivePageModule {}
