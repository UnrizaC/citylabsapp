var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ResultDetailsPage } from '../result-details/result-details';
var ResultsPage = /** @class */ (function () {
    function ResultsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.segmentB = "biomarkers";
        this.icons = ['danger', 'secondary', 'energized', 'secondary', 'energized', 'secondary'];
        this.iconsbis = ['arrow-round-up', 'arrow-round-down', 'arrow-round-up'];
        this.items = [];
        this.examples = [
            {
                value: '760',
                marker: 'Atmospheric Pressure',
                unit: 'mmHg',
                date: '14/04/2017',
                icon: '',
                trend: ''
            },
            {
                value: '140',
                marker: 'Sodium',
                unit: 'mmol/L',
                date: '02/02/2017',
                icon: '',
                trend: ''
            },
            {
                value: '3.50',
                marker: 'Potassium',
                unit: 'mmol/L',
                date: '1/12/2016',
                icon: '',
                trend: ''
            },
            {
                value: '1.02',
                marker: 'Calcium',
                unit: 'mmol/L',
                date: '5/11/2016',
                icon: '',
                trend: ''
            },
            {
                value: '7.08',
                marker: 'Glycated Hemoglobin',
                unit: ' %',
                date: '14/09/2016',
                icon: '',
                trend: ''
            },
            {
                value: '61',
                marker: 'HDL Cholesterol',
                unit: 'mg/dL',
                date: '02/09/2016',
                icon: '',
                trend: ''
            },
            {
                value: '180',
                marker: 'LDL Cholesterol',
                unit: 'mg/dL',
                date: '02/09/2016',
                icon: '',
                trend: ''
            }
        ];
        for (var i = 0; i < this.examples.length; i++) {
            this.items.push({
                value: this.examples[i].value,
                trend: this.iconsbis[Math.floor(Math.random() * this.iconsbis.length)],
                unit: this.examples[i].unit,
                date: this.examples[i].date,
                marker: this.examples[i].marker,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ResultsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResultsPage');
    };
    ResultsPage.prototype.itemTapped = function (event, item) {
        this.navCtrl.push(ResultDetailsPage, {
            item: item
        });
    };
    ResultsPage = __decorate([
        Component({
            selector: 'page-results',
            templateUrl: 'results.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], ResultsPage);
    return ResultsPage;
}());
export { ResultsPage };
//# sourceMappingURL=results.js.map