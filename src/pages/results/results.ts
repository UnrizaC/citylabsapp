import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ResultDetailsPage } from '../result-details/result-details';

@Component({
  selector: 'page-results',
  templateUrl: 'results.html',
})
export class ResultsPage {

  icons: string[]; iconsbis: string[];
  examples: Array<{ value: string, marker: string, unit: string, date: string, icon: string, trend: string }>;
  items: Array<{ value: string, marker: string, unit: string, date: string, icon: string, trend: string }>;
  segmentB: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.segmentB = "biomarkers";
    this.icons = ['danger', 'secondary', 'energized', 'secondary', 'energized', 'secondary'];
    this.iconsbis = ['arrow-round-up', 'arrow-round-down', 'arrow-round-up'];
    this.items = [];
    this.examples = [
      {
        value: '760',
        marker: 'Atmospheric Pressure',
        unit: 'mmHg',
        date: '14/04/2017',
        icon: '',
        trend: ''
      },
      {
        value: '140',
        marker: 'Sodium',
        unit: 'mmol/L',
        date: '02/02/2017',
        icon: '',
        trend: ''
      },

      {
        value: '3.50',
        marker: 'Potassium',
        unit: 'mmol/L',
        date: '1/12/2016',
        icon: '',
        trend: ''
      },

      {
        value: '1.02',
        marker: 'Calcium',
        unit: 'mmol/L',
        date: '5/11/2016',
        icon: '',
        trend: ''
      },

      {
        value: '7.08',
        marker: 'Glycated Hemoglobin',
        unit: ' %',
        date: '14/09/2016',
        icon: '',
        trend: ''
      },
      {
        value: '61',
        marker: 'HDL Cholesterol',
        unit: 'mg/dL',
        date: '02/09/2016',
        icon: '',
        trend: ''
      },
      {
        value: '180',
        marker: 'LDL Cholesterol',
        unit: 'mg/dL',
        date: '02/09/2016',
        icon: '',
        trend: ''
      }


    ];

    for (let i = 0; i < this.examples.length; i++) {
      this.items.push({
        value: this.examples[i].value,
        trend: this.iconsbis[Math.floor(Math.random() * this.iconsbis.length)],
        unit: this.examples[i].unit,
        date: this.examples[i].date,
        marker: this.examples[i].marker,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultsPage');
  }

  itemTapped(event, item) {
    this.navCtrl.push(ResultDetailsPage, {
		item: item
	});
  }
}
