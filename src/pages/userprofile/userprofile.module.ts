import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { UserprofilePage } from './userprofile';

@NgModule({
  declarations: [
    UserprofilePage,
  ],
  imports: [
    IonicPageModule.forChild(UserprofilePage),
    TranslateModule.forChild()
  ],
})
export class UserprofilePageModule {}
