import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { RemoteProvider } from '../../providers/remote/remote';
import { SyncProvider } from '../../providers/sync/sync';
import { DatabaseProvider } from '../../providers/database/database';
import { UserProvider } from '../../providers/user/user';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';

@IonicPage()
@Component({
	selector: 'page-userprofile',
	templateUrl: 'userprofile.html',
})
export class UserprofilePage {

	userProfile: any = {};
	DB: SQLiteObject;

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public remote: RemoteProvider,
		public toastCtrl: ToastController,
		public sync: SyncProvider,
		public dbProvider: DatabaseProvider,
		private userProvider: UserProvider,
		public modalCtrl: ModalController,
		private sqlite: SQLite,
		private toast: Toast) {
		this.setDB();
		this.getUserProfile();
	}

	setDB() {
		this.DB = this.dbProvider.getDb();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad UserprofilePage');
	}

	getUserProfile() {
		//get local user profile
		this.userProvider.getUser().then((data: any) => {
			this.userProfile = data;
			//console.log('LALA PROFILE:', data);
			this.calculateAge();
			this.formatConditions();
		}).catch(e => {
			console.log(e);
		});
	}

	editProfileModal() {
		var data = this.userProfile;
		console.log('Modal data', data);
		var modalPage = this.modalCtrl.create('EditprofileModalPage', data);
		modalPage.onDidDismiss(data => {
			console.log('CHANGED DATA', data);
			this.userProvider.updateUser(data).then((data) => {
				this.toast.show(`Profile successfully updated`, '3000', 'center').subscribe(
					toast => {
						console.log(toast);
					}
				);
			}).catch(e => console.log(e));
		});
		modalPage.present();
	}

	calculateAge() {
		var ageDiff = Date.now() - new Date(this.userProfile.birthdate).getTime();
		var ageDate = new Date(ageDiff);
		this.userProfile.age = Math.abs(ageDate.getUTCFullYear() - 1970);
	}

	formatConditions() {
		this.userProfile.condition.forEach(function (value, key, object) {
			switch (value) {
				case 'd1':
					object[key] = { name: 'Diabetes', type: 'Type 1'};
					break;
				case 'd2':
					object[key] = { name: 'Diabetes', type: 'Type 2'};
					break;
				case 'hf':
					object[key] = { name: 'Heart Failure', type: 'Yes'};
					break;
			}
		});
	}

}
