import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  private changeForm: FormGroup;
  private passwords: { oldPassword: string, newPassword: string, newPasswordConfirmation: string };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public fb: FormBuilder) {
    this.passwords = {
      oldPassword: '',
      newPassword: '',
      newPasswordConfirmation: ''
    };
    this.changeForm = fb.group({
      "oldPassword": [this.passwords.oldPassword, Validators.required],
      "newPassword": [this.passwords.newPassword, Validators.required],
      "newPasswordConfirmation": [this.passwords.newPasswordConfirmation, Validators.required]
    }, { validator: this.matchingPasswords('newPassword', 'newPasswordConfirmation') });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  public closeModal() {
    this.viewCtrl.dismiss(this.passwords);
  }

  matchingPasswords(password: string, passwordConfirmation: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let passwordInput = group.controls[password];
      let passwordConfirmationInput = group.controls[passwordConfirmation];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return {
          mismatchedPasswords: true
        }
      }
    }
  }

}
