import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as Highcharts from 'highcharts';
import HighchartsMore from 'highcharts-more';

HighchartsMore(Highcharts);

@Component({
  selector: 'page-result-details',
  templateUrl: 'result-details.html',
})
export class ResultDetailsPage {
	selectedItem: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  	this.selectedItem = navParams.get('item');
  	console.log(this.selectedItem);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResultDetailsPage');
    var myChart = Highcharts.chart('container', {
      chart: {
        type: 'spline'
      },
      title: {
        text: 'Timeline'
      },
      xAxis: {
        categories: ['Apples', 'Bananas', 'Oranges']
      },
      yAxis: {
        title: {
          text: 'Fruit eaten'
        }
      },
      series: [{
        name: 'Jane',
        data: [1, 0, 4]
      }, {
        name: 'John',
        data: [5, 7, 3]
      }]
    });
  }

}
