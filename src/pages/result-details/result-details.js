var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as Highcharts from 'highcharts';
import HighchartsMore from 'highcharts-more';
HighchartsMore(Highcharts);
var ResultDetailsPage = /** @class */ (function () {
    function ResultDetailsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.selectedItem = navParams.get('item');
        console.log(this.selectedItem);
    }
    ResultDetailsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ResultDetailsPage');
        var myChart = Highcharts.chart('container', {
            chart: {
                type: 'spline'
            },
            title: {
                text: 'Timeline'
            },
            xAxis: {
                categories: ['Apples', 'Bananas', 'Oranges']
            },
            yAxis: {
                title: {
                    text: 'Fruit eaten'
                }
            },
            series: [{
                    name: 'Jane',
                    data: [1, 0, 4]
                }, {
                    name: 'John',
                    data: [5, 7, 3]
                }]
        });
    };
    ResultDetailsPage = __decorate([
        Component({
            selector: 'page-result-details',
            templateUrl: 'result-details.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams])
    ], ResultDetailsPage);
    return ResultDetailsPage;
}());
export { ResultDetailsPage };
//# sourceMappingURL=result-details.js.map