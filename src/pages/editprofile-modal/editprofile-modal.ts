import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-editprofile-modal',
  templateUrl: 'editprofile-modal.html',
})
export class EditprofileModalPage {

  private profileData: {};
  private keys: any;
  

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl : ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditprofileModalPage');
    this.profileData = {
      firstname: this.navParams.get('firstname'),
      lastname: this.navParams.get('lastname'),
      birthdate: this.navParams.get('birthdate'),
      condition: this.navParams.get('condition'),
      email: this.navParams.get('email'),
      homeAddress: this.navParams.get('homeAddress'),
      language: this.navParams.get('language'),
      role: this.navParams.get('role'),
      username: this.navParams.get('username'),
      phone: this.navParams.get('phone'),
      gender: this.navParams.get('gender'),
    };
    this.keys = Object.keys(this.profileData);
  }

  closeEditProfileModal() {
    this.viewCtrl.dismiss(this.profileData);
  }

}
