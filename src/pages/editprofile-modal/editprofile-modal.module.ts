import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditprofileModalPage } from './editprofile-modal';

@NgModule({
  declarations: [
    EditprofileModalPage,
  ],
  imports: [
    IonicPageModule.forChild(EditprofileModalPage),
  ],
})
export class EditprofileModalPageModule {}
