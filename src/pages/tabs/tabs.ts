import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { ScreeningPage } from '../screening/screening';
import { DashboardPage } from '../dashboard/dashboard';

import { RemoteProvider } from '../../providers/remote/remote';

import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1Root = DashboardPage;
  //tab2Root = NewsPage;
  tab3Root = ScreeningPage;
  tab4Root = HomePage;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public remote: RemoteProvider,
    public translate: TranslateService) {
    this.setLanguage();
  }

  setLanguage() {
    this.remote.getUserProfile().then((data: any) => {
      if (data.language) {
        this.translate.use(data.language.toLowerCase());
      }
    }).catch(e => {
      console.log(e);
      console.log('Could not set language');
    });
  }
}
