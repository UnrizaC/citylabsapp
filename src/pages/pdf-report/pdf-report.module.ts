import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PdfReportPage } from './pdf-report';

@NgModule({
  declarations: [
    PdfReportPage,
  ],
  imports: [
    IonicPageModule.forChild(PdfReportPage),
  ],
})
export class PdfReportPageModule {}
