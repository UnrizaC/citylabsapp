import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { UserprofilePage } from '../userprofile/userprofile';
import { RemoteProvider } from '../../providers/remote/remote';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import { File } from '@ionic-native/file';
import { Toast } from '@ionic-native/toast';
import { FileOpener } from '@ionic-native/file-opener';
import { Base64 } from '@ionic-native/base64';

import { UserProvider } from '../../providers/user/user';

import * as Highcharts from 'highcharts';
import HighchartsMore from 'highcharts-more';

@IonicPage()
@Component({
  selector: 'page-pdf-report',
  templateUrl: 'pdf-report.html',
})
export class PdfReportPage {
  logo: string = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAa0AAAGtCAIAAAAqNLxgAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAABJ0AAASdAHeZh94AAAAB3RJTUUH4ggNDTYpljQZfwAAIABJREFUeNrsnXecFHWa/yt0V+c0oSfnwDAz5CAgSREMwCrrGggiighi3nBsuNvb3dtbd8873UWCBAEREHBEEHVBRSRKDgPDBJice6ZzrO6uqt8frSw/RZju/nZPVffzuXtxt/tihu6qb33q/X2e5/s8OMdxGAgEAsWxCLgEIBAIfBAEAoHAB0GgfhJ3M/3Yf39bhfyD4YtlWZPJ9Mwzz1y8eNHv94f54WFhRFkiuAQgUPhu3tzcvHjx4kOHDrW1te3YsUOtVsNlAR4EgeJFLMueOHFi5syZBw8e9Hq9Bw4c2LNnj9frhSsjIOEA4aD+JambLEo8xGUZ8g+G7IAsy77zzjt/+MMfenp6WJYN/Pf5+flHjx7V6/UEQYT24XEch7UBPAgC8d2+WZbt7u5euHDhL3/5S4PBcN0EMQxrbGx87733gkVCIBLgQRDwoGB4MJDK2L9//7Jly2pra30+3w//TlZW1tGjRzMyMoJCwhu/CKwN4EEQiL/G3d7evmTJkjlz5ly5cuWmJohhWFtb26ZNm4JCQvA+4EEQ8KAAeNDpdL777rv/+7//297e/mMOeF1paWmnTp1KT0/vo8FBfBB4EARCs12N0G/2eDxbt24dN27cL37xi+bm5tuaIIZhXV1dFRUVffmbIOBBEPAgT3kwYKy9vb0ffvjhunXramtraZq+MR9yW5WWlh4+fDghISGELwJrA3wQBD7Ynz7IcZzP5zty5MjWrVv37dtnNpt9Pl9QDvjtbosgdu/efd9994lEomC/CKyNaArOk4BA/3JkjuMaGhq2b9++c+fOhoYGr9fLMEzI3sqy7ObNm++66y6SJG9rbVEufgQBD4KAB7//GWia/uKLL9auXXvs2DGXy+Xz+ZA8Gkql8sKFC7m5ubctoIE8CfAgCNRvLmwymXbs2PHOO+/U1NQEABDhP+FwOPbt27dw4UKJRAIXHHgQBOIXDwYqAVevXr1t27aurq7QIoB90T333LNr1y6lUgk8CDwIAvFFgSNxK1eu3LhxY29vb6BNVuT+uRMnTnR3d8vl8tDOloDAB0EgxPhpMBhWr169bt06o9GIKgh4263xoUOHsrKyKIqCW8BPwQsKFC8O6Ha7161bN378+L/+9a9dXV1erzdqQaEDBw708Ywd9GEFHgSBIuKAHMcdPnz497///blz59xud/SN5ujRo330QYgMgg+CQIgdEMOwnp6ef//3f//ggw/sdnuEMiG3VUdHR319vUajIUkS7gvsi0GgqGLgJ598cvfdd7/77rtWqxWtCQZVB8Oy7JkzZ/x+P9wX8EEQKHomaDabX3zxxfnz59fU1CBvdkAQhFwu7/seluO4kydPgg/CvhgEip4JVlVVLV68+OzZsxGaE6LX681mc1BxxqqqKrQV2iDgQRDo5mIY5oMPPpg+ffqpU6ciNyxJq9UGC3dXr171eDxwg8AHQaDIyufz/fd///fixYvb2toiB19KpVImkwX7U3a7vaWlpb8SNSDwQVBc7IVpmn7llVdef/11q9Ua0cqYMWPGNDQ0BOtogU42sDXmpyA+CIoFE7TZbM8999zu3bsjvfcUi8Xp6ekejycEq21vb4caafBBECgiJuh2u5csWbJ7926apiP9z40fP76uri60zG93dzfwIOyLQSD0oml66dKle/bsiYIJkiR51113Xb58OTQ7a21thfgg+CAIhFh+v/9Xv/pVRUVFdFKxY8eO7cukuh+TyWSCfTH4IAiEUizLrlmzZvPmzS6XKwr/HEmSzzzzzGeffRZyOY7NZgMfBB8EgZCJ47gDBw788Y9/dDgc0fkX77rrLoPB0NvbG7KXuVwu8EHwQRAImdrb219++eWo7TQpilq2bNnWrVvDiUJCHTX4IAiETAzD/Pa3vw2hiC9kzZ07t6ur69q1a+H8i3C+GHwQBEKzHeY4bvv27Xv27IncsbnvKTk5+dVXX12zZk2YQAejmsAHQSA0MpvNf/7zn51OZ9T+xV//+tc1NTUXLlwIs/pPKpVCm1V+Cuqo+S6/32+z2cxms9lstlgsgT8DIghC8iNSKBTp6enZ2dlSqTTGLsjy5cubmpqitiMeN27c7Nmzn3jiCbfbHeavksvlsJ7BB0G3kdfrra+vr66urq6urqmpqamp6ejoCBRbXB9bwd2g63Bx0/8Hx3GCIFJTU3NycrKzs3Nzc6//mZmZKcSZQRzHtbW1rV+/Pmo7YplM9pe//OXQoUOnTp0K/yiIWq0GHgQfBH1fHR0dJ06cCLhedXV1fX291+tlb1D4U3vq6+sbGxsDnnj9T5lMNnz48DFjxowZM+aOO+5ISEgQyvO5du1ao9EYnRwxjuOvvvpqWVnZsmXLkJQo6vV6GN0JPgjCMAxzuVzffPPNgQMHDhw4cOXKFb/ff6PxRYKhfggyLpfrq6++OnToEEEQJEmWlJSM+U75+fm8fVZNJtOWLVuiA4M4jo8ZM+all16qqKgI+SDd95SdnQ0+CD4Yv2JZtqqq6quvvjpw4MDx48ddLpff72cYph9PmzIMc/3ZPn/+fGVl5fr160mSTE1NnTp16kMPPTRp0qQQuuxFdFO8a9cug8EQHRhUqVRvvPEGy7J///vfw48MBpSRkQE+yN+YCyhCCkznefnll4uKihQKhUQi4f+4MhzHxWKxTCbLyMh45pln9u7dGxh0GblL9EPd9L/3+XxTpkyJzgUkSfKNN95wOBy//OUvERa77N2793txjx/71vDsRFnggxGR2Wxes2bNuHHjlEqlWCwWYnT8uiHm5OS8+uqrp06d8vv9/eiDVVVVWq02Ot999uzZJpPp9OnTiYmJCL31h2EQ8EHwwdgEwKNHjz777LN6vV4qlcbGJihgiHK5fMSIEW+++abZbEb4oPbdB1esWBGdGqDBgwc3Nja63e5p06aJRMgCR9nZ2b29veyPC3wQfFDwMhqNy5cvHzFihEKhECgA3laBcsWsrKz/+q//6unpQfK49sUHGYbx+/2zZs1C6Eo/Jq1We/ToUZqmV69erVQqEf7mhx9+ODBIHnwQfDA2t8B/+ctfMjIyYgYAb4uHFEWlpqb+9re/bW9vD/Oh7aMP2u32jIyMSL9dxGLxhg0bXC5XVVVVdnY2wn8Ox/GVK1e63W7wQfDBWJPD4Xj99dezs7MlEkm81ccG3DApKenVV19tbGwM+dHt47744sWLaOnsprT7+9//3mazud3uGTNmoGVPiqIqKytvERwEHwQfFKqsVmtOTk6cV0KIxWKtVvvcc88Fms6H74OBcqLv6f333490Ec8TTzzR29vr9/v/8Y9/KBQKtL984sSJFouFvaXAB/tRUM0UVonZokWL+F8KE1H5fD6LxbJ+/foxY8Zs3Lgx/CLnm5J1pFtsTZ48+a9//atarT579uzf/vY35A2uZ82aBc1moH4wZmUymTIzM+HQaMC/pFLpAw88UF1dzTAM2n3x4sWLxWJxhD75sGHDamtr/X5/T0/PyJEjkb/YtFrt1atXb8q5wIPAg7EgrVb70ksvRSGPKYgXqsfj2b9///jx49944w1UZzAC6uzs5CJzjKS0tHTz5s15eXkMw7z66quXLl1CPlpz1qxZqamp8LIEHoxlWSwWtLnFGJBEIrnzzjtPnz59WzDsIw9OnDgROabhOJ6Xl3fmzBmv1+vz+d58881IpGLEYvHRo0d9Ph97OwEPAg8KWBqN5pVXXgEkvFE0TZ84cWLKlCl//OMfQwDDH6Kf0+lEzoMZGRlbtmwZNGgQQRD79+9/7bXXItHb9Z577ikvL4/zIDLwYFzIZrPl5eUBEv5QFEX97Gc/C5yjCIcHR4wYgTAvj+N4VlbWkSNH3G633+8/d+5cbm5uJPL+IpHowIEDfYFB4EHgQcFLpVL9/Oc/j1wgX7jyer27d++eMWNGQ0NDOECHlqfy8/M/+OCD0aNHUxTV1ta2YMGCQNEP8q//k5/8ZPjw4QCDwIPxIrvdXlRUBEh4UxEEUVBQcPz48R+GC/vIg5MmTULlJiUlJWfPnqVpmmEYk8k0ZcqUCL3A5HL5sWPH+giDwIPAg7EgpVL5i1/8ApDwpmJZtqGhYebMmRUVFaHNrkxJSUHyjhk6dOiOHTsGDRokEokcDsezzz4bSGJE4lu/8MILgwYNAhgEHowvOZ3OkpISQMJbSCaTvfbaax6PJ1geXLp0aZjvGBzH77777pqamgCgORyOOXPmRK6BzYABA5qbm29bMwg8yBOBD6LUO++8I8T5R9GURCJ5+eWXr/d27aMPvv766+Gcx8BxfP78+e3t7QETtNvt8+bNi5wJisXiiooKj8fDBiPwwX4UVHug1OzZs998882qqiqERR44jmdkZCQmJup0Op1Op9VqA39qtVqJROL1ej0eT+BPmqZpmna73d3d3S0tLc3NzWazmYtKF/u+K9DSKi0t7dVXX+37OyMvLy/kfK5YLF62bNkrr7yi1WoJgnA4HEuWLNm1a1eYQ9lvcb9efPHFadOmwRtRQML59pwIXVu2bFm4cGHIx2xxHM/Ozi75TgMHDiwuLlYqlYE5czcK++4oLvfdPM8b/wxghc1ma21tDXhiXV3dqVOnqqqqkJ+XCI0K165dO2fOnJuGz3D8+8uyqqpqzJgxwdb34TiuVCrfeOONxx57LNA6wWQyLV68+LPPPoucCY4dO/bDDz9MTk4O1rhv/NYQXQEfFLZomh4zZkxlZWVQdRjl5eV333333XffPXLkSLVaTZJkYJIcQRABBwwn+HtjKxeLxXLmzJkTJ06cOHHizJkzDoejvy6USqXauXPnPffc8z0r5G6Yy3xdLpdrwIABHR0dQS3XgoKClStXTpgwIbAF7urqevrppw8dOhQhE8QwLD09/eOPPx48eHAIdfXgg/0pCA0g1/vvv9+XPVFKSspjjz22Zs2a2tpau93udrsD0auIfrbAwCOPx+N0Oo1G49dff/2b3/xm4MCB/dI9TK/X22y2vvTd8vv9wfYEnDp1alVVVaA+hmGYixcvjhgxIqIJfaVSGdhusyEJ4oOQJ4kp0TQ9cuTIH3MWqVT62GOPffrpp2az2eVyBQaY9ddH9fv9Ho/HbrefOnXqN7/5TZRPxeA4brVa+zif5LXXXutjqkQsFv/qV7/q7OwMDEf1+/2fffZZfn5+REtYxGLxmjVrHA4HG6rAB8EHY00VFRU/fGgHDRr0P//zP42NjU6n0+fz8eoDMwzj8XisVuu+ffvmzp0btWZ5fffBU6dO3bYPQiC6umvXLpvNFuBKt9v9+uuvJyUlRRR4RSLRG2+8EWDbH0uCgw+CD8advF7v2LFjA8+eWCyeN2/el19+abVaaZrm+Sr3+Xwul+vy5ctLliyRy+X94oM3lcfjGTJkyC3sDMfxiRMnXrp0yePxBEywq6tr/vz5SqUyopBLUdSbb75ptVoD/yj4IPgg6F/6+OOPFQrF/PnzKysrnU5nJIb/RhQP3W53bW3tK6+8olar+eCDDMP86le/+rHAK0EQ8+fP7+jo8Pl8gb3wwYMHhwwZEuniFYVCsWbNmusmCD4IPgj6PhLW1NQ4nc6+N2fmmwL7ysbGxmXLliEf2RHsvphhmOPHj990a4zj+JIlSwwGQyAlQtP03/72txAqV0LLDjscjhtzO+CD4IOg71NVDHyLgBuePXt28uTJyFMNfffBwNZ4woQJP/wM8+bNu26CTqfzueeeU6lUkU74jBkz5vz58z/MDoMPgg+CYtnTbTbb8uXLExMTEVpMUD7IMMzatWu/N7hu6tSpHR0dASLzeDzPP/98hND1xiLwn//8521tbTftJQM+CD4IinHRNF1TUzNr1ixU/bf7Hh8MyGQy3djfrLCwMDBiKeCS//d//xfpMcfDhw//9NNPbwwIgg+CD4LiEQwDA+yR9CkI1gf9fv8bb7wRKOtRKBT79+8PpOBZlj106FBSUlLktsNpaWmvvfZaZ2dnoOSzL3YGPigUwbk6UCjyer179+599tlnw2zlYLVaVSpVUD9iMpnGjx9fV1f3u9/9btmyZTKZDMdxu90+bdq0M2fOROL0dGpq6vz58xcvXpyWlkZR1K3TLz88HA3n6vgv6DcDCrFo7sEHH8zIyJg3b16YPfeDlVar/f3vf79p06YXXnhBKpUGLGPFihWVlZVoTZAgiNLS0jlz5syePVuv10skkusdLkAxJuBBUOhiGKapqWnu3LmnT58Obb5HCDwYoFGj0ajX60mS5Diuvb19/Pjxra2tSBYzjuPp6en333//ww8/PGrUKLlcflsGBB4EHgTFr0iSzM/P37Fjx/Tp06urq1GNOrqtlVAUdeNk9NWrV3d3d4dpgjqdbvDgwRMnTpw8efLgwYNlMhlFUTCOFXgQBOqTOI6rqqq6//7729vbg11ON+XBviMVx3FtbW3jxo27sSWXUqm87777HA6HwWCwWCxutzvQyyew1ZVIJAqFQq1WJyUlZWRkZGdnFxUVDRo0KCsrSyaTiUQisVgcTpkk8CDwICgu36U4XlpaunXr1lmzZplMpij/61u3bu3t7b3RQV5++eWf//znFEVdr2MPNJ7hOI4kSZFIFGjpGOjteGOrx1sbLngT8CAIdBsxDPPhhx8++eSTQXU5DZMHaZoeMWJETU3N9S15Xl7e4cOH09PTv2dbgV8YTkfbPv4s8KAQBXM7QWhEkuSsWbMef/zxqMXUOI774osvmpubr5sgjuMvvfTSTasIw0z1gjGBD4JAfZJYLP7zn/98YwYj0j64bds2mqav/zcZGRmPPfYYDEgCgQ+C+lPp6el/+tOfotPG1WKxHDp06Maawblz52q1WmA3EPggqD+F4/jcuXMnTpwY0Sb4AX399ddWq/V6TE0kEj3++OMAgyDwQVD/i6Ko559/PtJ+xHHc3r17A9UwAQ0ZMiQ/P79fBk6BwAdBoO/r3nvvzcnJiej+lKbp48eP+/3+G/9RgEEQ+CCIL5JIJPPnz4/okMzq6urOzs4bK1SmTJkCxz9A4IMgHmn+/PkRzZYcOXLkRhhMSEgoLS0V9KYYKnnBB0GxpvT09OLi4sgZ08mTJ2/0wbKyMoVCAcFBEPggiEfCcXzo0KERyhozDFNZWRkonw4USA8bNkzom2Io9+lHQTwFFCkNGzZsy5YtkfjNnZ2dgeBgRkbGzJkzH3jggbFjx0Zt9jwIfBAECsIHQ+PB20bK6uvr77///pkzZ06YMEGtVkskEpIkQz7YyxNBfLA/YRyuPiicR9fDMFY3bfHQFg9tcdNWD+32M7Tf7/YxHa3N/3hqjtfjvsVvuO3xj6LS8tVbt6XptDq5VPrdztfv99M0HegPCNtJEPAgKEryM0yPy91ld3XZnZ12V6/LbXHTFg9NMwwbGPiF/etPDMM4jHPbXCz27VuWEIlIMUWIKVIsJimKFItJMUVQFCEWi8QSkhITIoqgKJKiSFHgL1CEWExSlESX+PcTlaRIROC4TERqZdIEmUQnk+pkEp1MqpVKMtTKBLmUADcEAQ+C0Iph2U67s8ls77A7Ou3Oboerx+HyB8a6c9i349QwjLvl6uEYxmnqJUQiUiTGCeJf4IbjGIbhGI7h//qP3+UJcOzbP777jzhOfLe5xr/NimA4huM4RgT+xHGlhMrVqnO0qlydOkerBlsEgQ+CQhHLcd0OV7PZ1mi2NZltLRabh2FYjgv8L8dhbGjrhOOwyFsSjmEEgRM4TuJ4wBZzNKpcnRpsEQQ+CLrdVpdlm832ml5TjcHUaLa6fH4mYHwsxwh5VdzKFnXqRJkMXBEEPhjf3MdybTZHdY+pxmCs67W4/H6G5RiOZdmYXQffs8U0lWJwWtLQVH1BgkZEQv0sCHwwbuTy+S919Z7vMFwxGO1eX8x73y1E4DhJ4CROqKXUoJSkYWlJpSmJ8kgehQaBD4L6Uzbae7Gz52y74YrBSDMMw3IMy8L9vs6JJEGQBC4hyZJk3dA0/eC0pES5DDbN4IOgWJDR5bnQaTjXYajtMftY1s+yDLjfLRUgRBGB52jVQ1KThqbrszRKEk4rgw+CBKcuu/N8h+Fsh6HRbPMxLMOyDNzckHbNIoJIlEmHpCWPzEgpTtKCIYIPgvguH8OcaTd8Vd/SYLb5GNYfp3E/1A8GjolwgiSIDLViUl7G2Jx0JUXBlhl8EMQ7mVyew01thxrbzW7axzBAfxEiRDFByMWiO7LSJudnZGvVUI0IPgjqf3EcV9drPtjQdrbDQPv9PuC/qEhMECKSKE7UTsrLHJGRIhGRcE3AB0H9INrvP9HadbChrcVi8zKs/7v55aCoicRxEUlopZLxOemT8jKTFTJo9AA+CIqSuh3OQ43tR5s6rDTtY4AA+/uxwTARSVAEMTgtaXJ+Vpk+UQS5FPBBUITEclyVwXiwvrWyq5dmGD/Uv/ANDwlcTJCpSvnE/Mzx2ekqKeRSwAdB6MSw7InWrs9qGzvsTh/D+MEAeSwCx0UEoZKIpxXl3FOQraDgdAr4ICg8cRx3ubv3w6r6JrPNyzCwBRbQZllMkoly6fQBeRNyMyCRAj4IClGNZtuuqqtV3UaPHxxQmE8UjlEEmaZSPDgwf1RmKrRyAB8EBSGDw7X7Sv2pti63H8KAsbBTFpNEnk49q7SwPDWJhJwy+CDo1rLR3k9rGr9ubHN6fVAKE2NuSJFkqT7hobKCwgQtFGCDD4JuItrv/7K+9Z91TVYP7WPAAWNTJIFTJDkiXf/gwIIMjRLcEHwQ9K38LHu8pePjKw09TreXYWL+HogIQkIQJBGYL4JhGI5hHIthHMd5GZZmGCwOrgAlIsbnpM8oyU+Wy8EMwQfjWhzHXejs3X3lWrPFTjOM0K+/mCA0UrGakmikYo2E0kgpDUUpKJGUJCUiUiIipSIRRRAiAsdudvAi8OVZlvOyrNfP0H7GwzK0n7F7fRaP1+qhrbTP4qEttNfm8fqFv1bFBCGnRFMKsmcMyJNDeQ34YHzK5PJsuVBzodMgxHQwgWOJMqleIUtRSPVyebJCqlfIlGIRSRCBGXLEd5PkvvufGwbP3fb1gGEYh3Hf/l/s+jw89rtZoAzHObw+G+21eLxGl6fN4Wy1OrudLiGmlCiSyFCr5g8bOCBZB9tk8MH4wsATrZ3bL9YZ3R6hJENEBJ6uVGRrlNkaZZZakSyXUiRJ4HiA7QgcJ6J7tvZbf+QwFvt2mJTHz3TYna12Z6vNISxbJHBcKiLvKcx+cGAB1F2DD8aFrB5664WaM+3dbp+f55c7QSYp1GlytcpstTJVKZOQJEF8O+GIn+Ry44w9wdmimCQy1aonhw0sBjAEH4xtDDzT1r3tYk2Pi78YqKLERQmaokR1kU6TrJCJcPy69wnugn/PFput9qoe86Ues8nl4edCBzAEH4xx2Wnv+xdrT7R2uv18DPHnaVWD9AmlSdpUhVxEEqRgve/WtsiwrI9hOxyuKz3mSwZTi83Jw8ismCSy1Kr5AIbggzGm8x2GLRdqDA6Xj08YSOJ4YYJ6sD6hTJ+QIJWICYIk8Hh48FiOY1jOz7EWN32l13K5x1xrtHj5VLMZAMOphdk/ATAEH4wBOb2+nZfqjjZ3uHx+nlxeEUEMSNQM1ieU6xPUErGIIETXK/niLlKBMRzrZzm3z19nsl7uMVX1WOy0lyePQQAMnxg+cEASgCH4oGB1ubt387nqToeTD+dDcAwbmKQblZ48MEmrpMQiAicJAp6tf+2aWc7PsT6GbbLaT7Qbznf20jy4a9+BYc5PBuYDGIIPCkxun/+Dy1cPNbbxAQMVItEdmfpxWSkpCpmYIMD+bmOIHOdnWavHe6qj51hrVy8PkioBMHxqZGkBHEwGHxSKOu2OVScqmy12b38fEctQKcZnpYxIS1ZQIjFBwCMUxJYZw/wMSzNMTa/1WFtXVY+5fzMqBI4rJeInhg4cm50Gff/BBwWwF1576lKvy9OPjw2J44NTEsdnpRTo1BIRKQIADG+/7GNZg8t9oq37mzaDw+vrx4dEKiLvLcr5aWmRRAyNXcEH+UkQHHegvnXHpTpH/wXaVZR4bGbKuMyURLmUIglodYdKLMf5WdbtYy4ajMdauhqtjv56z1EkMTg1+ekRZTq5FO4u+CC/5GeYbRdrv25sc/n8/fIB8rTKO7PShqQkKMQiMUHAAMlI3WiW9TFsi81xtLX7dIehX2bFkASeqVIuHj04NwEGyYMP8kY2j3fNqcpL3cZ+CQhmqxXTi3JKkrQSkoDIUdTw0Mew7Q7XvmstF7tNTNQfHALH1RLqyeGlozNTSQh7gA/2u1qt9tUnL7ZYHNE/Kpcol0wvyBqWmiQXi+Fh6Bc39DJso9n2WX1rrdEa5Z0yjmESkWj6gNyfDCyAOVDgg/2pcx2GDWcum9x0lJ8BpVg0NT9zXFaKihIDA/a7G3r8TE2v5bNrLdE/qEeR5PD05AXDyzQyCbwJwQf7YfXvq2vaVXXNGd0EopgkJuek3Z2ToZFSAIH8EcNyHr//fLfxn9daDS53NJ8kEUFka1TPjx2SrlJAXBh8MHqi/f7N56uPt3S6o5gVwTHsjgz9vQVZeoWUIklY7zyUn2VdPv/J9p7PG1otnuiVDRA4nqKUvzBmSF6CBjIn4IPRkMnleftUZU2POZpZkfLkhOmFWVkapYSEXDDf5WNZO+072tr1VVO7wxulNyWO4zqZ5LnRg0tTEqFeCnwwsupyON84erbd5ozaWOEsteLBAbnFCRqpiIRXvVDEYZiPYS0eem9d86nOnuisFhzDVBLq6ZHlozJSIGQCPhhBE/y/w2fb7VGKhZM4PjU/Y2pephIigYJ1Q6+fqew2fVDTYHbT0Xm6ZCLRvKElk/IzIYEGPih4E0xXKmaXFeTrVFASIXT5Wc7s8XxcGz0wlIjIhwYWTC/Jo0hYPOCDwjRBAsem5GVMy89USygI9AAYhiaKJKeX5M0aWEDBexR8EJkJHjnbHpXSsBSF7PGZGCSiAAAgAElEQVSy/KIErUQEGeFYBEO3Z09d8+mogCFFkvcX5zxcVgRWCD4oGBPEMWxybvr9BVkaCQXRwBgGQ9rPXOw2fljdaPZEHAwpkry3MPtng4ohugI+GJ4JHj3bbo24CSbJpbNLCwYkAQbGCRiyJjf9cVTAkCKJe4tyf1ZeBFYIPshrE5yQlTqjKFsrlQAGAhhGiAqnD8idVVoIG2TwQT6aoEIsemJQUZleJxWJwALjEwyNLs97l67VmSLbpoEiyZkD8x8cWECRUEwDPsgnE0xXyp8aWpylVkKdV1yDIYc5fL6Pa5uPtHZGtJshRZKPDy6eWpQjhvUGPsgTEyxPTphXXpggl8ARERCGYTTDHG/t/rC60RPJw5oSEbloZPkYmHACPnhrGZ3uvx053RZhE7wnL316YbaSosADQdflY9mrRuvmyqtGdwQn5CnE4ufHDh6SqodgNPjgzeXx+//n8JnaHnPk2guLCOLx0vw7MlNkELEG/UAMxxmc7s2VdfVme+TexBqp5NU7hxXDbHjwwR+K5bi1py4dbe6IXFtpjUS8YMiAAYlaiFWDfkwcx9m9vl01jd+0GyJUUoNjWJJC/osJw7M1KrBC8MH/T3uu1O+6co32Ryo6k61WPDVkQLpKAfsRUF+2Jkdauj6qbfIyEXkr4ziWqVb9cvwIvVIOTvg9xS+knG7r2lNdHzkTHJ6W+MKo8gw1mCCoT5KKRJNz058aPEAemTnFHIe12xxrT19yeL1wtcEHMQzDmsy2DWevRG7e5vTCrPmDinUySA2DgpCYIIalJT07fKBGIo7EumE5rqbHtONSncfvh6sd7z5ocdOrTl60eOiI7D4wbG554f2F2UpKDBYIClYiAh+YpFs6oixZEZGR7QzHHWpsO9zYHv1Ri+CDPJLPz7x9srLd6ohEYBTHsDnlheOyUqSQGgaFKhLH83SqpSPLMiIzg8nHsNsra6/0xwhm8EFeiOO49y5UV/UYmUiaoAQaYYLCfCxxPF0pf35Uab42Iuldl8+//szlbrsTyofj0Qc/v9Z8uKnDF4F8HJggCLkVJsllzw4fWKBTR8IKe1yudacv270+uNTx5YOVXb0fXLoaiQgxmCAoEsIxLEEmWTSspCgCkzk5DqvrNb9/sRZyJnHkgx02x7rTl5wRePuBCYIiaoU6meSZoQMKI0CFDMcdbW7/qr7Vx8R7ziQufNDp9a08edHoQn+EE0wQFAUr1MokzwwriUSs0MewOy/XXTb0xnnOJPZ9kOO4bRdqmiNweBNMEBRNKlw0fGCuRoncCj0+5p3TVd0OVzznTGLfB891GI63dCKvlgITBEXZChNkkoVDS1IVMuRZE6PLveNircfPxO3ljXEftHm8712oQR4JBhME9YsV6hWyhUNLdFLEJdYchp3tMJxs7Yzb4upY9kGO47ZerOlxupDj/szinHGZYIKgqFshjmVpFAuHFispMdrf7GfZHZfqDE53fO6OY9kHT7V1nWztQt7IaFR68pTcdJgEBuqfJxbHCxM08wcXyUQitL/Z4qZ3VMbp7jhmfdDs9my9WEuj3hHnalWPlebLUb+NQaC+i8TxQfrER8vy0Ta15DDsbLvhZGtXHO6OY9MHOY7bcqHG6HSjRUGdlHpqcLFGQkEDBVD/SkTgYzL09+RmoB3A5GfZHZdq43B3HJs+eLyl82x7N9qSKDFJLBhSnKqS49BKC8QDiQnigaKssmQdiXRBxufuOAZ90Oh0v19Zi7zB6uyyguJELQkmCOKNpCLRnPLCdKTv5vjcHceaD7Ic996FGhPqoyPT8jPvSNfDEFgQ36STSeYPKlaJUQasA7vjnnjaHcfag32kqf18pwHt0ZEhKQnTC7MgQQzioXAMy9EqZ5cVoK3isrjp7fG0OxbF0pfpcbp2oN4RZ6gUs8sLESaIDQZDRUXFkSNHrFYrwzB6vX7kyJETJ04cMmQIAbwZE6qrqzt06NDJkyc7OzsZhlGr1XfeeecjjzySlpYWieAygePD0hLbHRn769t8iDazgd3xqbauO3PS42EAfOzMq2NZ7o1j5851oIRBJSV6dfSgLESHOltaWl5//fV9+/bZ7Xav1xu48gRBiMViiqKmTZu2bNmywsJCyMMIV93d3W+++WZFRYXT6fR6vSzLYhiG4zhFUQqFYurUqb/+9a+zs7Mj8cLz+JmNF2ovdKPsMZwgk/zH3WNSlbGfG4wdHzze3L7m1GWaQQaDBI4tHVE2SJ+AZOBcZWXlokWLGhoaPB7PTf8CRVFpaWmbNm0aPnw4gKEQ1djYOH/+/Lq6OpfLddO/IJFI8vPz161bN2jQoEjcYouHXn66qs3mRIUCOIaNzkpdMnqwTCyK7XsXI8+b1898VFXvZVDuiO/OTS9N1iExwaampvnz59fW1v6YCWIY5vV6W1paFixYUFVVxcIMHaGpo6NjwYIFly9f/jETxDCMpuna2tpnnnmmsbExEvyhkUrmlhcinI0T2B1f7u6N0HR58EHE+qqhtdOB8hxxmlJ+f2E2knp9lmWXLVvW3NzM3M6mOY5raWn55S9/eQu7BPFQHMf953/+Z1VVlf9255dYlq2rq/vHP/4RiVuMY1ieVnVPXjrCwgY/y+6+Uh/zPatjwQcdXt8nNY0Iy51IHJ9TXqhClBs5d+7c4cOH/X1bSRzHnTp16pNPPmEYBvxFKDp58uT+/ftpuk+TYFmW3bVrV29vbySQkMDxe/Iys5G2KWwy2860d8d2OWEs+OA/a5vMSN+u9+RlIGyDvnv37qBe/n6//+9//7vb7QZ/EQoMrlixwm639/1HbDbb3r17fb6IzEiSi0WPluYj3B0zHPdxdYPbG8tIKHgf7HW6v7jWjDB+kaVW3FuQJUZ3gv2rr74KFu6uXLny6aefAhIKQmfOnOk771+3zi+++CJCPohFYHfcaXcebenwxS4SCt4H91TXO9BNXyIJ/PGyQiWFMjtmNBqDfgMzDCChUBQsDH77/o7MvjhCu2OW4z6taXR4vLGaLhG2DzZbbMeaOxAWDN6Xn4V8Go7H4wlhxVdXV+/du9cPMxV5D4MHDx4M4Ta53ZE9tYZ8d2x0ew42tPpjdLKdgH2Q47hdl68hPPqTo1Hek58hJgnknzOEn2IYJkJZRRBCrVy50uFwRG1V9OPumOO4fVebTW53TJ45FrAPVveYLnb1oIJBEUHMLitU8qletKam5uOPPwYk5K3Onz9/4MCByIX5+LY7ttPeL662+NgYDFsL1QdZlqu4fI1GR+kPFGblaJW8Oj8ESBirMBg1od0dcxh2oKE1Jid8CtUHT7d3XTNaUN2PfJ3q7tx0HrbVqq2t3bNnDyAhD3Xx4sWI5nz5uTt2+/yf1TZ6Yy5KKEgf9DHMrqprPkRlJTiG/WxgvoKXJyghccxnGAwhTdx/u2MFqt3x0eaOVqudjS0kFKQPHmps77A5Ud2HUenJuRoVbztqXL16FZCQb7p8+fL+/fv5D4PXd8fTi7JRDXXyMeze6gZvbBW3Cs8HvX5mb00DqlM+YoJ4oDALeY4YLRL+4x//ACQEGAxHpUkJxQnIpkqc7TBc7bXEEhIKzwdPt3cb0bXdn5CdmqKU87y52tWrVz/66CNAQp7oypUr//znP4UCgwGJCPyBwkxU73s/y35a2xhLSCgwH+Q47kB9C8OhgUG5iJyal8H/drsMwyxfvvwWDZ1A0dSqVatsNpvgPnaeTj1En4ikjxyGYZe7jR3oGh2CDwanRrO13mRFdfHvyc/UyaSC6LR77do1QEI+qKam5pNPPhEWDAZE4vh9BVmoxpj4WfZoc3vMnDgWmA8eqG/1IcrZ62SSSTlpIkIYDccBCXmi1atXCxEGA8pQy0enJ6PaAB1t7nB6fbEBhELyQYuHPtnahQrFHyjIUlJiAX39+vr6Xbt2ARL2owK5eyHC4LdPO45Py8+UIyqrtnu859q7mZhAQiH54NGmdjciF8hQyUenJwtrKHsACZ1OJ/hRf0mgkcEblSyX3pmdiqSsmsOwrxvavTEx21MwPuhn2K8aWllEfQZnFGULcfRMQ0MDIGF/KRCiFS4MBoTj+JTcDJVEjAQBGs3WRostBrIlgvHBi109PU43kutdnKAZpE8kBDiKEJCwHyXoyOCN0kipyTlpIhQ1NAzHHW5s9wn/mJ1gfPDL+lY/IhicWZwtIYV6sLqhoaGiogKQsF9IXOgw+C0SYtjE7LREmQQJCZxq67J4aKEDoTDsoNVqrzYYkXRVKNfr8nVq4c6lZln2rbfe4nmbk9jT22+/HRswGJCCEk/Jy0ASJXT7/Cdau4Ten1UYPniwvhVVi4vJ2XzsKxOUGhsbP/zwQ0DCqKmpqamioiI2YPA6Eo7NSElAhISHGlppgZ8tEYAjOL2+Y82dSGKxqQr5gEQNIVgYvI6Ey5cvBySMmtauXWu1WmOs6Z5URN6RrhfhCBygy+GqNhgFPetdAD54vLnTgehVPD4rhUI3saF/CeWDDz4AJIyCWlpaduzYEUsweF1jM/VImtCwgWyJkPtU890HWZY70NDCoqjVlJLkHRl6UuAweB0J33rrLWG1PBGo1q1bZ7FYuFicypEgk5Yl65CcOL7Q2WMQcp9qvvvgVaMZVavBUenJKok4ZhZxc3MzIGGk1dbW9v7778ckDGIYRuD4nZloaqp9LHusWcADjvnug2faDahaDU7MSSNxImYWMSBhFLR+/Xqz2RyTMBhQcaImVSFDEjE/2txBC/ZsCa99geW4M+1dSFbhgERthkoRE3vif6mlpWXnzp2AhBFSR0fH1q1bYxUGAxKTxJgMPZJuIyY33WS2McJ8Z/DaB5vMNlQtVydmpwqltUywSBhLdW0Ag9HXqAy9TITgjCnHcZe6egXadoHXPni23YAkGZ8ok5TrdQSOx94ibm1tBSSMhDo7O7ds2eL1emP+m6ol1LDURCTNuC509vhZ4EGk4jjuTDuaLlt3ZqZIRaKYXMQsy65YsQKQELk2bNhgMpliHgYxDMMx7M7MFCS7pXabw+AQZJNq/vpgu83ZaXeFf0XFBDE2KzUmYfA6Em7fvh2QEKG6u7vfe++9eIDBgLK1qjyNKvxnhOG4S92CLKjmrw+e6zAgiTUMT03SSSk8dhcxy7IrV660Wq3gXwhhsLe3Nx5gMCASx8dmpSApoLnY2eMXYIiQvz54pq2LxRAsxLGZMVI7fQu1tbUBEqJST0/P5s2b4wcGAxqWkoikKWFtr9nu9QruBcJTH+x2uJqt9vDfxxqJuEDI3WX6joSrVq0CJESijRs39vT09B0G1epYWGAysWhISiIZNhL6GPZKt0lwWWOe+uD5DjTl0+X6BIok4+HpbWtr27ZtGyBhmDIajZs2bQoKBufNm0dRVAx890HJOiTZksou4WWNeeqDZ9q7kVzJoSmJQnxVS6XSYBGDZdnVq1cDEoapTZs2BQWDiYmJMeODhQkaJGUVl7qMtE9g72M++qDT67tmRNDmSC4iixIE2WUrMTHxgQceIIMk2fb2dkDCcGQymTZu3EjTdN9/ZMGCBXq9PjYCL1IROSBBHX7bBYfXW2+yCitrzEcfrOs1I9kUl+kTpMLsskWS5EsvvRQsZQSihLHaHCUK2rx5c3d3d9+vnk6ne/LJJyUSScxcgTJ9QvgdCblvt8ZCChHy0QevGIxISjGHpiQKt2xw0KBB06dPDxYJA0diAQlDkNls3rBhQ1AwOH/+/NTU1FjKwpUmoQkRXgAfDF/VPSYu7IoZiiQHJmqF64MikSg0JFy9ejUgYQh67733Ojs7g0oTL1iwQCqVxtJF0EioXBQF1QaHu8PmENDBEt75oNVDt1kd4V/AgUkaOSXss3ShIWFnZycgYbCyWCzvvPNOUDD4xBNPZGRkxFhJFo5jpSiyxizHVXb1CggJeeeDtT1mJEn3oSlJQj9LB0gYNW3dujUoGFSpVE899VQsRQb/9fbVJyBp01nbaxZQqoR3PljdgyA4SOBYWbI2Bs4Uh4aEXV1d7733HiBhH2Wz2davX+/xePr+I7Nnz87KyiIIIvauRqpSppdLw392ms02BngwZF0xIGjykalSqGKipCtkJHz77bfjpF1K+Nq2bVt7e3vfr5VcLl+4cGGMRQZvYAi8VK8L/yiqzes1u2mhrEB++aDN4+1yIOgxU5yoIWKl62poSNjd3b1lyxZAwtvKbrevXbs2KBh8/PHHc3NzYxIGAypHMbyJ47AWq50BHwyFpa02JDGFAYnamAlfh4yEa9asMRqNgIS3hcG2tra+XyWpVPrss8/GKgwGlK9VK8UIei40W2ws+GAIakBxjIQk8HydOpYaDoaMhBAlvLUcDse6deuCShM/+uijeXl5MQyDGIaJSaI4URM+EjaZbUJJlfDrdjaareFfthyNUi6Oqe7TISPh2rVrAQlvoe3bt7e2tvZ9OjZFUYsXL45tGAwISRWhgFIlPPJBjuMaTLbwH9oBiZrY6z4NSIhcTqcz2Mjgww8/XFBQENswGFCmRkHEU6qER3fU4qYtHjr8a5avVcdeu0GRSPTiiy8Gi4Qcx61duzauWiv3XTt37mxubu47DIpEoiVLlshksni4OJkqRfgpYwGlSnjkg01WNFHVHBRIz0MNHjwYkBCVXC7XmjVrgoLBhx56qLi4OB5gEMMwmViULEcw310oqRIe3dQGI4JNcZJMqqTEMbk0Q4sSBpAwqJ568aCKiorGxsa+wyBJkkuXLpXL5XFyfXAMy1Qrwq89axZIqoRHPthqtYV/wbI1yhgeTTdo0KAZM2YEi4QGgwGQ8EZ5PJ7Vq1cHBYMzZ84cOHBgnMDgt4+SGsGj1GSxCyJVwqP72mF3hs8suRpVDA8jCSdKCEgYMgwSBLF06dI4iQxeF5pUCS2MVAlffJBhWIPThYIHFbH9yg4NCXt6et59911Awusw6Ha7+/4jDzzwQHl5ORkfg27+5YMqBD7IcZwgUiV8MQ2Dy80w4V4sHMOy1MrYnk4XMhKuW7fOYDAAEn700Uf19fV9h0Ecx+MqMnhdMrFIHzepEr74YJfdGf6lSpRJhd5zMHJI2NvbC0hI0/SqVauCgsFp06YNGTIk3mAQi7NUCV98sBNFcDBFISUwPOYXaMhIuH79+jhHwj179ly9epUNJnL//PPPKxSK+LxcSFIlBocLeLDvPoggOKhXyuPABsNCwo0bN/p8vvh8qr1e78qVK4OCwSlTpowYMSIOYTAgJKkSk4cGH+z7vtgR/qVKVcjiwwZDR8J33nknbpFwz549dXV1AINB+CCKVInXz7h9fp4vON74oMOFIdgXx4sPYqGeODYajZs2bYpDJPT5fKtXr3a5XH3/kUmTJo0ePTpuYRDDMJlYlCiThOmEHIZZPB6ev3p54YMMy9o83vCvU4pShuHx4oQhHy+JTyTcu3dvdXU1wGBQwjFMK5WEH3M3u73gg7eX1eMNP4IgJUk1ReHxtExDRsINGzbEFRL6/f5g08Tjxo0bO3asSCTC4ltaKRU+WpjdHp5njHnhg0jazCTKpCQRVzYYOhJu2LChu7s7fpDw008/vXLlCsMwQcGgUqnE4l4aCYWHzYMWDx3+RPK48MHwf4lOiuCGxQkSmkym+EFCv9+/cuXKoCKDo0ePnjBhAsAghmFaCQIeNLlpDniwL9gc/utCI6XizwYxkUj08ssvh4CEGzdu7Orqigck3Ldv3+XLl4OFQZVKBSaIYZhOJgn/gJbZ5WYhPnh7HnTT4VOzRkrhcblSy8vLQ6glNJlM77zzTswjIcMwK1asCAoGhw8fftdddwEMXt8Xh+8RFhRZ0DjgQY8n/MuklVLxuVJDjhJu3Lixs7MztpFw//79lZWVAINh4UX4POj2AA/2iQcR+KBEgsfrYg0NCS0WS2wjYQgwOHjw4ClTpgAMXpeaEofvEU6fz+tn+Pw1eeGDDq8PCz8+KKHip3gQFRJu2rQphpHwiy++uHjxYlAw+Nxzz6nVarC/6yIJQiUJN+LEcZjD6+PzMuOFD3r8/vDjg2oJFc/rNWQkXL9+fUwiYQAGnU5n33+ktLT0vvvuAxj8wU4LwdbY4+f10Tpe+CCNgpmlYhKP48UaDhK2t7fHHhJ+9dVX586dCwoGlyxZotFocDye19FNpKBE4V8Rjw988HZy+8JtwYpjmCSeZkcgREKr1Rp7UUKWZZcvXx5UZLCoqGjGjBkAgz+UhAx/hCdGMwyfK6n73zs4jnOH3RxUQpI4Ee+v8ZCb0GzevLmjoyOWkPDgwYMAg6gkFZEYFv6+mOHzkZL+90E/wyI4XCwiYP1iofYltFgsa9eujRkkZFn2rbfeCioymJ+f/9BDD4nFYlhCPxQlImFfHPlNMYMgOEjFcXMkVEjY1tYWG0h4+PDh06dPBwWDixcv1mq1AIM3hwySxGBfHGl5fAiSxVIR+GBYSGiz2WIjccxxXLAwmJOT89Of/hRg8MckQcGDbh/si28pL4ohLlKRCMPgZQ5IiB05cuTkyZNBweCiRYsSEhIABn/UB0kCxWMOddS3FJIDNxAe/CESBpv6tNlsQo8SBmDQ4XD0/UcyMjIeeeQRgMFbSEwS4XdygnN1t1u7KGgZx4EGESDhe++919LSIlwkPH78+DfffBMUDD7zzDOJiYkAg7d6uFA8WxzGQZ7k1i8KvtyqGEPC6dOnh4CE69atEygShhAZTE1NnT17drAvjHgTgeIlAf2oo8KDsFp/gIQhHC/BMGzLli0CRcITJ04cO3YsqEH1CxcuTEpKAhiMzlsK8sX8/gQxqsDxkjiJEnIct2LFiqAig3q9fs6cOQCDfbi2SKASeDAa0QcQSiRsamoSFhKePn368OHDQcHgU089pdfrAQajtGPjdwifBz6I4upw8TmZ/HYqKysLAQntdruwooQhwGBCQsK8efMkEgkskujwIM7v4BUP9sUojNAHNvgjSBhC4hjDsK1btwoICc+ePfv1118HBYNPPvlkamoqwGBfxHAsTx7zWPZBMYmgWt3rZ2BzfFOFFiW02+1r1qwRChKuXLnSbrf3/e/rdLonn3wSYLCPohkER0HEBK9PfPW/DyI5Eufhd9dvISLhtm3bGhsb+Y+EZ8+ePXDgQFAwOG/evPT0dIDBvvqgH8WJLzHJ5wAhD3xQLAr/+ngY8EHESOhwOASBhKtWrQoKBtVq9VNPPSWVSmFhBAEZYRuhhBRBnuTWwEyEHzug/QzsipEj4fvvv9/Q0MBnJLxw4cKXX34ZFAzOnTs3MzMTYDDIfTESHgQfvMUnwHFJ2F2zaD8DGeNIIOHbb7/t9Xp5+71Wrlxps9n6/veVSuXTTz8NkcEgedAffvBdKhKBD95GMnG414jFMB/DwpJFjoTbt2/nLRJWVlZ+/vnnQcHg7Nmzs7OzCQKK96O92ZKISD6Pk+TFgpBAqoSvSOh0OnmLhMFGBmUy2cKFCyEyGPSTxbDhxwdlItgX9+EahR9DtXt9sDGOBBLu2LGjvr6eZfmF21VVVfv27QsqjfPYY4/l5eUBDAb9ZNFeFDzI6wFYvFgTcrE4/Gpzi4fGIEQYMSTkW+J41apVQUUGJRLJokWLAAaDFcdxVtoXfvdAuViEw7741tLIJOFfISsNPNgnJAwhS7Bz585r167xBwmrq6s//fTToKz5Zz/7WUFBAcBgsKIZNvxW0hRJhJ8DiH0fTJAiyN9ZaC+s2r4gYQh9CfmGhKtXrw4KBimKWrx4sUwmgwUQrGy0N/wqao1UAufqbi+tTBr+VbJ6aODBviDhSy+9FAISfvDBB1evXuUDEtbW1u7duzeoNPFDDz1UVFQEMBjSY+UN/1CdVirBwQdvK51MgiOID3rhhHFfVFZWFhoSrl69mg9I+Pbbb1ut1r6X8ohEoueee04ul8OtD8UHaW/4UXetTAL9B/vyukAQvbbQXg6MMJJIWFFRUVdX179IeO3atd27dwcFgw8++GBJSQnAYD8+VhqphOeTM3jCg1T4F6nX5YEjJRFFQpfL1e9IGCwMEgSxZMkSiAyGLKOLDj8+qJVKcODB20otlYT/tnD7/HavHxZuRJHwww8/rK2t7S8krK+v37VrV1AwOGPGjLKyMpIk4aaHJoPLHT5eaCFP0hdRJKmiwh0gy317z2DpRhYJV61a1V9IuGbNGovFEhQMQmQwXB90ulkkeRLYF/dFeqU8/BdGt8MNIcJII+FHH31UU1MTfSRsbGysqKgICgbvu+++wYMHAwyGt8dC0Otdr5RDnqRPSlMpwn9jdDtc4IJRQMJ+iRKuXbs2KBjEcXzp0qUKhQJudMjqcXmYsF94EpLUSCmom+mTUlXK8K9Tl8sDOBgdJKyuro4mEjY3N+/cuTMoGJw6deqwYcMABsPdFKOAQTHJ92Q9f3hQHv4bw+Bwwb44Oki4atWqaDahCRYGMQx7/vnnAQbDfaBcCIKDqUo5wfuut3zxwXQVgiXb4/J4/NCFMBpIuHv37qghYWtr644dO4Laid99990jR44EGAxTHXZX+B0WUlUK8MG+KlkpJ8O+WAzHddgdUEUYBSR0u92rV6+ODhKuX7/ebDYDDEZZHMe12hwIfBB4sO+iSDJJIQv/ajXbHOCCwSJhaE1o9uzZc+XKlUgjYXt7+7Zt24KCwQkTJtxxxx0Ag2HK5vVaPAgO1aWgCHnFiw/iGJaqlIffubvZ4gAcDFahNaGJTpRw/fr1JpMJYDD6arO5GBTPUooCeDAYZWlUCHjQamchVRItJPz444+rqqoih4QdHR3BwuDYsWPvvPNOEb+7HwtCrTZH+D6YJJfJKDH/ZwPyyAdzEzQIUsZOjwtO14WEhCG0qna73RFFwg0bNvT29gYFg0uXLlUqlXBDw1eLFUFwMEenEhECGJHKIx/MT1CH/2kYjmu3OyFVEgISvvDCCyEg4d69ey9fvhwJJOzq6tqyZUtQMDhq1KhJkyYBDIYvjuNabA427OrBXK2awMEHg0RotW+Ee78AACAASURBVIQK//fUm21QOxNNJFy5cmUkkHDjxo3BwuBzzz2nUqngVoavHpfHjmLQRa5OTeIC6HjGo49I4HiOTh3+1rjOZAEejCYSfvrpp5cuXUKLhAaDYfPmzUHZ67Bhw6ZMmQIwiETXzDZ/2DCIY1iWVkXAvjhY5ekQbI0bzA6agVnGwkbCjRs39vT0BBsZVKvVcBOR6KrJynDhvthSlHIlReFC+L4880EUqRKaYZotUE0dVST87LPPKisrUSFhT0/Pu+++G5SxlpeXT506FWoGkYjluGsmW/jJ4mytWhBJEt75YL5OjeSy1RqtECKMMhKuWLECFRJu2rTJYDCEAIM4jsMdDF/dTheSsSR5OjWBC2McAr8+pVYmRXKqpM5kBR6MMhLu27fvwoUL4SOh0WgMFgZLSkruv/9+iAyi0jWTzY8C7fMTtCTwYCifBsdLknXhv9WbLHaXD0KE0UZCJFHCd999t7u7O6jX2JIlSzQaDcAgKtUZreFviuViUbZWSeLggyFpYHJC+JfOx7K1RgsLSBhdJNy/f3+YSGg2mzdu3EjTdN9/pLCw8Cc/+QnAICp5GabOZA2/crAgQSsVzk3hnw/qEwkUowwudhvBBqOPhGFGCTdv3tzV1RUUDC5evFir1QIMIoNBk83p84f/6JQk60jhzErl3QdNlEtTVfLwF/XlHhPNwAG7fkDC8+fPh4aEZrN5w4YNQcFgXl7erFmzAAYRqrLbGH7lIIZhJfoEoSSL+eiDBI6XJCeE/3p3ev3XTHbIlkQZCT0eT8hIuHXr1o6OjqBu2bPPPqvT6QAGUcnPslU9pvBnkqgoKkOlJHDwwTBUqk8Mf2VzGHah28iCDYaBhKE1ofn888/PnTsXLBJaLJZ33nknKBjMzs5++OGHxWIx3CxUajDbbCiO0xUmaSUiIdVy8tEHS5ITkHysSoPRx0LWOHSVlZWFhoRvvfVWsEi4bdu29vb2oGBw0aJFiYmJAIMoN8UGE5pNcZJOJJzgIE99UCOlchPU4S9vK+1rtMDWuB+Q8Msvvzxz5gzT59ONNptt/fr1Ho+n7/9ERkbGo48+CjCIUCzLXTKY/ByCysGylESSENL7iY8+SOD48HR9+FljjuMudpvgYEm/IOGKFSv63jLr/fffb2trC+qN9fTTTwMMolWzzWFG0Yg/S60UxEwSvvsghmEjM1KQLPGL3UaGBSfsByQ8cOBAH5HQbrevXbs2qMhgamrq7NmzKYqCG4RQlwwmH4PgYRmankyJBHbQm6c+mK5SpqsV4Ruh0U23WGFiSf8g4fLly/uChNu3b29tbQ0qr/LUU0/p9XqAQZSbYo67ZDAiGUgyLF0vrOAgf32QJPDh6QgWOstxJzsMcLCkX5Dw4MGDp06dujUSOhyOtWvXBhUZTEpKmjdvHsAgWtWbbd1OT/hPil4pz9SoCKG9ovhr26MyUpBczVPtPQ6vDxZ6vyDhW2+9dWsk3LFjR3Nzc1CRwaeeeiolJQVgEK2Ot3V7kWyKU5MkAux+xl8fzNaqklH0nnH5/Gc6ewAJw0TC0I6XfP3117dAQqfTGWxkUKfTPfHEEwCDaGVy05XdJiQ9ZoanpwhuU8xrHxST5LD0ZCQF1YdbOv2QLQlP5eXlM2fORFtLWFFR0djYGFRk8Mknn0xLSwMYRKtTHQa3H8Eh1ASZJFenFlbFDN99EMOwkYi2xl0Od00vdCTsNyQ8efLkD5HQ5XK9/fbbQcGgVqtdsGBBCJ8BdAt5Gfabtm4fClAYnJosFQvyrDevfbAwUauVIlj0DMcdae1kwAbDUzi1hD9Ewg8//LChoSEoGJw7d256ejrAIFpdMpiMbhoJJAxP14sJQd4dXvsgRZJD0pKQIGFVj9ngdIMT9gsSHjp06MSJEzciocfjWb16dVBpYrVa/fTTT0ulUrgRCMVx3NGWLiRlgyqKKkzSEgIMDvLdBxFujb0Me7ytG7IlYQpVlHDXrl319fVBweDs2bOzsrIABtGqxeZstNqRlA2WpybKxSKB3h6++2CpPhHJxBIMw75p63b7oCNh/yDh4cOHv/nmmwAS0jS9atWqoGBQoVAsXLgQIoPI9U1bl8ePphfJ+Ox0MSHUeYF890GJiJyUm4EECW2093xXLyBhmCorKwuhDz5N02+99VYgK7J79+5r164FBYOPP/54Tk6OQPdcvJWN9p7t7EVy8DRbqypK0pGEUGldAAtrYl6GBEXDYQ7DDjV3wnHj8JHw+eefDwHNjhw58s0337hcrlWrVrnd7r7/oEwmW7RoEUQGketMZw+SFvwYhk3KzZCJBdwVXAA+mCCTojpb0mpz1kMnrv5DwuXLl1dUVNTW1gYFg4888kheXh7AIFr5WfZYazeSboNKSjwqM1Uk5BskgI+O4/iUwiwkPshw3OEWKKDpNyQ8duzYX/7yF5fLFURgRCJ59tlnAQaR60qP2YDiQDGGYWOy0tRSiaAzWMKw8PwETWGiBsmFvtBlbLU5wAn7CwmDnUDy05/+tLCwEGAQrRiW/aKxnWYQZEgIHJ+Un0GRwr5Bwvj0IoK4Kz8LVQHNZ9da/AxECfsHCYMyQYqilixZIpPJ4IKj1fkuY7PVgQQGy1MS0wU1kknAPohh2KiMFB2izdHlHnOdCY7ZIUDCEGoJg9KDDz5YXFwMMIhWXob9vKENVbnM5LxMiYgU+jURzAqTU+LxOelIXjs+hv3karMPZtmFjYSh1RL2/fcvWbJELpfDpUarb9q6OxwuJDCYppKX6hNEwn9RCekLTM7PFCMKQzRa7FBLyHMknDlzZmlpKcAgWjm8vi8RRQYxDBufkyGnYmFUlpAWmV4pH5aejAQJ/Sz32bVW2g9RQp4iIUEQzz33HEQGketQc6fR7UECADIROVbIZ0iE6oMEjk8pyEYVke10uL5phxPHPEXC6dOnl5eXkyQJVxihTC76SEunD1GScFRWaoJcEhsHvgW26ShJSsjSqpBceZbj9te32qFlP/+QMACDEBlEri+a2qy0D9Wbf3JeJhUrLyqB+aCYJO7KyyQQHWM0eehDzZ0MICHPkHDatGlDhgwBGESrdpvzdEcPqsbsJckJ2Vo1ESvtf4QXhB6bnaZCNJ6C47CDTR0mtweMkFdIuHTpUoVCARcWoTiO+2d9m92LEAYzpKLYeVEJzweVEureohwS0YvI4fV92dDOQg0Nb5BwypQpI0aMABhEq2smW1WPiUG0znN16iFpyWQMpfKF901wDLunIDsZUVNCDsOOtXV3OJxghOEjIZJTwC+88ALAIFqxHPfPhlYXuuabPynJV0moWOqIK0hHV0qpGSX5qKKEHj+zq6YJTtrxAQknT548atQogEG0OtluuGayoYqDl+kTB6UmiWKrrlOQXwbHsHE56dkaNao3Uk2v5UhrF9TQhImEzz//fJhIuHTpUqVSCRcToXpc7k+vtiAZy4lhGI5jMwfmx0bttOB9EMMwuVg0q7QA1WEDH8t+Utfc5XCBEYaj0tLScJBw/PjxY8eOBRhEuSNmuY9qmnoRFU5jGDY6M7UoUUvG3JQYAcPt0PTk0uQEVIN7bF7fzisNPjhhEobEYnE4SAgwiFzH27qresx+ROkRiiBmlOQJuu90DPogRZKzygoQvppqjNbDrR2wO+4XJLzjjjvGjx8f0e418aZup+uzayjTIxPzMjPVKiIWRwYKO9hZnKQblZmC6sb4Wfazq63tNsgd9wMSLl26VKVSwQVEJYZlP6xuMnqQ1cYqKfG9RTmxVDMYOz4oIoiHBhYgPNwT2B17EbVmAyTso0aMGDF58mSAQYQ60tpVY7Qw6Apj7ynI1ivlsTo/WvDJ70yNaiKiwZ4BXTXbvmpCuTumKCrY1UMhOjDTX0j4wgsvBBXpe/HFF9VqtUC/bwj3F8MwiUQSOU/psLv2XWtFOK07SS67qyCLEsVsCkvwPkgS+PSSPCWFDCX8LLuvvrXFimyGiUajCfZH1Gq1oF+8paWl//Ef/9HHk3bz58+/5557hAuDUqk0hDOFkbvFfpbdVdNo9ngRhnfuH5CTIJPGJgrGhg9iGJaskE0tzEGYMHH6/DuvNNCoGpdPnhxsLcjEiRMFXT4iEonmzJmzaNEisfg2hWazZ8/+3e9+J1wYxDAMx/EJEybc9pv+8BZHyPoPNXfWGq0Iu4dkaVRjs9JFZCw3xI2F70bg+NSiHJ0c5fuq3mI70NiOZDE98cQTQa14giAeffRRoZfRKZXKf/u3f1uxYkVWVtZNwSc1NfVPf/rTH//4x7S0NKFHnebNmxdUKEMikcyaNStY6+yL2mzOzxvaPH4/wt85syRfLaViGAYxDIuRyLRGKpkxIO+9C9WoAsMMy33e0FaUoClKCHdeaFlZ2ZgxYw4fPtzH4eWTJk3Kz8+PgX70Op1u1qxZI0eOPH369PHjx2tqajweD0VRxcXFw4YNmzRpUkZGhkqlioHQ+5133llcXHzx4sU+3uJp06ZlZWUhv8UeP1NR02hBuiMuSdYNTUsWxfp0BDxmxra5fL7Xj5yt6TEj/EZpSvmLo8uS5eH2dDhz5szs2bO7u7tv+9kSEhJ27tw5atSomJnLwXGc2+12u900TXMch+M4RVFSqVQul8fS7JGDBw8+/fTTRqPxtrc4PT39/fffHzp0KNqvz3Lc9qprx1oNqGaPYBhGkcS/TRxVqk8g8NjGQYz8wx/+EBvfREyS2RrViZYOH4vsTIjL52+3O4ekJIRZmpOcnFxaWvr111+7XK5bUa1G89Zbb4UQbOL1mxbHxWKxXC5XqVRqtVqlUsnl8tByrHxWenp6fn7+kSNH3G73Lf6aXq9fsWLF6NGjkd/iA40dB5s73Uh3xA+W5o/JTqPi4KRj7PgghmFqiYQgiCs9RlREyGGYxUPbaG+ZPiGcPAxJkpmZmXfccUdbW1tLS8tNkWHMmDF/+9vf7r77bhhOJESJRKK8vLxhw4Z1dHS0tbX98BYH0imvv/76nXfeKUU0ifu6LnYbP6ptstNehL+zKFE7Z0iJSiLB4+D24TE2ztzl9b157PxlgxHh95KQ5Iyi7GkFmWGmpH0+X29vb11d3a5duw4fPmy1WnEcVyqV48eP/+lPf1pSUpKUlBS5ccCgKMjr9RqNxrq6uo8//vjQoUMWi4VhGJVKNWbMmEceeaS0tFSv1yMnwTab8+2zV7pdboSPskRE/tuEESX6RBKPBxuMOR/EMKzJYvvvgyftNMoBTAqxaP7g4mGpieEHSnw+n81mczqdgZg6QRByuVyj0cTSXjjO5fP57Ha70+lkGIbjOJIkZTKZRqOJRHm8jfauPnOl3mJHey7+kfKi6SV50rg54RODPshw3OdXm7egyx0HlCCTLB1RmqNRxccLEiQE/GTZjedrL3QbEcbEMQwrSda9NHaoTi6Ln5Ueg+lwEscn5WUMSklCm+Qyu+nNlVfNHhjqBOKFOI77uLbpco8JrQnKxKLHBw3QxvTpkbjwQQzD5GLxnKElKqRdczkMa7M7t1y65vH54SEE9buOtnYda+32oO4J8tDAgrwEDRFnu56YLY/MVCt/NqiYJFDeTpbjqnstu2thmAmon1Xda9l7tcWBbg5nQOX6xIm5GTHcTyHufJDA8Qm56cPSktG+2Xwse6yt+3BrJ7RrBfWXuh2uHVX1aDspYBgmp8SPDi7WyCRxGACP5eMyUpHo8SElagniJJ3Hz3xc13K6swesEBR9mVz05sqrnQ4X8gznT0sLcrRqIi7zgDF+bDBdpXh0UDHy05EOr29HVT1YISj6JvjOxdp6i51BvfCGpCWPj8sdcVz4IIHj43LSR2bokb/lbDRYIag/TNBs87OIw9Mqinp0ULFaQsVtSRgR899QKiIfHzwgRSlHfo9ttG9HVcMZsEJQVExwQ2RMkMDxxwcXZ2mURBxXxhLx8CVTlPJFowYpIzB82kZ7t4MVgqJigtciYIIYhs0YkHdHfDRTiHcfxHG8OEk7f/hAcQR66oIVgoRrgqMzU+8bkKug4v1MJxEn31NEEKMz034ysCAS58bBCkFCNME8nfrxwcU6qQROihLx81UlIvL+4pwx2WkEWCEo7k1QJ5MsGFGWolLgcGA+rnwQwzAlRc0dUlKYqInErQcrBAnFBCmSWDC8LF+nIcEE49AHMQzTyaXPjCxPikwvjW+tsAOsEBSWjC5P5EwQw7DHBg8YnJokJgm41HHqgziGZWpUz4wsl4kj0lvNRnu3X2n4vKE9QisYFPNqstjXna+JnAneW5QzITdDKhbBpf6XLXBxSS4+lj3c2Lbh7BUmMktNIRaNTE9+uCRPDqsNFIzOdvZ8VNPU4/IwkXkwh6YlPTNyUKJcBhti8EEMwzCP319x+do/axsjtOAkIrJYp5k7qDBRLoUlB7qtGJb9vKHtYHOn2U1H6JnMVCtfunNYpjquS6bBB78vO+1dc+rSuQ5DhMJ5IoJIV8nnlhfma9Ww8EC3kMvnr6huON9lRN5K67pUEuqVccNKknUkAWFB8MH/XwaH641j55rNtghdBQLHE6SSWSW5o9KT4SUMuql6XO5tl+uvmqzIm6re+Epecseg0ZmpcX5uBHzw5uI4rMFk+fvx871Od+QuhFpC3ZWTdl9BFmToQN/TNZN1e1VDu93pi1hi7f+1d6Y/bhzZAe+qru5mXyRH5JBDcu5L0kiypQ0iy7GhrL22EwGLRXZhOwmMIPmjsvm4QRAESJA4ycZ2sAsj3mCz9lryyseORiNpLs1BzgzPJptHH3XkA7WxgUSx4ojXzPsBQzQwH2ZQx4+vuqreQ5L0R8+efXVx2oBaYODBx8G42CjXfvjx5+V2D2uPGAq5nE68uTJvqTAWge53sPg4X3xnY6/c9np30ApJ0huXll9dnLZOcToZ8OATqpBvlJ1eq1CV5YUx+62Li71IfgOMFpTzdzf2Ptw/djy/d0OuK8FXFqdtkCB4cHhUKGM0Yep/uLJwLhmH14WnlrofvL2+80Wx0g4oSBA8OIwq/POPP6/0UoUISXFNvZZL31iYMlQ4XXjq1sJfHFff29wruG2fsR5ObJAgeHDIVShJkk7IpG1+//zM0lgMLrqfEtwgeHdj77OjiuMFPb15iSTp9YtLry7NgATBg8OuQhmhqKa+ODXx2nxOh2snJz0MXCvV3tnYy7ut3h2OAQmCB0dShZIkGQqZjprfPzc3H7chMDyRtILwvc3924elWo/DQJAgePDpq/BB2flhX1QoIxSLqNenJl6Zz0UIBIYnivVS7V8e7B647Q6lPZ/MIEHw4EirsBsYzsasH5ybm4lZEBieADoh/cnW/s18qeb5rPezDCQIHjwhKiQYxTT1pdnsSzNZjcD9pxFmo1r/5/sP9xutTkj7MHJAguDBfqjwL3p8rvCrmAqZG7NvLEwtnYnBGcORo+4FH+wWbuaLNc9nvB9DBiHp9QsgQfBg71X40Gn85e27W9V6f5qLYGSr6vlk7LX5qZxtggxHZSH8i/2jDw+Oqx2/P2GgJEk6kf/48rnnpzJwbQ482HO4EMVm+29XH9zaP+pbzn1VxlFV/VYm+fJsFpIYDjOU808KpX/fLRy3vFYY9m1KjZv6n15ZWUknIN0veLBPCElyOt5PN3bfvbdNef8aTSdyTFOvTaZ/dyZjQ46G4fuCXC1WP9gpHLgtNwj7WZdmORn/k8vnZ8aikEoLPNhvmkF4a//obz6/1wrDfv5dUyEJXbs+nX1+MgVbKEPxvSjEtuO+v32wXXMbQdDPr0ZJkl6cyf5gZTFtmzKGdQJ4cBB4lN0vVX90e63YbPez7RCSLEWZsPSXZ3NXJhIEsgoPjkO3/f7Owd2yU/eCsL8FuTBGf3B+4TsL02d0Dc5XgQcHCeX8oO7+1afr98q1PjegjJClKtMx69W5yXOJGIZwoL/UOv7Pdgu3D8uOF/Q0UcJjlgXKW5fP/fZkGnZFwINDARei3Oq8vbbxHw8L/a9WTDC2VWUmZl7NpZ5JJyLwhqj3q+C9RvNmvrRarDp+4PVrO/irTNjGn125cHZ8DG6jgweHaW5IUsPzP9ja/8e7myEbQLVigrGpkDFd+9ZE8mp2HPaUe0HA+GqxcrNQ3Ku3WkHoUTaQCbOSSrx1+ex0LAqlHcCDw0g7CG8Xin/92XrDDwbyD2CEdCIbCllJxp/LpRbGYvDu/KlQ7fi/KpR+dViqen4roH1+D/hVXpqf/N75hZRlyPBCEDw4tPiUbVadH91eK9SbA2xNTZZNlWQs42p2/MpEEs6UfeM3HtuOeytfvFuuNQPaDikf3BwhGL9+cen6XG4sArsi4MGhh3J+7Lbfub/98508H2iTEowMhcQ09XI6+VwulTZ1mD5PiEfZF8eVm/li3m23wrDXiQK/lsmY/ealpZVUwlQV6EPw4GgghNTwg9Wj0t+tPuhpFdAn6ldJ0hViKGRpLPbsxJlziTjUyXscjPO9evNOufb5UcXxglZI6eCWwI/edWD0yvz0a0szacuAF4LgwdEjYOzYbf94ffujvQIfgrZVZawTohN5Lh5dGY+vJOPRiAbBhSRJIec7NfduubZedhwv6FDaoWwYpkPGNt+8tLySOhPVVFgLgwdHNzAUdT/4rFD8+9WNWscbkvbtCjFC5OmouTI+dj45djr3l33KNmv19bJzr+I0/NCjzKOMD8cswAh9e27y95dnJmwTbsuBB0/IfCu4zR/f3bp1cMyHqZEVjHVFjsgkFzXOJeLLiVjWNk/8RmTdC7adxnrZ2ajWmwH1KPUYG6qxn7aMNy4tXUglYxEV8q2BB08OXIi6F9zOH//DnQeOFwzbv6dgrBE5QmRLITNxeyEenRuzU6Z+YpzYDMKHjrvjuFuOW251PMY8yvwBHQD83yahJF2fy91Yns3YFlweBw+eTDxK843mP61tfloo8aFsbYQkVZY1WdZkHNXU2bg1FbVytpmxjMhITUsuRKXj5RvtvNvacdyjVsenzGcsYKzPeRCenKShv3Fp6ZmJZCyiQRgIHjzxgaF/a//o7bXNQR23fkIwQqqMVRkrWFZlPG5EcraZixpZy0yZkWErI8U4r3nBYbNdcFsHjdZhs92mLGAsZNxnfODbvl/LCzPZ756dy0YhDAQPnho6IT2ou2+vbf76qMxHpNkJxgrGqowVGXezZCcNLanrSUNLGnpS12IRtT+Zb4QQ7ZBWOn657ZXbXrnjldteteMFXISMh5x39Tcqo/mMHnn94uLlTCquQxgIHjx9gaHT8VePyu/d39lvuCPX9hghghHBmGDcfZARshRiaUpUU21VsVUlqqmGQjRZVgnurrW7i+7H3fbjQlDOfcp8xgPGfMp9xjzK3CBoBKHrh64fNIKwGYQh41QIyjnljz4Z5yM3fHUif3t+6vpcLmObUKYVPHh68SlzPP+Tg6OfbOxWBn3i+qnIUUYIYyQj9OgZIYQkjBCSJIQQliSEEMEIIyRJqOtDIUlCCCEkyjnvPksSF0IISUiCC8GFYFyw7oMQnI/8SCUYPz+deWVhKhO1LFWBMBA8CEidkFbanQ93C+9v7TX98OQPtf/+JP7r4+RzJTv+e4uzM2PRaESFdAngQeBLhCS1grDYbP9se//nO/n+5/UE+sByMn5jeXY5ORaLaJBFHDwI/M9wIZpBmK+7P93Y++TgiEGPnBRytnXj7OwzE8m4rsH9EPAg8PUwLhp+sFOt/+uDnbvFKod+GWXGdO21xZmrUxMJIwKbIeBB4P9GyHjD9+8Wq+/d39l1XOidkcNQyMvzUy/O5sZN3YB8WeBB4BvT3VD+NF/8t629vNuELhoJdIVcm5p4aX4yY1uWqkCqGPAg8BTohLTh+Q8qzke7hV8flxmHnhpSMpb5OzOZ38qmz5iRqAZZEsCDQA9iw1YQHjVbN/ePfrl3OOTX8k4VGKELqcQLs9nlRDwa0QyFgAHBg0APYVy0wrDe8b44Kn+4W3gIrw4HiqUqV6cmrk1lsrZpa6pKZPAfeBDoE0KSOiFtBsF2tf7LvcNPC6UQjhz2l5m4/fx05nImNaZrpqrAeUDwIDAwAsZaQVhqdm7ljz7aO6y1PejFnkIwfjaTfGE6O5+IR1VFVwhsg4AHgaGAC9EKwoYfrB2Xf7F7uFlx4NThUyeua9cmM89NTaQtw9IUOA4NHgSGlE5Iu/fz1oqV1aPKZs3hsLn8/yNp6BfSiUsTibl41I5opqI8Ll8OAB4EhgjGRYfSTkirHW+9WLlzXLlXrlHGoWWenIxtXkwnLqWTkzHLUBVDIYoMeyDgQWA018teSDuUOp5/r1i7c1xeL1UHXpV8mJmO2xfTiYvpZNY2DUXRFQJVg8GDwMkRok9ZJ6SuHzyo1O4clx+UHMfzocslSVIwnh2LrqQSF9OJlGUYCtEVAvu/4EHgxCKE5DPaDqkX0uNWZ6vibFWdjUq95QenqvtljKdj1mIivngmNh2PWpoSIcRQiAz6Aw9CK5wquknwPcq8kB66rc1qfatS23Vc94Q6UcE4GzXnz8QWE/HZeNTWVI2QCJFVGcPZFwA8CEgh6xYDoQFltY5/UG/u1939hntQb7aCUU2RjTHKWuZkzJqK2ZMxO23puqJEiKwRWZNlcB8AHgQeC+MiYCxgLGA8oLTS9g4azeNmu9hsl1qdUqsznOmyEZLGIpFxU09ZRsrUc1ErY5uGqqgyVmVZlWUCB14A8CDwjZfP3XqYIeOU85Axx/O7Qiw129WO3/CChu+7ftC3gzk6kW1Ni0bUWERLmfq4qY+bRtKIRBTSrS9KflN5GWI+ADwI9CpgDDmnnFPGKedMCMYFZbwZBnXPb/hhw/MbfuBT5lPms+4n9UPuMRpQRkUXSUiSkASWJAkh3K0cT2RNJhrBGpFVmUQIVmU5ohBTUaIRNaqp0YgWVZWIQmSEZIxlhIiMlUflQyHJCwAeBAbuRyEY549qbHLBJSGE4EISovvcrcb55TgT3R/0Zd267oYFRl0kVAwXHAAAABxJREFULCGEEEYS/o31ZIzk7u8AADwIAADw1PlPauOk/2qkeugAAAAASUVORK5CYII=';
  locale: string = 'en-GB';
  exportUrl = 'https://export.highcharts.com/';
  datesValid: boolean;
  reportObj = {
    firstname: '',
    lastname: '',
    from: new Date().toISOString(),
    to: new Date().toISOString(),
    dateOfBirth: '',
    gender: '',
    weight: '',
    text: ''
  }

  agpChart = {
    title: {
      text: ''
    },
    xAxis: {
      type: 'datetime',
      opposite: true,
      crosshair: true,
      tickInterval: 3600 * 1000,
      labels: {
        formatter: function () {
          return Highcharts.dateFormat('%H:%M', this.value);
        },
        overflow: 'justify',
        rotation: -45,
        step: 2
      },
      plotLines: [{
        value: 0,
        width: 1,
        color: '#e6e6e6',
        dashStyle: 'solid',
        label: {
          text: '',
          align: 'center',
          y: 12,
          x: 0
        }
      }, {
        value: 0,
        width: 1,
        color: '#e6e6e6',
        dashStyle: 'solid',
        label: {
          text: '',
          align: 'right',
          y: 60,
          x: 0
        }
      }, {
        value: 0,
        width: 1,
        color: '#e6e6e6',
        dashStyle: 'solid',
        label: {
          text: '',
          align: 'right',
          y: 60,
          x: 0
        }
      }, {
        value: 0,
        width: 1,
        color: '#e6e6e6',
        dashStyle: 'solid',
        label: {
          text: '',
          align: 'right',
          y: 60,
          x: 0
        }
      }]
    },
    yAxis: {
      title: {
        text: ''
      },
      gridLineWidth: 0,
      minorGridLineWidth: 0,
      plotLines: [{
        value: 180,
        width: 1.3,
        dashStyle: 'dash',
        color: '#9e9e9e',
        zIndex: 5,
        label: {
          text: 'max (180 mg/dL)',
          align: 'left',
          x: 5
        }
      }, {
        value: 70,
        color: '#9e9e9e',
        dashStyle: 'dash',
        width: 1.3,
        zIndex: 99,
        label: {
          text: 'min (70 mg/dL)',
          x: -5,
          y: 15,
          align: 'right'
        }
      }, {
        value: null,
        width: 2,
        color: 'blue',
        zIndex: 1,
        dashStyle: 'ShortDot',
        label: {
          text: null,
          align: 'center',
          x: 5,
          y: 20
        }
      }],
      min: 0,
      max: 220,
      lineColor: '#e6e6e6',
      lineWidth: 3
    },
    plotOptions: {
      series: {
        marker: {
          enabled: false
        },
        fillOpacity: 0.2
      }
    },
    legend: {
      align: 'center',
      verticalAlign: 'bottom',
      floating: true,
      x: 0,
    },
    credits: {
      enabled: false
    },

    series: [],

    responsive: {
      rules: []
    },

    options: {
      chart: {},
      tooltip: {
        shared: true
      }
    },
    noData: 'No data'
  };

  pdfObj = null;

  userData: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private plt: Platform,
    private toast: Toast,
    private file: File,
    private base64: Base64,
    private fileOpener: FileOpener,
    private userProvider: UserProvider,
    private remoteService: RemoteProvider) {
    this.getUserData();
  }

  getUserData() {
    this.userProvider.getUser().then((data: any) => {
      this.userData = data;
      this.reportObj.firstname = data.firstname;
      this.reportObj.lastname = data.lastname;
      this.reportObj.dateOfBirth = new Date(data.birthdate).toLocaleDateString('en-GB');

      switch (data.gender) {
        case '1':
          this.reportObj.gender = 'Male';
          break;
        case '2':
          this.reportObj.gender = 'Female';
          break;
        case '3':
          this.reportObj.gender = 'Other';
          break;
      }
    }).catch(e => console.log(e));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PdfReportPage');
  }

  createPdf() {
    //return new Promise((resolve, reject) => {
    this.remoteService.getGlycaemiaEntries(this.reportObj.from, this.reportObj.to).then((data) => {
      console.log('GLY ENTRIES:', data);
      this.agpChart.series = data['series'];
      //this.agpChart = Highcharts.chart('agp', this.agpChart);
      //resolve();
    }).catch((err) => {
      //this.entriesErrorMessage = ' NO ENTRIES NEW GLY';
      //this.entriesError = err;
      //this.agpChart = Highcharts.chart('agp', this.agpChart);
      //reject(err);
      console.log(err);
    });
    //});`
    var docDefinition = {
      header: { text: 'Generated on: ' + new Date().toLocaleString(this.locale), alignment: 'right', margin: [0, 5, 5, 20] },
      footer: function (currentPage, pageCount) { return 'Page' + currentPage.toString() + ' of ' + pageCount; },
      content: [
        {
          columns: [
            { image: this.logo, width: 60, alignment: 'center' }
          ]
        },

        { text: 'PATIENT REPORT', margin: [0, 5, 0, 20], alignment: 'center', style: 'header' },

        {
          columns: [
            { text: 'Patient', style: 'subheader', margin: [0, 0, 0, 10] },
            { text: 'From' },
            { text: 'To' }
          ]
        },

        {
          columns: [
            { text: this.userData.firstname + ' ' + this.userData.lastname },
            { text: new Date(this.reportObj.from).toLocaleDateString(this.locale) },
            { text: new Date(this.reportObj.to).toLocaleDateString(this.locale) }
          ]
        },

        {
          columns: [
            [
              { text: 'Personal Information', alignment: 'center', bold: true, margin: [0, 20, 0, 15] },
              { text: 'Date of birth: ' + this.reportObj.dateOfBirth, fontSize: 12, margin: [0, 20, 0, 15] },
              { text: 'Gender: ' + this.reportObj.gender, fontSize: 12, margin: [0, 0, 0, 15] },
              { text: 'Weight: 79 kg', fontSize: 12, margin: [0, 0, 0, 15] },
              { text: 'Condition(s): DM1', fontSize: 12, margin: [0, 0, 0, 15] },
              { text: 'Blood type: B+', fontSize: 12, margin: [0, 0, 0, 15] }
            ],
            [
              { text: 'Diabeties statistics', alignment: 'center', bold: true, margin: [0, 20, 0, 15] },
              {
                text: [
                  { text: 'HbA1c: ' },
                  { text: '6.05%', bold: true }
                ], margin: [0, 20, 0, 15], fontSize: 12
              },
              {
                text: [
                  { text: 'Avg. glucose: ' },
                  { text: '6.5%', bold: true }
                ], fontSize: 12, margin: [0, 0, 0, 15]
              },
              {
                text: [
                  { text: 'Glyc.var: ' },
                  { text: '15%', bold: true }
                ], fontSize: 12, margin: [0, 0, 0, 15]
              },
              {
                text: [
                  { text: 'Hypo: ' },
                  { text: '32', bold: true }
                ], fontSize: 12, margin: [0, 0, 0, 15]
              },
              {
                text: [
                  { text: 'Hyper: ' },
                  { text: '16', bold: true }
                ], fontSize: 12, margin: [0, 0, 0, 15]
              },
              {
                text: [
                  { text: 'Avg.read/day: ' },
                  { text: '5.47', bold: true }
                ], fontSize: 12, margin: [0, 0, 0, 15]
              }
            ]
          ]
        },

        {
          text: 'Ambulatory Glucose Profile (AGP)', fontSize: 15, alignment: 'center', margin: [0, 15, 0, 15]
        }

      ],
      styles: {
        header: {
          fontSize: 20,
          bold: true,
        },
        subheader: {
          fontSize: 14,
          bold: true,
          margin: [0, 15, 0, 0]
        },
        story: {
          italic: true,
          alignment: 'center',
          width: '50%',
        }
      }
    }

    if (new Date(this.reportObj.to).getTime() - new Date(this.reportObj.from).getTime() > 1) {
      this.datesValid = true;

      let data = {
        options: JSON.stringify(this.agpChart),
        filename: 'agp_patient',
        type: 'image/png',
        async: false //true means it will receive a download link instead of an image, false denotes the response as image/png or whatever is specified in the type
      };

      //console.log(data.options);

      this.remoteService.exportChart(this.exportUrl, data).then((res: string) => {
        console.log('BEFORE res');
        if (res) {
          console.log('AFTER res', res);
          // this.toast.show(`Chart received `, '2000', 'center').subscribe(
          //   toast => {
          //     console.log(toast);
          //   }
          // );

          //this.file.writeFile(res, 'agp.png', blob)
          //1. convert the png to a blob -> maybe not necessary?
          //2. save it to a file
          //3. get its new filename
          //4. save the new path/filename to the pdf definition
          //5. generate the PDF file

          //if cordova is available (e.g. device)
          if (this.plt.is('cordova')) {
            //res.getBuffer((buffer) => {
            //let blobAgp = new Blob([buffer], { type: 'image/png' });

            this.file.writeFile(this.file.dataDirectory, 'agpChart.png', res, { replace: true }).then(fileEntry => {
              //let filePath: string = this.file.dataDirectory + '/agpChart.png';
              this.base64.encodeFile(fileEntry).then((base64File: string) => {
                //let imageString: string = 'data:image/png;base64,' + base64File + '\'';
                docDefinition.content.push({
                  columns: [{
                    image: base64File, width: 250, alignment: 'center'
                  }]
                });
                return base64File;
              }, (err) => {
                console.log(err);
                this.toast.show(`Error Base64 Encoding`, '5000', 'center').subscribe(
                  toast => {
                    console.log(toast);
                  }
                );
              });
              //this.fileOpener.open(this.file.dataDirectory + 'agpChart.png', 'image/png');
            }).catch((err) => {
              console.log(err);
              this.toast.show(`Error writing Chart File: ` + err, '3000', 'center').subscribe(
                toast => {
                  console.log(toast);
                }
              );
            });
            //});
          }
          else {
            // what to do if we are in browser?
            // for now just put the logo instead
            docDefinition.content.push({
              columns: [{
                image: this.logo, width: 250, alignment: 'center'
              }]
            });
            return this.logo;
          }
        }
        // console.log('PDF CREATED');
        // this.pdfObj = pdfMake.createPdf(docDefinition);
      }).then(data => {
        console.log('PDF CREATED:', data);
        this.pdfObj = pdfMake.createPdf(docDefinition);
      }).catch(e => {
        console.log(e);
      });
      //this.pdfObj = pdfMake.createPdf(docDefinition);
    } else {
      this.datesValid = false;
    }

  }

  downloadPdf() {

    if (this.plt.is('cordova')) {
      // this.toast.show(`Download Chart`, '3000', 'center').subscribe(
      //   toast => {
      //     console.log(toast);
      //   }
      // );
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, 'myReport_Manzana.pdf', blob, { replace: true }).then(fileEntry => {
          this.toast.show(`PDF written successfully`, '4000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + 'myReport_Manzana.pdf', 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }

}
