import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { EntriesProvider } from '../../providers/entries/entries';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {

  constructor(public navCtrl    : NavController,
              public DB       : EntriesProvider) {
  }

  ionViewDidLoad()
   {
   }

}
