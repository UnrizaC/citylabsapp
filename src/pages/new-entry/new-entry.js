var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { EntriesProvider } from '../../providers/entries/entries';
import { RemoteProvider } from '../../providers/remote/remote';
var NewEntryPage = /** @class */ (function () {
    function NewEntryPage(navCtrl, navParams, fb, DB, toastCtrl, remoteService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.DB = DB;
        this.toastCtrl = toastCtrl;
        this.remoteService = remoteService;
        this.isEdited = false;
        this.hideForm = false;
        this.hasEntries = false;
        this.resetFields();
        if (navParams.get("key") && navParams.get("rev")) {
            this.recordId = navParams.get("key");
            this.revisionId = navParams.get("rev");
            this.valueType = navParams.get("valueType");
            this.value = navParams.get("value");
            this.isEdited = true;
            this.getEntry(this.recordId);
            this.pageTitle = 'Amend entry';
            console.log("Value :", this.value);
        }
        else {
            this.recordId = '';
            this.revisionId = '';
            this.valueType = navParams.get("valueType");
            this.unit = navParams.get("unit");
            this.value = navParams.get("value");
            this.isEdited = false;
            this.pageTitle = 'New entry';
            this.getEntries();
        }
        // Default value
        this.acquisitionDate = new Date().toISOString();
        /* var datestring = ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" +
      d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2); */
        //this.date = datestring.toString();
        this.form = fb.group({
            "value": [this.value, Validators.required],
            "acquisitionDate": [this.acquisitionDate, ''],
            "comment": [this.comment, '']
        });
    }
    NewEntryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NewEntryPage');
    };
    NewEntryPage.prototype.getEntry = function (id) {
        var _this = this;
        this.DB.getEntry(id).then(function (data) {
        }).catch(function (err) {
            _this.sendNotification('The entry could not be retrieved. Please try again!');
        });
    };
    NewEntryPage.prototype.getEntries = function () {
        var _this = this;
        this.DB.getEntries(['glycaemia']).then(function (data) {
            var existingData = Object.keys(data).length;
            if (existingData !== 0) {
                _this.hasEntries = true;
                _this.entries = data;
                console.log(data);
            }
            else {
                _this.sendNotification("No entries in DB yet!");
                console.log("No entries in DB yet!");
            }
        });
    };
    NewEntryPage.prototype.addEntry = function () {
        var _this = this;
        var value = this.form.controls["value"].value, acquisitionDate = this.form.controls["acquisitionDate"].value, comment = this.form.controls["comment"].value, revision = this.revisionId, valueType = this.valueType, unit = this.unit;
        var entry = {
            value: this.form.controls["value"].value,
            acquisitionDate: this.form.controls["acquisitionDate"].value,
            comment: this.form.controls["comment"].value,
            revision: this.revisionId,
            valueType: this.valueType,
            unit: this.unit
        };
        if (this.recordId !== '') {
            this.DB.updateEntry(entry)
                .then(function (data) {
                _this.hideForm = true;
                _this.sendNotification(valueType + " " + value + " was updated in your entries list");
                _this.navCtrl.popToRoot();
                //this.navCtrl.push('HomePage');
            });
        }
        else {
            this.remoteService.postNewEntry(entry).then(function (data) {
                _this.sendNotification(valueType + " " + value + " was added to your entries list ON REMOTE");
            }).catch(function (err) {
                _this.sendNotification('ERROR' + err.name + ' ' + err.error + ' ' + err.status + ' ' + err.message);
            });
            this.DB.addEntry(entry)
                .then(function (data) {
                _this.hideForm = true;
                _this.resetFields();
                _this.sendNotification(valueType + " " + value + " was added to your entries list");
                _this.navCtrl.popToRoot();
                //this.navCtrl.push('HomePage');
            });
        }
    };
    NewEntryPage.prototype.deleteEntry = function (id, rev) {
        var _this = this;
        this.DB.deleteEntry(id, rev).then(function (data) {
            _this.sendNotification('The entry was deleted');
        }).catch(function (err) {
            _this.sendNotification('The entry was not deleted! Try again.');
        });
    };
    NewEntryPage.prototype.resetFields = function () {
        this.value = '';
        this.acquisitionDate = new Date().toISOString();
        this.comment = '';
    };
    NewEntryPage.prototype.sendNotification = function (message) {
        var notification = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        notification.present();
    };
    NewEntryPage = __decorate([
        IonicPage({
            name: 'NewEntryPage',
            segment: 'entry/:valueType'
        }),
        Component({
            selector: 'page-new-entry',
            templateUrl: 'new-entry.html',
        }),
        __metadata("design:paramtypes", [NavController,
            NavParams,
            FormBuilder,
            EntriesProvider,
            ToastController,
            RemoteProvider])
    ], NewEntryPage);
    return NewEntryPage;
}());
export { NewEntryPage };
//# sourceMappingURL=new-entry.js.map