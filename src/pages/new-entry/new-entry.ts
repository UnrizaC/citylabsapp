import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { EntriesProvider } from '../../providers/entries/entries';
import { RemoteProvider } from '../../providers/remote/remote';
import { AuthorizationProvider } from '../../providers/authorization/authorization';

import { HomePage } from '../home/home';

@IonicPage({
  name: 'NewEntryPage',
  segment: 'entry/:valueType'
})
@Component({
  selector: 'page-new-entry',
  templateUrl: 'new-entry.html',
})
export class NewEntryPage {

  public form: FormGroup;
  public value: string;
  public unit: string;
  public acquisitionDate: string;
  public comment: string;
  public isEdited: boolean = false;
  public recordId: any;
  public revisionId: any;
  public pageTitle: string;
  public hideForm: boolean = false;
  public hasEntries: boolean = false;
  public valueType: string;
  public meals: Array<{ name: string, grade: number, carbs: number }>;
  public mealType: any;
  public searchInput: string;
  public entriesError: string;
  public entriesErrorMessage: any;
  public auth: AuthorizationProvider;
  public duration: string;
  public intensity: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public entries: EntriesProvider,
    public toastCtrl: ToastController,
    private remoteService: RemoteProvider,
    public autho: AuthorizationProvider
  ) {
    this.auth = autho;
    this.resetFields();

    if (navParams.get("key")) {
      this.recordId = navParams.get("key");
      this.valueType = navParams.get("valueType");
      this.value = navParams.get("value");
      this.isEdited = true;
      this.getEntry(this.recordId);
      this.pageTitle = 'Edit entry';
      console.log("Value :", this.value);
    }
    else {
      this.recordId = '';
      this.valueType = navParams.get("valueType");
      this.unit = navParams.get("unit");
      this.value = navParams.get("value");
      this.isEdited = false;
      this.pageTitle = 'New entry ';
      this.getEntries();
    }

    // Default value
    this.acquisitionDate = new Date().toISOString();
    console.log(this.valueType);

    switch (this.valueType) {

      case 'glycaemia':
        this.form = fb.group({
          "value": [this.value, Validators.required],
          "acquisitionDate": [this.acquisitionDate, Validators.required],
          "comment": [this.comment, '']
        }
        );
        break;
      case 'meal':
        this.form = fb.group({
          "mealType": [this.mealType, Validators.required],
          "acquisitionDate": [this.acquisitionDate, Validators.required],
          "comment": [this.comment, '']
        }
        );
        break;
      case 'activity':
        this.form = fb.group({
          "duration": [this.duration, Validators.required],
          "intensity": [this.intensity, Validators.required],
          "acquisitionDate": [this.acquisitionDate, Validators.required],
          "comment": [this.comment, '']
        });

    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewEntryPage');
  }

  getEntry(id) {
    this.entries.getEntry(id).then((data) => {

    }).catch((err) => {
      this.sendNotification('The entry could not be retrieved. Please try again!');
    });
  }

  getMeals(ev: any) {
    this.meals = [{ name: "Pasta bolognese", grade: 15, carbs: 37 },
    { name: "Pasta with pesto genovese", grade: 23, carbs: 43 }
    ];
  }

  clearMeals(ev: any) {
    this.meals = [];
  }

  getEntries() {
    this.entries.getEntries(['glycaemia', 'meals']).then((data) => { }).catch((err) => {
      this.sendNotification('Entries could not be retrieved. Please try again!');
    });
  }

  addEntry() {
    let value: string;
    let valueType: string = this.valueType;

    let entry: any = {
      value: "",
      acquisitionDate: this.form.controls["acquisitionDate"].value,
      comment: this.form.controls["comment"].value,
      valueType: this.valueType,
      unit: this.unit
    };

    if (this.form.contains("value")) {
      value = this.form.controls["value"].value;
      entry.value = value;
    } else if (this.form.contains("duration")) {
      value = this.form.controls["duration"].value;
      entry.value = value;
      entry.subType = 'sport';
      entry.values = [{
        type: 'intensity',
        value: this.form.controls["intensity"].value

      }];
    }

    if (this.recordId !== '') {
      this.entries.updateEntry(entry)
        .then((data) => {
          this.hideForm = true;
          this.sendNotification(`${valueType} ${value} was updated in your entries list`);
          this.navCtrl.popToRoot();
          console.log(data);
          //this.navCtrl.push('HomePage');
        });
    }
    else {
      console.log('ENTRY:', entry);
      this.entries.addEntry(entry).then((data) => {
        this.hideForm = true;
        this.resetFields();
        this.sendNotification(`${valueType} ${value} was added to your entries list`);
        this.navCtrl.popToRoot();
        //this.navCtrl.push('HomePage');
      }).catch((err) => {
        this.sendNotification('ERROR' + err.name + ' ' + err.error + ' ' + err.status + ' ' + err.message);
      });

      //add to REMOTE DB
      this.remoteService.postNewEntry(entry).then((data) => {
        this.sendNotification(`${valueType} ${value} was added to the REMOTE`);
        this.navCtrl.popToRoot();
      }).catch((err) => {
        this.sendNotification('ERROR' + err.name + ' ' + err.error + ' ' + err.status + ' ' + err.message);
      });
    }
  }

  deleteEntry(id) {
    this.entries.deleteEntry(id).then((data) => {
      this.sendNotification('The entry was deleted');
    }).catch((err) => {
      this.sendNotification('The entry was not deleted! Try again.');
    });
  }

  resetFields(): void {
    this.value = '';
    this.acquisitionDate = new Date().toISOString();
    this.comment = '';
  }

  sendNotification(message): void {
    let notification = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    notification.present();
  }

}
