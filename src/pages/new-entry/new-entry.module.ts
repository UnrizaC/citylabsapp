import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { NewEntryPage } from './new-entry';

@NgModule({
  declarations: [
    NewEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(NewEntryPage),
    TranslateModule.forChild()
  ],
  exports: [
  	NewEntryPage
  ]
})
export class NewEntryPageModule {}
