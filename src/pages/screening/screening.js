var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EntriesProvider } from '../../providers/entries/entries';
var ScreeningPage = /** @class */ (function () {
    function ScreeningPage(navCtrl, navParams, entries) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.entries = entries;
        // timeline: Array<{ date: number, title: string, value: string, profilePicture: string, unit: string, type: string }> = [];
        this.timeline = [];
        this.myDate = new Date();
        this.myDateStr = new Date().toISOString();
    }
    ScreeningPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        this.timeline = [];
        this.logBook = this.entries.getEntries(['glycaemia', 'picture', 'video', 'meal']).then(function (data) {
            _this.timeline.push(data);
            console.log(data);
            refresher.complete();
        }).catch(function (err) {
            console.log('REFRESH ERROR');
        });
    };
    ScreeningPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('ionViewDidLoad LobgookPage');
        this.logBook = this.entries.getEntries(['glycaemia', 'picture', 'video', 'meal']).then(function (data) {
            _this.timeline.push(data);
            console.log(data);
        });
    };
    ScreeningPage.prototype.newEntry = function () {
        this.navCtrl.push('NewEntryPage', {
            'valueType': 'glycaemia',
            'unit': 'mg/dL'
        });
    };
    ScreeningPage = __decorate([
        Component({
            selector: 'page-screening',
            templateUrl: 'screening.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, EntriesProvider])
    ], ScreeningPage);
    return ScreeningPage;
}());
export { ScreeningPage };
//# sourceMappingURL=screening.js.map