import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, FabContainer } from 'ionic-angular';
import { EntriesProvider } from '../../providers/entries/entries';
import { RemoteProvider } from '../../providers/remote/remote';
import { SyncProvider } from '../../providers/sync/sync';

@Component({
  selector: 'page-screening',
  templateUrl: 'screening.html',
})
export class ScreeningPage {
  logBook: Promise<any>;
  // timeline: Array<{ date: number, title: string, value: string, profilePicture: string, unit: string, type: string }> = [];
  timeline: Array<any> = [];
  myDate: Date;
  myDateStr: String;
  logBookTab: String;
  result: any;


  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public entries: EntriesProvider,
    private remote: RemoteProvider,
    private syncProvider: SyncProvider) {
    this.myDate = new Date();
    this.myDateStr = new Date().toISOString();
    this.logBookTab = "logBookAll";
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.timeline = [];
    this.logBook = this.entries.getEntries(['glycaemia', 'picture', 'video', 'meal']).then((data: any) => {
      //data.sort((a,b) => a.date <= b.date ? -1 : 1);
      for (let i = 0; i < data.rows.length; i++) {
        let item = data.rows.item(i);
        this.timeline.push(item);
      }
      console.log(data);
      refresher.complete()
    }).catch((err) => {
      console.log('REFRESH ERROR');
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LobgookPage');
    this.getLogBookEntries();
    this.syncProvider.updateLastSyncTimestamp();
    this.syncEntriesFromServer();
  }

  ionViewWillEnter() {
    //this.getLogBookEntries();
  }

  getLogBookEntries() {
    this.timeline = [];
    this.logBook = this.entries.getEntries(['glycaemia', 'meal']).then((data: any) => {
      //data.sort((a,b) => a.date <= b.date ? -1 : 1);
      for (let i = 0; i < data.rows.length; i++) {
        let item = data.rows.item(i);
        this.timeline.push(item);
      }
      this.result = JSON.stringify(this.timeline);
      console.log(data);
    });
  }

  newEntry(entryType: string, fab: FabContainer) {
    fab.close();
    this.navCtrl.push('NewEntryPage',
      {
        'valueType': entryType,
        'unit': 'mg/dL'
      }
    );
  }

  deleteEntry(id) {
    this.entries.deleteEntry(id).then((d) => {
      this.getLogBookEntries();
      //this.timeline.splice(1,1);
    }).catch((err) => {
      this.toastCtrl.create({
        message: 'Entry was not deleted! Try again!',
        duration: 3000
      }).present();
    });
  }

  syncEntriesFromServer() {
    console.log('EMAIL:', window.localStorage.getItem('email'));
    // this.syncProvider.updateLastSyncTimestamp();
    let timestamp = this.syncProvider.getLastSyncTimestamp();
    console.log('TIMESTAMP', new Date(timestamp));
    this.remote.getFilterEntries(JSON.stringify(['glycaemia', 'activity', 'insulin']), new Date(timestamp), new Date()).then((data: any) => {
      console.log('FILTERED DATA', data);

      var field = {
        value: '',
        acquisitionDate: '',
        comment: '',
        valueType: ''
      };

      data.forEach(entry => {
        field = {
          value: entry.value,
          acquisitionDate: entry.datetimeAcquisition,
          comment: entry.comment,
          valueType: entry.type,
        };
        this.entries.addEntry(field);
      });
      this.syncProvider.setTimestamptoNow();
    }).catch(e => console.log(e));
  }

  /*(function() {

    // VARIABLES
    const timeline = document.querySelector(".timeline ol"),
      elH = document.querySelectorAll(".timeline li > div"),
      arrows = document.querySelectorAll(".timeline .arrows .arrow"),
      arrowPrev = document.querySelector(".timeline .arrows .arrow__prev"),
      arrowNext = document.querySelector(".timeline .arrows .arrow__next"),
      firstItem = document.querySelector(".timeline li:first-child"),
      lastItem = document.querySelector(".timeline li:last-child"),
      xScrolling = 280,
      disabledClass = "disabled";
  
    // START
    window.addEventListener("load", init);
  
    function init() {
      setEqualHeights(elH);
      animateTl(xScrolling, arrows, timeline);
      setSwipeFn(timeline, arrowPrev, arrowNext);
      setKeyboardFn(arrowPrev, arrowNext);
    }
  
    // SET EQUAL HEIGHTS
    function setEqualHeights(el) {
      let counter = 0;
      for (let i = 0; i < el.length; i++) {
        const singleHeight = el[i].offsetHeight;
  
        if (counter < singleHeight) {
          counter = singleHeight;
        }
      }
  
      for (let i = 0; i < el.length; i++) {
        el[i].style.height = `${counter}px`;
      }
    }
  
    // CHECK IF AN ELEMENT IS IN VIEWPORT
    // http://stackoverflow.com/questions/123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
    function isElementInViewport(el) {
      const rect = el.getBoundingClientRect();
      return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
      );
    }
  
    // SET STATE OF PREV/NEXT ARROWS
    function setBtnState(el, flag = true) {
      if (flag) {
        el.classList.add(disabledClass);
      } else {
        if (el.classList.contains(disabledClass)) {
          el.classList.remove(disabledClass);
        }
        el.disabled = false;
      }
    }
  
    // ANIMATE TIMELINE
    function animateTl(scrolling, el, tl) {
      let counter = 0;
      for (let i = 0; i < el.length; i++) {
        el[i].addEventListener("click", function() {
          if (!arrowPrev.disabled) {
            arrowPrev.disabled = true;
          }
          if (!arrowNext.disabled) {
            arrowNext.disabled = true;
          }
          const sign = (this.classList.contains("arrow__prev")) ? "" : "-";
          if (counter === 0) {
            tl.style.transform = `translateX(-${scrolling}px)`;
          } else {
            const tlStyle = getComputedStyle(tl);
            // add more browser prefixes if needed here
            const tlTransform = tlStyle.getPropertyValue("-webkit-transform") || tlStyle.getPropertyValue("transform");
            const values = parseInt(tlTransform.split(",")[4]) + parseInt(`${sign}${scrolling}`);
            tl.style.transform = `translateX(${values}px)`;
          }
  
          setTimeout(() => {
            isElementInViewport(firstItem) ? setBtnState(arrowPrev) : setBtnState(arrowPrev, false);
            isElementInViewport(lastItem) ? setBtnState(arrowNext) : setBtnState(arrowNext, false);
          }, 1100);
  
          counter++;
        });
      }
    }
  
    // ADD SWIPE SUPPORT FOR TOUCH DEVICES
    function setSwipeFn(tl, prev, next) {
      const hammer = new Hammer(tl);
      hammer.on("swipeleft", () => next.click());
      hammer.on("swiperight", () => prev.click());
    }
  
    // ADD BASIC KEYBOARD FUNCTIONALITY
    function setKeyboardFn(prev, next) {
      document.addEventListener("keydown", (e) => {
        if ((e.which === 37) || (e.which === 39)) {
          const timelineOfTop = timeline.offsetTop;
          const y = window.pageYOffset;
          if (timelineOfTop !== y) {
            window.scrollTo(0, timelineOfTop);
          }
          if (e.which === 37) {
            prev.click();
          } else if (e.which === 39) {
            next.click();
          }
        }
      });
    }
  
  })();B */

}
