import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ScreeningPage } from './screening';

@NgModule({
  declarations: [
    ScreeningPage,
  ],
  imports: [
    IonicPageModule.forChild(ScreeningPage),
    TranslateModule.forChild()
  ],
})
export class ScreeningPageModule {}
