import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, Events, FabContainer } from 'ionic-angular';
import { ScreeningPage } from '../screening/screening';
import { AppointmentPage } from '../appointment/appointment';
import { ResultsPage } from '../results/results';
import { NewEntryPage } from '../new-entry/new-entry';
import { ObjectivePage } from '../objective/objective';

import * as Highcharts from 'highcharts';
import HighchartsMore from 'highcharts-more';

import { NFC, Ndef } from "@ionic-native/nfc";
import { Toast } from '@ionic-native/toast';
import { Vibration } from '@ionic-native/vibration';

import { RemoteProvider } from '../../providers/remote/remote';
import { AuthorizationProvider } from '../../providers/authorization/authorization';
import { UserProvider } from '../../providers/user/user';
import { BluetoothPage } from '../bluetooth/bluetooth';

HighchartsMore(Highcharts);

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {
	treatmentTab: string;
	bolusAssistantTab: string;
	sportPlan: boolean = false;
	payloadNFC: any;
	seePayload: any;
	granted: boolean;
	denied: boolean;
	scanned: boolean;
	tagId: string;
	message: string;
	status: string;
	temp: any;
	entries: any;
	doesnotwork: any;
	agpChart: any;
	to: any = new Date().toISOString();
	from: any = new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDate()).toISOString();
	toDate: any;
	fromDate: any;
	auth: AuthorizationProvider;
	userProfile: any;
	entriesErrorMessage: string;
	entriesError: any;
	glyStats: Array<{ glymax: number, glymean: number, glymin: number, glystdev: number, glyvar: number, hba1c: number, hyperperc: number, hypoperc: number, length: number, readperday: number, risk: number }> = [];

	constructor(public navCtrl: NavController,
		private nfc: NFC,
		private ndef: Ndef,
		private remoteService: RemoteProvider,
		public autho: AuthorizationProvider,
		public userProvider: UserProvider,
		public menuCtrl: MenuController,
		public events: Events,
		private vibration: Vibration,
		private toast: Toast) {

		this.menuCtrl.close();
		this.menuCtrl.enable(true, 'sidebarMenu');
		this.resetScanData();
		this.displayAgp();
		this.auth = autho;
		this.getUserProfile();
		this.getGlycemiaStats();
		this.getInsulinData();
		this.treatmentTab = "treatmentGlucose";
	}

	doRefresh(refresher) {
		console.log('Begin async operation', refresher);

		this.displayAgp().then(
			refresher.complete()
		).catch((err) => {
			console.log(err);
		});
		this.getInsulinData();
		this.getGlycemiaStats();

	}

	estimateBolus() {
		console.log('Estimate Bolus');
	}

	getUserProfile() {
		this.remoteService.getUserProfile().then((data) => {
			console.log('USER PROFILE:', data);
			this.userProfile = data['username'];
			console.log(this.userProfile, typeof this.userProfile);
		}).catch((err) => {
			console.log(err);
		});
	}

	getInsulinData() {
		this.remoteService.getInsulinLogs('snack4', this.from, this.to).then((data) => {
			console.log('INSULIN', data);
		}).catch((err) => {
			console.log(err);
		});
	}

	resetScanData() {
		this.granted = false;
		this.scanned = false;
		this.tagId = "";
		this.message = "";
		this.status = "";
		this.temp = "";
	}

	ionViewDidEnter() {
		this.nfc.enabled().then((resolve) => {
			this.addListenNFC();
		}).catch((reject) => {
			console.log("NFC is not supported by your Device");
		});

	}

	addListenNFC() {

		this.nfc.addNdefFormatableListener(() => {
			console.log('Listener added');
		},
			(error) => {
				console.log('Listener error attaching');
			}).subscribe((nfcEvent) => {
				this.toast.show('NDEF Formatable Listener Detected', '10000', 'center').subscribe(toast => {
					console.log('NDEF Formatable Listener Detected');
				});

				var mimeType = "text/pg",
					payload = "AA:00",
					record = this.ndef.record(4, '', '1', this.nfc.stringToBytes(payload));

				this.nfc.write([record]).then(() => {
					this.toast.show('Wrote to the Meter', '500', 'center').subscribe(toast => {
						console.log('Wrote to the meter');
					});
					this.vibration.vibrate([80, 150, 80]);
				},
					(err) => {
						this.toast.show('Error writing to the Meter', '500', 'center').subscribe(toast => {
							console.log('Error writing to the Meter');
						});
						this.vibration.vibrate(500);
					});
			});

		/**
		 * Listen for a discovery of a NDEF formatted tag
		 * If a tag is discovered it will display a Toast with the info message
		 */
		this.nfc.addNdefListener(nfcEvent => this.sesReadNFC(nfcEvent.tag)).subscribe(data => {
			this.toast.show('NDEF Listener Detected', '10000', 'center').subscribe(toast => {
				console.log('NDEF Listener Detected');
			});

			if (data && data.tag && data.tag.id) {
				let tagId = this.nfc.bytesToHexString(data.tag.id);
				this.tagId = tagId;
			}

			// if(data && data.tag && data.tag.id){
			// 	let tagId = this.nfc.bytesToHexString(data.tag.id);
			// 	if (tagId) {
			//           this.tagId = tagId;
			//           this.scanned = true;

			//           // only testing data consider to ask web api for access
			//           this.granted = [
			//             "dd3b515f"
			//           ].indexOf(tagId) != -1;
			// 		  let payload = data.tag.ndefMessage[0].payload;
			// 		  this.payloadNFC = payload;
			// 		  console.log("Payload:", payload);
			//           let tagContent = this.nfc.bytesToString(payload).substring(3);
			//           this.message = tagContent;

			// 		  this.vibration.vibrate(750);

			//     		//let mes = this.nfc.bytesToHexString(data.tag.ndefMessage[0].payload);
			//     // 		let mes = data.tag;
			//     		// if(mes) {
			//     		// 	this.status = "Reading the tag...";
			//     		// 	//this.message = this.decodePayload(mes);
			//     		// 	this.temp = JSON.stringify(mes);
			//     		// 	this.message = JSON.parse(this.temp);
			//     		// 	this.status = "Tag read successfully";
			//     		// }
			//       } else {
			//           alert('NFC_NOT_DETECTED');
			//     }
			// } 
			//if did not read a tag
			//SD Biosensor
			// else {
			// 	var type = "text/pg",
			// 	id = [],
			// 	payload = this.nfc.stringToBytes("41 B4 FF C0"),
			// 	record = this.ndef.record(this.ndef.TNF_MIME_MEDIA, type, id, payload);

			// 	var message = [record];

			// 	this.nfc.write(message).then(data => {
			// 		this.toast.show('Wrote to device', '10000', 'center').subscribe(toast => {
			// 			console.log('Wrote to device');
			// 		});
			// 		console.log(data);
			// 	}, error => {
			// 		this.toast.show('Failed to write to device', '10000', 'center').subscribe(toast => {
			// 			console.log('Failed to write to device');
			// 		});
			// 	});
			// }
			//Menarini
			//else {
			/*
			 * NDEF message is an array of records
			 * NDEF record is an object of a format: (content_type, data type, if, payload)
			 */
			var id = [], type = "text/pg", payload, record;

			//The payload is a set of bytes to be sent to establish the communication with the device
			payload = this.nfc.stringToBytes("0x02 0xa2 0x2b 0x10 0x0B 0xAA 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x0B");

			this.seePayload = payload;
			record = this.ndef.record(this.ndef.TNF_WELL_KNOWN, type, id, payload);

			var message = [record];

			//After we have constructed the NDEF message, we can write it to the device
			//using nfc.write method

			this.nfc.write(message).then(data => {
				console.log(data);
				this.vibration.vibrate([150, 150, 150]); //vibrate to give a haptic feedback to the user
				//let ack = this.nfc.bytesToHexString(data.tag.ndefMessage[0].payload);
				let ack = this.nfc.bytesToHexString(data); //this is supposed to be the answer from the device, but it is not
				//this.seePayload = 'ACK:';
				this.seePayload = ack; //display the response on the screen

				//Display a toast with the info message on the screen
				this.toast.show('Wrote to device' + data, '10000', 'center').subscribe(toast => {
					console.log('Wrote to device' + ack);
				});

				//this was another attempt to write the command, but unsuccessful (no opening/closing bytes etc.)
				// payload = this.nfc.stringToBytes("0xAA 0x07 0x00 0x00 0x00 0x00 0x00 0x00 0x00");
				// record = this.ndef.record(this.ndef.TNF_EXTERNAL_TYPE, type, id, payload);
				// message = [record];

				// this.nfc.write(message).then(data => {
				// 	let ack2 = this.nfc.bytesToHexString(data);
				// 	this.seePayload = ack2;
				// }, error => {
				// 	this.vibration.vibrate([100,50,100]);
				// 	this.toast.show('Failed to write to device' + error, '10000', 'center').subscribe(toast => {
				// 		console.log('Failed to write to device' + error);
				// 	});
				// })


			}, error => {
				this.vibration.vibrate([100, 50, 100]);
				this.toast.show('Failed to write to device' + error, '10000', 'center').subscribe(toast => {
					console.log('Failed to write to device' + error);
				});
			});



			//}
		});

		//SD Biosensor glucosemeter
		//nfc.transceive is not actually working with IONIC
		// this.nfc.addTagDiscoveredListener(function(nfcEvent) {
		// 	this.nfc.connect('android.nfc.tech.NfcA', 500).then(
		// 		() => {
		// 			this.ndef.
		// 			console.log('connected to: ', this.nfc.bytesToHexString(nfcEvent.tag.id));
		// 			this.nfc.transceive('41 B4 FF C0').then(response => {
		// 				this.message = response;
		// 			},
		// 			error => this.toast.show(error, '10000', 'center').subscribe(
		// 			  toast => {
		// 			    console.log(error, 'Error Meter Recognize');
		// 			  })
		// 			)
		// 		},
		// 		(error) => this.toast.show(error, '10000', 'center').subscribe(
		// 			  toast => {
		// 			    console.log(error, 'Connection Failed');
		// 			  })
		// 	);
		// });
	}

	sesReadNFC(data): void {
		this.status = "Reading the tag...";
		// for(var prop in data) {
		// 	// this.message += prop;
		// 	this.message = 'bbllaa';
		// 	if(prop == 'techTypes') {
		// 		this.status = 'bla';
		// 		this.message += 'types';
		// 	}
		// 	if(prop == 'isWritable') {
		// 		this.status = 'bla2';
		// 		this.message += 'isWritable';
		// 	}
		// }
		console.log('DATA:', data);
		this.status = "Tag read successfully";
	}

	failNFC(err) {
		alert("Error while reading: Please Retry");
	}

	//Do we even use this for anything? I think not anymore
	decodePayload(data) {

		var languageCodeLength = (data[0] & 0x3F), // 6 LSBs
			languageCode = data.slice(1, 1 + languageCodeLength),
			utf16 = (data[0] & 0x80) !== 0; // assuming UTF-16BE

		// TODO need to deal with UTF in the future
		// console.log("lang " + languageCode + (utf16 ? " utf16" : " utf8"));

		return this.nfc.bytesToString(data.slice(languageCodeLength + 1));
	}

	navToScreeningPage() {
		this.navCtrl.push(ScreeningPage);
	}

	navToResultsPage() {
		this.navCtrl.push(ResultsPage);
	}

	navtoBluetoothPage() {
		this.navCtrl.push(BluetoothPage);
	}

	newEntry(entryType: string, fab: FabContainer) {
		console.log(this.message);
		fab.close();
		this.navCtrl.push('NewEntryPage',
			{
				'valueType': entryType,
				'value': this.message,
				'unit': 'mg/dL'
			}
		);
	}

	/**
	 * Retrieve glycemia statistics to be displayed in the dashboard
	 * It takes two variables, from and to, to calculate the statistics bounded with these two dates
	 */

	getGlycemiaStats() {
		var date1 = new Date(this.from);
		var date2 = new Date(this.to);
		var timeDiff = Math.abs(date2.getTime() - date1.getTime());
		var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
		console.log('DIFF:', diffDays);
		this.remoteService.getGlycemiaStats(70, 180, diffDays, 0, 23).then((data: any) => {
			this.glyStats = data;
			console.log(data);
		}).catch((err) => {
			console.log(err);
		});

		this.remoteService.getEntries().then((data: any) => {
			//this.glyStats = data;
			console.log('ENTRIES:', data);
		}).catch((err) => {
			console.log(err);
		});
	}

	/**
	 * This function is called to update the from and to variables needed to generate the AGP charts.
	 * Also the whole dashboard depends on these two dates and display the data accordingly. 
	 * @param duration represent the duration of the preset buttons 1m,2m,3m 
	 */

	updateAgp(duration) {
		switch (duration) {
			case '1m':
				this.to = new Date().toISOString();
				this.from = new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDate()).toISOString();
				break;
			case '2m':
				this.to = new Date().toISOString();
				this.from = new Date(new Date().getFullYear(), new Date().getMonth() - 2, new Date().getDate()).toISOString();
				break;
			case '3m':
				this.to = new Date().toISOString();
				this.from = new Date(new Date().getFullYear(), new Date().getMonth() - 3, new Date().getDate()).toISOString();
				break;
			default:
				this.to = new Date().toISOString();
				this.from = new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDate()).toISOString();
		}
	}

	/**
	 * Contains the object describing the AGP chart.
	 * Calls the remote service to retrieve the data series from the remote API and displays on successful response.
	 * It is called every time there is an update in the date from or to variable. 
	 */

	displayAgp(): Promise<any> {
		this.getGlycemiaStats();
		Highcharts.setOptions({ global: { useUTC: false } });
		var agpChart = {
			title: {
				text: ''
			},
			xAxis: {
				type: 'datetime',
				opposite: true,
				crosshair: true,
				tickInterval: 3600 * 1000,
				labels: {
					formatter: function () {
						return Highcharts.dateFormat('%H:%M', this.value);
					},
					overflow: 'justify',
					rotation: -45,
					step: 2
				},
				plotLines: [{ //meal plotlines
					value: 0, //Date.UTC(2017,10, 3, 7, 0),
					width: 1,
					color: '#e6e6e6',
					dashStyle: 'solid',
					label: {
						text: '',
						align: 'center',
						y: 12,
						x: 0
					}
				}, {
					value: 0, //Date.UTC(2017,10, 3, 11, 0),
					width: 1,
					color: '#e6e6e6',
					dashStyle: 'solid',
					label: {
						text: '',
						align: 'right',
						y: 60,
						x: 0
					}
				}, {
					value: 0, //Date.UTC(2017,10, 3, 17, 0),
					width: 1,
					color: '#e6e6e6',
					dashStyle: 'solid',
					label: {
						text: '',
						align: 'right',
						y: 60,
						x: 0
					}
				}, {
					value: 0, //Date.UTC(2017,10, 3, 20, 0),
					width: 1,
					color: '#e6e6e6',
					dashStyle: 'solid',
					label: {
						text: '',
						align: 'right',
						y: 60,
						x: 0
					}
				}]
			},
			yAxis: {
				title: {
					text: ''
				},
				gridLineWidth: 0,
				minorGridLineWidth: 0,
				plotLines: [{
					value: 180,
					width: 1.3,
					dashStyle: 'dash',
					color: '#9e9e9e',
					zIndex: 5,
					label: {
						text: 'max (180 mg/dL)',
						align: 'left',
						x: 5
					}
				}, {
					value: 70,
					color: '#9e9e9e',
					dashStyle: 'dash',
					width: 1.3,
					zIndex: 99,
					label: {
						text: 'min (70 mg/dL)',
						x: -5,
						y: 15,
						align: 'right'
					}
				}, {
					value: null,
					width: 2,
					color: 'blue',
					zIndex: 1,
					dashStyle: 'ShortDot',
					label: {
						text: null,
						align: 'center',
						x: 5,
						y: 20
					}
				}],
				min: 0,
				max: 220,
				lineColor: '#e6e6e6',
				lineWidth: 3
			},
			plotOptions: {
				series: {
					marker: {
						enabled: false
					},
					fillOpacity: 0.2
				}
			},
			legend: {
				align: 'center',
				//layout: 'vertical',
				verticalAlign: 'bottom',
				floating: true,
				x: 0,
				//y: 30
			},
			credits: {
				enabled: false
			},

			series: [],

			responsive: {
				rules: []
			},

			options: {
				chart: {},
				tooltip: {
					shared: true
				}
			},
			noData: 'No data'
		};
		return new Promise((resolve, reject) => {
			this.remoteService.getGlycaemiaEntries(this.from, this.to).then((data) => {
				console.log('GLY ENTRIES:', data);
				agpChart.series = data['series'];
				this.agpChart = Highcharts.chart('agp', agpChart);
				resolve();
			}).catch((err) => {
				this.entriesErrorMessage = ' NO ENTRIES NEW GLY';
				this.entriesError = err;
				this.agpChart = Highcharts.chart('agp', agpChart);
				reject(err);
			});
		});
	}

	ionViewWillLoad() {
		console.log("HomePage DidLoad");
		this.getInsulinData();
	}
}
