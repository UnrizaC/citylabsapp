var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ScreeningPage } from '../screening/screening';
import { AppointmentPage } from '../appointment/appointment';
import { ResultsPage } from '../results/results';
import * as Highcharts from 'highcharts';
import HighchartsMore from 'highcharts-more';
import { NFC } from "@ionic-native/nfc";
import { RemoteProvider } from '../../providers/remote/remote';
HighchartsMore(Highcharts);
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, nfc, remoteService) {
        this.navCtrl = navCtrl;
        this.nfc = nfc;
        this.remoteService = remoteService;
        this.to = new Date().toISOString();
        this.from = new Date(new Date().getFullYear(), new Date().getMonth() - 1).toISOString();
        this.resetScanData();
        this.getToken();
    }
    HomePage.prototype.doRefresh = function (refresher) {
        console.log('Begin async operation', refresher);
        // setTimeout(() => {
        //   console.log('Async operation has ended');
        //   refresher.complete();
        // }, 2000);
        this.displayAgp().then(refresher.complete()).catch(function (err) {
        });
    };
    HomePage.prototype.getToken = function () {
        //this.remoteService.returnToken().then((data)=>{
        //this.entries = data;
        //this.works = data['activated'];
        this.token = this.remoteService.returnToken();
        console.log(this.token);
        this.displayAgp();
        //}).catch((err) => {
        //this.doesnotwork = 'NO TOKEN';
        //this.displayAgp();
        //});
    };
    HomePage.prototype.getEntries = function () {
        var _this = this;
        this.remoteService.getEntries(this.token).then(function (data) {
            //this.entries = data['length'];
            _this.entries = data;
        }).catch(function (err) {
            _this.doesnotwork = ' NO ENTRIES';
            _this.doesnotwork += ' ' + err.name + ' ' + err.error + ' ' + err.status + ' ' + err.message;
        });
    };
    HomePage.prototype.resetScanData = function () {
        this.granted = false;
        this.scanned = false;
        this.tagId = "";
        this.message = "";
        this.status = "";
        this.temp = "";
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.nfc.enabled().then(function (resolve) {
            _this.addListenNFC();
        }).catch(function (reject) {
            console.log("NFC is not supported by your Device");
        });
    };
    HomePage.prototype.addListenNFC = function () {
        var _this = this;
        this.nfc.addNdefListener(function (nfcEvent) { return _this.sesReadNFC(nfcEvent.tag); }).subscribe(function (data) {
            if (data && data.tag && data.tag.id) {
                var tagId = _this.nfc.bytesToHexString(data.tag.id);
                if (tagId) {
                    _this.tagId = tagId;
                    _this.scanned = true;
                    // only testing data consider to ask web api for access
                    _this.granted = [
                        "dd3b515f"
                    ].indexOf(tagId) != -1;
                    var payload = data.tag.ndefMessage[0].payload;
                    _this.payloadNFC = payload;
                    console.log("Payload:", payload);
                    var tagContent = _this.nfc.bytesToString(payload).substring(3);
                    _this.message = tagContent;
                    //let mes = this.nfc.bytesToHexString(data.tag.ndefMessage[0].payload);
                    // 		let mes = data.tag;
                    // if(mes) {
                    // 	this.status = "Reading the tag...";
                    // 	//this.message = this.decodePayload(mes);
                    // 	this.temp = JSON.stringify(mes);
                    // 	this.message = JSON.parse(this.temp);
                    // 	this.status = "Tag read successfully";
                    // }
                }
                else {
                    alert('NFC_NOT_DETECTED');
                }
            }
        });
    };
    HomePage.prototype.sesReadNFC = function (data) {
        this.status = "Reading the tag...";
        // for(var prop in data) {
        // 	// this.message += prop;
        // 	this.message = 'bbllaa';
        // 	if(prop == 'techTypes') {
        // 		this.status = 'bla';
        // 		this.message += 'types';
        // 	}
        // 	if(prop == 'isWritable') {
        // 		this.status = 'bla2';
        // 		this.message += 'isWritable';
        // 	}
        // }
        console.log('DATA:', data);
        this.status = "Tag read successfully";
    };
    HomePage.prototype.failNFC = function (err) {
        alert("Error while reading: Please Retry");
    };
    HomePage.prototype.decodePayload = function (data) {
        var languageCodeLength = (data[0] & 0x3F), // 6 LSBs
        languageCode = data.slice(1, 1 + languageCodeLength), utf16 = (data[0] & 0x80) !== 0; // assuming UTF-16BE
        // TODO need to deal with UTF in the future
        // console.log("lang " + languageCode + (utf16 ? " utf16" : " utf8"));
        return this.nfc.bytesToString(data.slice(languageCodeLength + 1));
    };
    HomePage.prototype.navToScreeningPage = function () {
        console.log('Navigating to another module');
        this.navCtrl.push(ScreeningPage);
    };
    HomePage.prototype.navToResultsPage = function () {
        console.log('navigating within same module');
        this.navCtrl.push(ResultsPage);
    };
    HomePage.prototype.navToAppointmentsPage = function () {
        console.log();
        this.navCtrl.push(AppointmentPage);
    };
    HomePage.prototype.newEntry = function (entryType) {
        console.log(this.message);
        this.navCtrl.push('NewEntryPage', {
            'valueType': entryType,
            'value': this.message,
            'unit': 'mg/dL'
        });
    };
    HomePage.prototype.changeObjective = function (objective) {
        this.navCtrl.push('ObjectivePage', {
            'valueType': 'glycaemia'
        });
    };
    HomePage.prototype.displayAgp = function () {
        var _this = this;
        Highcharts.setOptions({ global: { useUTC: false } });
        var agpChart = {
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                opposite: true,
                crosshair: true,
                tickInterval: 3600 * 1000,
                labels: {
                    formatter: function () {
                        return Highcharts.dateFormat('%H:%M', this.value);
                    },
                    overflow: 'justify',
                    rotation: -45,
                    step: 2
                },
                plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#e6e6e6',
                        dashStyle: 'solid',
                        label: {
                            text: '',
                            align: 'center',
                            y: 12,
                            x: 0
                        }
                    }, {
                        value: 0,
                        width: 1,
                        color: '#e6e6e6',
                        dashStyle: 'solid',
                        label: {
                            text: '',
                            align: 'right',
                            y: 60,
                            x: 0
                        }
                    }, {
                        value: 0,
                        width: 1,
                        color: '#e6e6e6',
                        dashStyle: 'solid',
                        label: {
                            text: '',
                            align: 'right',
                            y: 60,
                            x: 0
                        }
                    }, {
                        value: 0,
                        width: 1,
                        color: '#e6e6e6',
                        dashStyle: 'solid',
                        label: {
                            text: '',
                            align: 'right',
                            y: 60,
                            x: 0
                        }
                    }]
            },
            yAxis: {
                title: {
                    text: ''
                },
                gridLineWidth: 0,
                minorGridLineWidth: 0,
                plotLines: [{
                        value: 180,
                        width: 1.3,
                        dashStyle: 'dash',
                        color: '#9e9e9e',
                        zIndex: 5,
                        label: {
                            text: 'max (180 mg/dL)',
                            align: 'left',
                            x: 5
                        }
                    }, {
                        value: 70,
                        color: '#9e9e9e',
                        dashStyle: 'dash',
                        width: 1.3,
                        zIndex: 99,
                        label: {
                            text: 'min (70 mg/dL)',
                            x: -5,
                            y: 15,
                            align: 'right'
                        }
                    }, {
                        value: null,
                        width: 2,
                        color: 'blue',
                        zIndex: 1,
                        dashStyle: 'ShortDot',
                        label: {
                            text: null,
                            align: 'center',
                            x: 5,
                            y: 20
                        }
                    }],
                min: 0,
                max: 220,
                lineColor: '#e6e6e6',
                lineWidth: 3
            },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false
                    },
                    fillOpacity: 0.2
                }
            },
            legend: {
                align: 'center',
                //layout: 'vertical',
                verticalAlign: 'bottom',
                floating: true,
                x: 0,
            },
            credits: {
                enabled: false
            },
            series: [],
            responsive: {
                rules: []
            },
            options: {
                chart: {},
                tooltip: {
                    shared: true
                }
            },
            noData: 'No data'
        };
        return new Promise(function (resolve, reject) {
            _this.remoteService.getGlycaemiaEntries(_this.from, _this.to).then(function (data) {
                //this.entries = data['series'];
                agpChart.series = data['series'];
                _this.agpChart = Highcharts.chart('agp', agpChart);
                resolve();
            }).catch(function (err) {
                _this.doesnotwork = ' NO ENTRIES NEW GLY';
                _this.doesnotwork += ' ' + err.name + ' ' + err.error + ' ' + err.status + ' ' + err.message;
                _this.agpChart = Highcharts.chart('agp', agpChart);
                reject(err);
            });
        });
    };
    HomePage.prototype.ionViewDidLoad = function () {
        //console.log("ionViewDidLoad");
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html'
        }),
        __metadata("design:paramtypes", [NavController, NFC, RemoteProvider])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map