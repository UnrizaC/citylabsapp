import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-termsofuse',
  templateUrl: 'termsofuse.html',
})
export class TermsofusePage {

  private termsofuse: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsofusePage');
    this.termsofuse = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
                      + "In sed lacus lacus. Quisque elit eros, blandit non ultricies a, "
                      + "ultrices eget tortor. Sed convallis mattis dui at suscipit. Proin eget eleifend mi."
                      + " Vivamus tincidunt placerat justo, eget luctus mauris." 
                      + "Class aptent taciti sociosqu ad litora torquent per conubia nostra, "
                      + "per inceptos himenaeos. Vivamus in libero eu tellus egestas varius. " 
                      + "Phasellus imperdiet, metus non tincidunt sodales, odio massa sodales ligula, ac dictum nunc sem vel dui."
                      + "Mauris venenatis posuere erat, ut dignissim mauris accumsan vitae."
                      + " Sed sed est sem. Morbi et erat faucibus, ornare mi eu, cursus nunc."
                      + " Maecenas eu scelerisque lorem. Morbi ac ipsum viverra, ullamcorper mauris vel, condimentum justo."
                      + "Duis sed nisi quis nulla bibendum dapibus ut in dui. Nulla pretium eleifend pellentesque. Aenean cursus bibendum vulputate."
                      + "Nullam est lorem, tristique vitae dolor in, tempus consectetur velit." 
                      + "Duis sed rutrum justo. Aenean nec pellentesque ligula."
                      + "Mauris molestie leo nunc, sit amet malesuada nisi faucibus quis." 
                      + "Donec nec interdum nibh, a aliquam felis. In finibus congue vestibulum."
                      + "Cras quis enim convallis, consequat tellus in, dictum urna." 
                      + " Nunc vehicula interdum ipsum, eu tristique nulla interdum eget. "
                      + " Nullam placerat finibus pellentesque. Ut vel tristique arcu, ac feugiat massa.";
  }

}
