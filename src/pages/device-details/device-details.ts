import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';

//Services UUIDs
const GLUCOSE_SERVICE = '1808';
const BATTERY_SERVICE = '180f';
const TIME_SERVICE = '1805';
const DEVICE_SERVICE = '180a';
const GLUCOSE_FEATURE = '2a51';
const GLUCOSE_RACP_WRITE = '2a52';
const GLUCOSE_MEASUREMENT = '2a18';
const GLUCOSE_MEASUREMENT_CONTEXT = '2a34';
const BATTERY_READ = '2a19';
const TIME_READ = '2a2b';

@IonicPage()
@Component({
  selector: 'page-device-details',
  templateUrl: 'device-details.html',
})
export class DeviceDetailsPage {

  peripheral: any = {};
  statusMessage: string;
  batteryStatus: any;
  glucoseRecord: any;
  numberRecords: any;
  numberRecordsLength: any;
  glucoseFeature: any;
  currentTimeGlucoseMeter: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private ble: BLE,
              private toastCtrl: ToastController,
              private ngZone: NgZone) {

    let device = navParams.get('device');
    this.setStatus('Connecting to ' + device.name || device.id);

    this.ble.connect(device.id).subscribe(
      peripheral => this.onConnected(peripheral),
      peripheral => this.onDeviceDisconnected(peripheral)
    );
  }

    onConnected(peripheral) {
      this.ngZone.run(() => {
        this.setStatus('Connected to ' + (peripheral.name || peripheral.id));
        //this.peripheral = peripheral;
        this.ngZone.run(() => {
          this.peripheral = peripheral;
        });

        // //get current battery status
        // this.ble.read(peripheral.id, BATTERY_SERVICE, BATTERY_READ).then(
        //   buffer => {
        //     var data = new Uint8Array(buffer);
        //     this.ngZone.run(() => {
        //       this.batteryStatus = data[0];
        //     });
        //   }
        // );

        // //get current time
        // this.ble.read(peripheral.id, TIME_SERVICE, TIME_READ).then(
        //   buffer => {
        //     var data = new Uint8Array(buffer);
        //     this.ngZone.run(() => {
        //       this.currentTimeGlucoseMeter = data[0];
        //     });
        //   }
        // );

        // //get glucose features
        // this.ble.read(peripheral.id, GLUCOSE_SERVICE, GLUCOSE_FEATURE).then(
        //   buffer => {
        //     var data = new Uint8Array(buffer);
        //     this.ngZone.run(() => {
        //       this.glucoseFeature = data;
        //     });
        //   }
        // );

        //start receiving glucose measurements
        this.ble.startNotification(this.peripheral.id, GLUCOSE_SERVICE, GLUCOSE_MEASUREMENT).subscribe(
          data => {
            this.onReceivedData(data);
            this.setStatus('Received some data!');
          },
          () => this.setStatus('Failed to subscribe to glucose measurements')
        );

        // //start receiving glucose measurements
        this.ble.startNotification(this.peripheral.id, GLUCOSE_SERVICE, GLUCOSE_MEASUREMENT_CONTEXT).subscribe(
          data => this.onReceivedData(data),
          () => this.setStatus('Failed to subscribe to glucose context data')
        );

        this.requestNumberofStoredRecords();
        //this.requestStoredRecords();
      });
    }

    requestNumberofStoredRecords() {
      this.setStatus('Requesting Num of Records...');
      var data = new Uint8Array(2);
      data[0] = 0x04;
      data[1] = 0x01;
      
      this.ble.write(this.peripheral.id, GLUCOSE_SERVICE, GLUCOSE_RACP_WRITE, data.buffer).then(
        number => {
          var numberOfRecords = new Uint16Array(number);
          this.ngZone.run(() => {
            this.numberRecords = numberOfRecords[0];
            this.numberRecordsLength = numberOfRecords.length;
          });
        },
        // () => {
        //   this.setStatus('Requested the number of records successfully!');
        // },
        () => {
          this.setStatus('Error while retrieving the number of stored records.');
        }
      );
    }

    requestStoredRecords() {
      this.setStatus('Trying to write to device!');
      //write to RACP to get glucose measurements
      //send 2 bytes to request stored records AND all stored records
      var records = new Uint8Array(2);
      records[0] = 0x01;
      records[1] = 0x01;
      //records[2] = 0;

      this.ble.write(this.peripheral.id, GLUCOSE_SERVICE, GLUCOSE_RACP_WRITE, records.buffer).then(
        () => {
          this.setStatus('Wrote to RACP');
        },
        () => {
          this.setStatus('Error writing to RACP');
        }
      );
    }

    onReceivedData(buffer:ArrayBuffer) {
      var data = new Uint8Array(buffer);
      this.ngZone.run(() => {
        this.glucoseRecord = data;
      });
    }

    onDeviceDisconnected(peripheral) {
      let toast = this.toastCtrl.create({
        message: 'The peripheral unexpectedly disconnected',
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }

    // Disconnect peripheral when leaving the page
    ionViewWillLeave() {
      console.log('ionViewWillLeave disconnecting Bluetooth');
      this.ble.disconnect(this.peripheral.id).then(
        () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
        () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
      )
    }

    setStatus(message) {
      console.log(message);
      this.ngZone.run(() => {
        this.statusMessage = message;
      });
    }

    // ASCII only
    stringToBytes(string) {
      var array = new Uint8Array(string.length);
      for (var i = 0, l = string.length; i < l; i++) {
          array[i] = string.charCodeAt(i);
      }
      return array.buffer;
    }

    // ASCII only
    bytesToString(buffer) {
      return String.fromCharCode.apply(null, new Uint8Array(buffer));
    }

}
