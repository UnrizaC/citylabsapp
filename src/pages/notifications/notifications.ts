import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
	notifications:Array<{class: string, name: string, number: number, title: string, date: string, text: string}> = [];
	messageSteps: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private translate: TranslateService) {
  	this.getNotifications();
  }

  getNotifications() {
    this.notifications.push({
      class: 'activity', name: 'walk', number: 1000, title: 'steps', date: '23/08/2018', text: 'Great job! Yesterday you broke your record number of steps!'
    },{
      class: 'measurement', name: 'timer', number: 12, title: 'measurements in a day', date: '23/08/2018', text: 'Great job! Yesterday you broke your record number of measurements!'
    },{
      class: 'literacy', name: 'book', number: 5, title: 'correct answers in a quiz!', date: '23/08/2018', text: 'Bravo! You are an expert in diabetes!'
    });

    // this is how to request a translation from @Component
  	// this.translate.get("Great job! Yesterday you broke your record number of steps!").subscribe(value => {
  	// 	this.messageSteps = value;
  	// });
  }

  dismissAllNotifications() {
  	this.notifications.splice(0, this.notifications.length);
  }

}
