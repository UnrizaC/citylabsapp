import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { RemoteProvider } from '../../providers/remote/remote';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  private email: string;
  private resetSuccess: boolean = false;
  private resetForm: FormGroup;
  private captchaPassed: boolean = false;
  private captchaResponse: string = '';

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public remote: RemoteProvider,
    public toastCtrl: ToastController,
    public fb: FormBuilder,
    public zone: NgZone) {
    this.resetForm = fb.group({
      "email": [this.email, Validators.compose([Validators.minLength(3), Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  // captchaResolved(response: string): void {

  //   this.zone.run(() => {
  //     this.captchaPassed = true;
  //     this.captchaResponse = response;
  //   });

  // }

  resetPassword() {
    let reset: any = {
      email: this.resetForm.controls["email"].value,
      captcha: this.captchaResponse
    };
    this.remote.lostPassword(reset).then((data) => {
      if (data) {
        this.resetSuccess = true;
      }
    }).catch(e => {
      console.log(e);
      this.sendNotification('Password Reset Failed! Try again.');
    });

  }

  sendNotification(message): void {
    let notification = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    notification.present();
  }

}
