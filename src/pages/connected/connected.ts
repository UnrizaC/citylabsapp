import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-connected',
  templateUrl: 'connected.html',
})
export class ConnectedPage {

  private devices: Array<{name: string, enabled: boolean}> = [];
  private services: Array<{name: string, enabled: boolean}> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConnectedPage');
    this.devices = [{ name: 'BioSensor BT GlucoNavii', enabled: true}, { name: 'Menarini Glucomen Areo NFC', enabled: false}];
    this.services = [{ name: 'Google Fit', enabled: false}, { name: 'Withings', enabled: false}];
  }

}
