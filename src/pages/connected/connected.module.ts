import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ConnectedPage } from './connected';

@NgModule({
  declarations: [
    ConnectedPage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectedPage),
    TranslateModule.forChild()
  ],
})
export class ConnectedPageModule {}
