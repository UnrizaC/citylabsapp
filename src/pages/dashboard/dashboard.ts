import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer } from 'ionic-angular';
import { NotificationsPage } from '../notifications/notifications';
import { Health } from '@ionic-native/health';

@IonicPage()
@Component({
	selector: 'page-dashboard',
	templateUrl: 'dashboard.html',
})
export class DashboardPage {
	
	bglManagement: number;
	dietScore: number;
	fitnessScore: number;
	literacyScore: number;
	bglcolor: any;
	fitnesscolor: any;
	dietcolor: any;
	literacycolor: any;
	responseFit: any;
	todaySteps: any = 0;
	todayCalories: any = 0;
	hba1c: any = 6.5;


	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				private health: Health) {
					
		this.bglManagement = 50;
		this.dietScore = 32;
		this.fitnessScore = 63;
		this.literacyScore = 41;
		this.bglcolor = "#095256";
		this.fitnesscolor = "#5AAA95";
		this.dietcolor = "#86A873";
		this.literacycolor = "#BB9F06";
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad DashboardPage');
		this.initGoogleFit();
	}

	navToNotificationsPage() {
		this.navCtrl.push(NotificationsPage);
	}

	newEntry(entryType: string, fab: FabContainer) {
		fab.close();
		this.navCtrl.push('NewEntryPage',
			{
				'valueType': entryType,
				'unit': 'mg/dL'
			}
		);
	}

	initGoogleFit() {
		this.health.isAvailable().then((available: boolean) => {
			console.log(available);
			this.health.requestAuthorization([
				'distance', 'nutrition',
				{
					read: ['steps'],
					write: ['height', 'weight']
				}
			])
				.then(res => {
					console.log(res);
					//this.getHeight();
					this.getTodaysSteps();
					this.getTodayCalories();
				})
				.catch(e => console.log(e));
		}).catch(e => console.log(e));
	}

	getHeight() {
		this.health.query({
			startDate: new Date(new Date().getTime() - 3 * 24 * 60 * 60 * 1000), // three days ago
			endDate: new Date(), // now
			dataType: 'height',
			limit: 1000
		}).then(data => {
			console.log(data);
			this.responseFit = JSON.stringify(data);
		}).catch(e => console.log(e));
	}

	getTodaysSteps() {
		this.health.queryAggregated({
			startDate: new Date(new Date().setUTCHours(0, 0, 0, 0)), //today midnight
			endDate: new Date(), // now
			dataType: 'steps',
			bucket: 'day',
			filtered: true
		}).then(data => {
			console.log(data);
			this.responseFit = JSON.stringify(data);
			data.forEach(day => {
				this.todaySteps = day.value;
			});
		}).catch(e => console.log(e));
	}

	getTodayCalories() {
		this.health.queryAggregated({
			startDate: new Date(new Date().setUTCHours(0, 0, 0, 0)), //today midnight
			endDate: new Date(), // now
			dataType: 'calories',
			bucket: 'day',
			filtered: true
		}).then(data => {
			data.forEach(day => {
				this.todayCalories = day.value;
			});
		}).catch(e => console.log(e));
	}

}
