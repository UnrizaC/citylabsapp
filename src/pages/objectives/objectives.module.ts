import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { ObjectivesPage } from './objectives';

@NgModule({
  declarations: [
    ObjectivesPage,
  ],
  imports: [
    IonicPageModule.forChild(ObjectivesPage),
    TranslateModule.forChild()
  ],
})
export class ObjectivesPageModule {}
