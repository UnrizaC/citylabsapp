import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, MenuController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { RegistrationPage } from '../registration/registration';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';
import { AuthorizationProvider } from '../../providers/authorization/authorization';
import { SyncProvider } from '../../providers/sync/sync';
import { TabsPage } from '../../pages/tabs/tabs';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  @ViewChild('email') email: any;
  private username: string;
  private userProfile: any;
  private password: string;
  private error: string;
  //private token: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public auth: AuthorizationProvider,
    public toastCtrl: ToastController,
    public menuCtrl: MenuController,
    private storage: Storage) {
    this.menuCtrl.close();
    this.menuCtrl.enable(false, 'sidebarMenu');
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.email.setFocus();
    }, 500);
  }

  login(): void {
    this.auth.login(this.username, this.password).then((data) => {
      console.log(data);
      //window.localStorage.setItem('email', this.email);
      this.storage.set('email', this.username);
      //window.localStorage.setItem('jwtoken', data['token']);
      //console.log(window.localStorage.getItem('email'));
      this.navCtrl.push(TabsPage);
    }).catch((err) => {
      console.log('REMOTE LOGIN FAILED');
      this.sendNotification('REMOTE LOGIN FAILED! TRY AGAIN');
      // LOCAL AUTHENTICATION ONLY
      // this.auth.loginLocally(this.username, this.password).then((data) => {
      //   console.log('FROM DB:', data);
      //   let user = <any>{};
      //   user = data;
      //   this.userProfile = data;
      //   window.localStorage.setItem('email', user.email);
      //   if (user.email == this.username && user.password == this.password) {
      //     this.navCtrl.push(TabsPage);
      //   }
      // }).catch((err) => {
      //   console.log(err);
      //   console.log('LOCAL LOGIN FAILED');
      //   this.sendNotification('LOCAL LOGIN FAILED! TRY AGAIN');
      // });
    });
  }

  getUserProfile() {
    return this.userProfile;
  }

  navToRegistrationPage() {
    this.navCtrl.push(RegistrationPage);
  }

  navToForgotPasswordPage() {
    this.navCtrl.push(ForgotPasswordPage);
  }

  sendNotification(message): void {
    let notification = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    notification.present();
  }
}
