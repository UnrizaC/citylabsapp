var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { RegistrationPage } from '../registration/registration';
import { AuthorizationProvider } from '../../providers/authorization/authorization';
import { TabsPage } from '../../pages/tabs/tabs';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, auth, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.toastCtrl = toastCtrl;
        this.token = false;
    }
    LoginPage_1 = LoginPage;
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // console.log('ionViewDidLoad LoginPage');
        setTimeout(function () {
            _this.email.setFocus();
        }, 500);
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        this.auth.login(this.username, this.password).then(function (data) {
            console.log(data);
            _this.navCtrl.push(TabsPage);
        }).catch(function (err) {
            console.log('LOGIN FAILED');
            _this.sendNotification('LOGIN FAILED! TRY AGAIN');
            // console.log(this.username, this.password);
        });
    };
    LoginPage.prototype.logout = function () {
        this.token = this.auth.logout();
        this.navCtrl.setRoot(LoginPage_1);
    };
    LoginPage.prototype.hasValidIdToken = function () {
        return this.token;
    };
    LoginPage.prototype.navToRegistrationPage = function () {
        this.navCtrl.push(RegistrationPage);
    };
    LoginPage.prototype.sendNotification = function (message) {
        var notification = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        notification.present();
    };
    var LoginPage_1;
    __decorate([
        ViewChild('email'),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "email", void 0);
    LoginPage = LoginPage_1 = __decorate([
        Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, AuthorizationProvider, ToastController])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map