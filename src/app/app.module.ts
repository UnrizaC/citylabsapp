import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, Injector, Injectable } from '@angular/core';
import { Pro } from '@ionic/pro';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule, Http } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { ScreeningPage } from '../pages/screening/screening';
import { AppointmentPage } from '../pages/appointment/appointment';
import { ResultsPage } from '../pages/results/results';
import { TabsPage } from '../pages/tabs/tabs';
import { NewsPage } from '../pages/news/news';
import { ResultDetailsPage } from '../pages/result-details/result-details';
import { RegistrationPage } from '../pages/registration/registration';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { ObjectivesPage } from '../pages/objectives/objectives';
import { UserprofilePage } from '../pages/userprofile/userprofile';
import { LoginPage } from '../pages/login/login';
import { NotificationsPage } from '../pages/notifications/notifications';
import { SettingsPage } from '../pages/settings/settings';
import { DeviceDetailsPage } from '../pages/device-details/device-details';
import { BluetoothPage } from '../pages/bluetooth/bluetooth';
import { ConnectedPage } from '../pages/connected/connected';
import { AboutPage } from '../pages/about/about';
import { TermsofusePage } from '../pages/termsofuse/termsofuse';
import { PdfReportPage } from '../pages/pdf-report/pdf-report';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';

import { ProgressbarComponent } from '../components/progressbar/progressbar';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NFC, Ndef } from "@ionic-native/nfc";
import { Network } from '@ionic-native/network';
import { Vibration } from '@ionic-native/vibration';
import { SQLite } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { Stepcounter } from '@ionic-native/stepcounter';
import { BLE } from '@ionic-native/ble';
import { Health } from '@ionic-native/health';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Base64 } from '@ionic-native/base64';
import { FileTransfer} from '@ionic-native/file-transfer';
import { IonicStorageModule } from '@ionic/storage';


import { EntriesProvider } from '../providers/entries/entries';
import { RemoteProvider } from '../providers/remote/remote';
import { AuthorizationProvider } from '../providers/authorization/authorization';
import { DatabaseProvider } from '../providers/database/database';
import { UserProvider } from '../providers/user/user';
import { NetworkProvider } from '../providers/network/network';
import { SyncProvider } from '../providers/sync/sync';

Pro.init('D0F9A57F', {
  appVersion: '0.0.1'
})

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@Injectable()
export class MyErrorHandler implements ErrorHandler {
  ionicErrorHandler: IonicErrorHandler;

  constructor(injector: Injector) {
    try {
      this.ionicErrorHandler = injector.get(IonicErrorHandler);
    } catch (e) {
      // Unable to get the IonicErrorHandler provider, ensure
      // IonicErrorHandler has been added to the providers list below
    }

    Pro.monitoring.exception(new Error('error'));
  }

  handleError(err: any): void {
    Pro.monitoring.handleNewError(err);
    // Remove this if you want to disable Ionic's auto exception handling
    // in development mode.
    this.ionicErrorHandler && this.ionicErrorHandler.handleError(err);
  }
}

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    HomePage,
    ScreeningPage,
    ResultsPage,
    AppointmentPage,
    TabsPage,
    NewsPage,
    ResultDetailsPage,
    RegistrationPage,
    DashboardPage,
    ObjectivesPage,
    UserprofilePage,
    NotificationsPage,
    SettingsPage,
    DeviceDetailsPage,
    BluetoothPage,
    ConnectedPage,
    AboutPage,
    TermsofusePage,
    PdfReportPage,
    ForgotPasswordPage,
    ProgressbarComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    //RecaptchaModule.forRoot(),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    ScreeningPage,
    ResultsPage,
    AppointmentPage,
    TabsPage,
    NewsPage,
    ResultDetailsPage,
    RegistrationPage,
    DashboardPage,
    ObjectivesPage,
    NotificationsPage,
    UserprofilePage,
    SettingsPage,
    BluetoothPage,
    ConnectedPage,
    AboutPage,
    TermsofusePage,
    PdfReportPage,
    ForgotPasswordPage,
    DeviceDetailsPage
  ],
  providers: [
    NFC,
    Ndef,
    SQLite,
    StatusBar,
    SplashScreen,
    EntriesProvider,
    IonicErrorHandler,
    { provide: ErrorHandler, useClass: MyErrorHandler },
    Image,
    RemoteProvider,
    AuthorizationProvider,
    DatabaseProvider,
    UserProvider,
    Vibration,
    Network,
    NetworkProvider,
    SyncProvider,
    Toast,
    Stepcounter,
    BLE,
    Health,
    File,
    FileTransfer,
    FileOpener,
    Base64
  ]
})
export class AppModule { }
