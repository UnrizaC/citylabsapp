var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ScreeningPage } from '../pages/screening/screening';
import { AppointmentPage } from '../pages/appointment/appointment';
import { ResultsPage } from '../pages/results/results';
import { TabsPage } from '../pages/tabs/tabs';
import { NewsPage } from '../pages/news/news';
import { ResultDetailsPage } from '../pages/result-details/result-details';
import { RegistrationPage } from '../pages/registration/registration';
import { LoginPage } from '../pages/login/login';
import { NFC } from "@ionic-native/nfc";
import { EntriesProvider } from '../providers/entries/entries';
import { RemoteProvider } from '../providers/remote/remote';
import { AuthorizationProvider } from '../providers/authorization/authorization';
//import { Image } from '../providers/image/image';
//import { Camera } from '@ionic-native/camera'
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                MyApp,
                LoginPage,
                HomePage,
                ScreeningPage,
                ResultsPage,
                AppointmentPage,
                TabsPage,
                NewsPage,
                ResultDetailsPage,
                RegistrationPage
            ],
            imports: [
                BrowserModule,
                HttpModule,
                HttpClientModule,
                NgCircleProgressModule.forRoot({
                    // set defaults here
                    percent: 70,
                    radius: 100,
                    outerStrokeWidth: 16,
                    innerStrokeWidth: 8,
                    outerStrokeColor: "#78C000",
                    innerStrokeColor: "#C7E596",
                    animationDuration: 300,

                  }),
                IonicModule.forRoot(MyApp),
            ],
            bootstrap: [IonicApp],
            entryComponents: [
                MyApp,
                LoginPage,
                HomePage,
                ScreeningPage,
                ResultsPage,
                AppointmentPage,
                TabsPage,
                NewsPage,
                ResultDetailsPage,
                RegistrationPage
            ],
            providers: [
                NFC,
                StatusBar,
                SplashScreen,
                EntriesProvider,
                { provide: ErrorHandler, useClass: IonicErrorHandler },
                Image,
                RemoteProvider,
                AuthorizationProvider
            ]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map