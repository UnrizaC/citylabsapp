import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController, Events, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';

import { TranslateService } from '@ngx-translate/core';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { ObjectivesPage } from '../pages/objectives/objectives';
import { UserprofilePage } from '../pages/userprofile/userprofile';
import { SettingsPage } from '../pages/settings/settings';
import { ConnectedPage } from '../pages/connected/connected';
import { AboutPage } from '../pages/about/about';
import { PdfReportPage } from '../pages/pdf-report/pdf-report';

import { AuthorizationProvider } from '../providers/authorization/authorization';
import { NetworkProvider } from '../providers/network/network';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
  rootPage:any = LoginPage;
  pages: any;

  constructor(private platform: Platform, 
              private statusBar: StatusBar, 
              private splashScreen: SplashScreen,
              public translate: TranslateService,
              public authorizationProvider: AuthorizationProvider,
              public menuCtrl: MenuController,
              public networkProvider: NetworkProvider,
              public network: Network,
              public events: Events,
              public toast: ToastController) {
    

    if (authorizationProvider.isLoggedIn()) {
      this.rootPage = TabsPage;
    } else {
      this.rootPage = LoginPage;
    }

    translate.setDefaultLang('en');
    //translate.use('fr');

    this.pages = [UserprofilePage, ObjectivesPage, SettingsPage, ConnectedPage, AboutPage, PdfReportPage];

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      statusBar.backgroundColorByHexString('#ffffff');
      splashScreen.hide();

      this.networkProvider.initializeNetworkEvents();

         // Offline event
      this.events.subscribe('network:offline', (status) => {
        console.log('network:offline' + this.network.type);
          //alert('network:offline ==> '+this.network.type);
          const toast = this.toast.create({
            message: `Network is ${status}`,
            duration: 5000
          });
          toast.present(); 
      });

      // Online event
      this.events.subscribe('network:online', (status) => {
        console.log('network:online' + this.network.type);
          //alert('network:online ==> '+this.network.type);
          const toast = this.toast.create({
            message: `Network is ${status}`,
            duration: 5000
          });
          toast.present();       
      });
    });
  }

  openPage(page) {
    this.menuCtrl.close();
    this.navCtrl.push(page);
  }

  logout() {
    this.authorizationProvider.logout();
    this.navCtrl.setRoot(LoginPage);
  }

  // checkPreviousAuthorization(): void { 
  //   if((window.localStorage.getItem('username') === "undefined" || window.localStorage.getItem('username') === null) && 
  //      (window.localStorage.getItem('password') === "undefined" || window.localStorage.getItem('password') === null)) {
  //     this.rootPage = LoginPage;
  //   } else {
  //     this.rootPage = TabsPage;
  //   }
  // }
}

