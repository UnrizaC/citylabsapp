import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RemoteProvider } from '../remote/remote';
import { DatabaseProvider } from '../database/database';

@Injectable()
export class SyncProvider {
  db: any;
  lastSyncTimestamp: Date;

  constructor(public http: HttpClient, public dbProvider: DatabaseProvider) {
    console.log('Hello SyncProvider Provider');
    //this.setDB();
  }

  getLastSyncTimestamp() {
    return this.lastSyncTimestamp;
  }

  updateLastSyncTimestamp() {
    this.lastSyncTimestamp = new Date(new Date().setDate(new Date().getDate() - 20));
  }

  setTimestamptoNow() {
    this.lastSyncTimestamp = new Date();
  }

  setDB() {
    //this._DB = this.pouchDB.getDB();
  }

  checkEntryExists(data) {
  	return new Promise(resolve => {
  		resolve(false);
  	});
  }

  addMissingEntry(data) {
  	console.log(data);
  }

}
