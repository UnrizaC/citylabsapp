import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { DatabaseProvider } from '../database/database';
import { UserProvider } from '../user/user';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { Storage } from '@ionic/storage';

/*
Class that supports the communication with a local SQLite Database

*/

@Injectable()
export class EntriesProvider {
    private db: any;
    private logbook: any;
    private success: boolean = true;
    myDate: Date;
    myDateStr: String;
    userId: any;

    constructor(public http: Http,
        public alertCtrl: AlertController,
        public dbProvider: DatabaseProvider,
        private userProvider: UserProvider,
        private sqlite: SQLite,
        private toast: Toast,
        private storage: Storage) {
        this.setDB();
        this.myDate = new Date();
        this.myDateStr = new Date().toISOString();
        //this.getAllDocs();
    }

    setDB() {
        this.db = this.dbProvider.getDb();
    }

    getAllDocs() {
        //this._DB.allDocs().then(entries => console.log('ALL', entries));
    }

    getLogbook() {
        return this.logbook;
    }

    getEntry(id) {
        return new Promise(resolve => {
        });
    }

    /*
        Get all entries from the local DB filtered by type
        @types array an array of types
    */
    getEntries(types) {
        return new Promise((resolve, reject) => {
            this.userProvider.getUserId().then((data: any) => {
                for (let i = 0; i < data.rows.length; i++) {
                    let item = data.rows.item(i);
                    this.userId = item.rowid;
                    return item.rowid;
                }
            }).then(res => {
                this.sqlite.create({
                    name: 'egle.db',
                    location: 'default'
                }).then((db: SQLiteObject) => {
                    db.executeSql('SELECT * FROM entries WHERE type=? AND user=?', [types[0], res])
                        .then(res => {
                            //if(res.rows.length > 0) {
                            console.log(res);
                            resolve(res);
                            //}
                        })
                        .catch(e => {
                            console.log(e);
                            this.toast.show(e, '5000', 'center').subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                            reject(e);
                        });
                }).catch(e => {
                    console.log(e);
                    this.toast.show(e, '5000', 'center').subscribe(
                        toast => {
                            console.log(toast);
                        }
                    );
                    reject(e);
                });
            }).catch(e => {
                console.log(e);
            });
        });
    }

    /*
        Add a new entry to a local DB
        @fields object from the new entry form
    */
    addEntry(fields) {
        var timeStamp = new Date().toISOString(),
            title, unit, userId;
        switch (fields.valueType) {
            case 'glycaemia':
                title = 'Glycaemia measure';
                unit = 'mg/dL';
                break;
            case 'meal':
                title = 'Meal log';
                unit = '';
                break;
            case 'weight':
                title = 'Weight';
                unit = 'kg';
                break;
            case 'bp':
                title = 'Blood Pressure';
                unit = 'mm/Hg';
                break;
            case 'hba1c':
                title = 'HbA1c';
                unit = '%';
                break;
            case 'activity':
                title = 'Physical activity';
                unit = '';
                break;
            case 'meds':
                title = 'Medication log';
                unit = '';
                break;
            default:
                title = 'New measure'
                break;
        }

        var entry = {
            user: "",
            date: fields.acquisitionDate,
            title: title,
            unit: unit,
            type: fields.valueType,
            time: timeStamp,
            comment: fields.comment,
            //optional fields below
            value: "",
            profilePicture: ""
        };

        if (fields.value !== undefined) {
            entry.value = fields.value;
        }

        return new Promise((resolve, reject) => {
            this.userProvider.getUserId().then((data: any) => {
                if (data.rows.length > 0) {
                    for (let i = 0; i < data.rows.length; i++) {
                        let item = data.rows.item(i);
                        userId = item.rowid;
                        entry.user = item.rowid;
                        return userId;
                    }
                } else {
                    //user is not saved locally, we have to add it
                    this.userProvider.saveUserLocally().then((res: any) => {
                        return res.insert_id;
                    })
                }

            }).then(res => {
                //return new Promise((resolve, reject) F=> {
                this.sqlite.create({
                    name: 'egle.db',
                    location: 'default'
                }).then((db: SQLiteObject) => {
                    db.executeSql('INSERT INTO entries VALUES(NULL,?,?,?,?,?,?,?,?,?)', [res, entry.value, entry.type, entry.title, entry.date, entry.time, entry.unit, entry.profilePicture, entry.comment])
                        .then(res => {
                            console.log(res);
                            this.toast.show('New entry saved', '5000', 'center').subscribe(
                                toast => {
                                    //this.navCtrl.popToRoot();
                                }
                            );
                            resolve(true);
                        })
                        .catch(e => {
                            console.log(e);
                            this.toast.show(e, '5000', 'center').subscribe(
                                toast => {
                                    console.log(toast);
                                }
                            );
                            reject(e);
                        });
                }).catch(e => {
                    console.log(e);
                    this.toast.show(e, '5000', 'center').subscribe(
                        toast => {
                            console.log(toast);
                        }
                    );
                    reject(e);
                })
            }).catch(e => {
                console.log(e);
            });
            //});
        });
    }

    updateEntry(fields) {
        var timeStamp = new Date().toISOString(),
            title, unit;
        switch (fields.valueType) {
            case 'glycaemia':
                title = 'Glycaemia measure';
                unit = 'mg/dL';
                break;
            default:
                title = 'New measure'
                break;
        }

        var entry = {
            rowid: fields.id,
            //user: window.localStorage.getItem('email'),
            user: this.storage.get('email'),
            value: fields.value,
            type: fields.valueType,
            title: title,
            date: fields.acquisitionDate,
            time: timeStamp,
            unit: unit,
            profilePicture: "",
            comment: fields.comment
        };

        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: 'egle.db',
                location: 'default'
            }).then((db: SQLiteObject) => {
                db.executeSql('UPDATE entries SET value=?, type=?, title=?, date=?, time=?, unit=?, profilePicture=?, comment=? WHERE rowid=?',
                    [entry.value, entry.type, entry.title, entry.date, entry.time, entry.unit, entry.profilePicture, entry.comment, entry.rowid]).then(res => {
                        resolve(res);
                    }).catch(e => {
                        console.log(e);
                        reject(e);
                    });
            }).catch(e => {
                console.log(e);
                reject(e);
            });
        });
    }

    deleteEntry(id) {
        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: 'egle.db',
                location: 'default'
            }).then((db: SQLiteObject) => {
                db.executeSql('DELETE FROM entries WHERE rowid=?', [id])
                    .then(res => {
                        console.log(res);
                        resolve(res);
                    }).catch(e => {
                        console.log(e);
                        reject(e);
                    });
            }).catch(e => {
                console.log(e);
                reject(e);
            });
        });
    }

    getUserId() {
        // return new Promise((resolve, reject) => {
        //     this.sqlite.create({
        //         name: 'egle.db',
        //         location: 'default'
        //     }).then((db: SQLiteObject) => {
        //         db.executeSql('SELECT rowid FROM userprofiles WHERE email=?', [window.localStorage.getItem('email')]).then(res => {
        //             resolve(res);
        //         }).catch(e => {
        //             console.log(e);
        //             reject(e);
        //         });
        //     }).catch(e => {
        //         console.log(e);
        //         reject(e);
        //     });
        // });

        return new Promise((resolve, reject) => {
            this.userProvider.getUserId().then((data: any) => {
                if (data.rows.length > 0) {
                    resolve(data);
                } else {
                    reject(new Error('No user found in DB'));
                }
            }).catch(e => console.log(e));
        });

    }

    errorHandler(err) {
        let headsUp = this.alertCtrl.create({
            title: 'Heads Up!',
            subTitle: err,
            buttons: ['Got It!']
        });

        headsUp.present();
    }

}
