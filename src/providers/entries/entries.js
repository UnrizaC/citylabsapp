var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import cordovaSqlitePlugin from 'pouchdb-adapter-cordova-sqlite';
import pouchdbfind from 'pouchdb-find';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
/*
Class that supports the communication with a local SQLite Database

*/
var EntriesProvider = /** @class */ (function () {
    function EntriesProvider(http, alertCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.success = true;
        this.initialiseDB();
        this.myDate = new Date();
        this.myDateStr = new Date().toISOString();
    }
    EntriesProvider.prototype.initialiseDB = function () {
        //register PouchDB adapters
        PouchDB.plugin(cordovaSqlitePlugin);
        PouchDB.plugin(pouchdbfind);
        // when cordova is ready
        // register event to be notified when cordova and so cordova-sqlite-plugin-2 is ready
        //document.addEventListener("deviceready", onDeviceReady, false);
        //function onDeviceReady() {
        //load PouchDB
        this._DB = new PouchDB('comics.db', { adapter: 'cordova-sqlite' });
        this._DB.createIndex({
            index: { fields: ['type', 'value'] }
        });
        //this._DB.debug.enable('*');
        this._DB.info().then(console.log.bind(console));
        //}
    };
    EntriesProvider.prototype.getLogbook = function () {
        return this.logbook;
    };
    EntriesProvider.prototype.getEntry = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this._DB.get(id, { attachments: false })
                .then(function (doc) {
                var item = [];
                item.push({
                    id: id,
                    rev: doc._rev,
                    value: doc.value,
                    //date: doc.date,
                    time: doc.time,
                    comment: doc.comment,
                    unit: doc.unit
                });
                resolve(item);
            });
        });
    };
    EntriesProvider.prototype.getEntries = function (types) {
        var _this = this;
        return new Promise(function (resolve) {
            _this._DB.find({
                selector: {
                    type: { $in: types }
                    //sort: [{debut: 'desc'}]
                }
            }).then(function (results) {
                resolve(results.docs);
                // results.docs.forEach(
                //     function (doc) {
                //         console.log(doc);
                //     }
                // );
            }).catch(function (error) {
                console.warn("An error occurred:");
                console.error(error);
            });
            _this._DB.allDocs().then(function (entries) { return console.log(entries.rows.length); });
            //{
            //    let k,
            //        items   = [],
            //        row   = doc.rows;
            //    for(k in row)
            //    {
            //       var item = row[k].doc;
            //           //dataURIPrefix   = 'data:image/jpeg;base64,',
            //           //attachment;
            //       // if(item._attachments)
            //       // {
            //       //    attachment     = dataURIPrefix + item._attachments["character.jpg"].data;
            //       // }
            //       items.push(
            //       {
            //          id      :   item._id,
            //          rev     :   item._rev,
            //          value 	: 	item.value,
            //          type	: item.type,
            //          comment : 	item.comment
            //       });
            //    }
            //      resolve(items);
            //});
        });
        //date: date,
    };
    EntriesProvider.prototype.addEntry = function (fields) {
        var _this = this;
        var timeStamp = new Date().toISOString(), title, unit;
        switch (fields.valueType) {
            case 'glycaemia':
                title = 'Glycaemia measure';
                unit = 'mg/dL';
                break;
            default:
                title = 'New measure';
                break;
        }
        var entry = {
            _id: timeStamp,
            value: fields.value,
            type: fields.valueType,
            title: title,
            date: fields.acquisitionDate,
            //time: time,
            unit: unit,
            profilePicture: "",
            comment: fields.comment
        };
        return new Promise(function (resolve) {
            _this._DB.put(entry).catch(function (err) {
                _this.success = false;
            });
            resolve(true);
        });
    };
    EntriesProvider.prototype.updateEntry = function (fields) {
        var _this = this;
        var entry = {
            _id: fields.id,
            _rev: fields.revision,
            value: fields.value,
            type: fields.valueType,
            date: fields.acquisitionDate,
            unit: fields.unit,
            profilePicture: "",
            comment: fields.comment
        };
        return new Promise(function (resolve) {
            _this._DB.put(entry)
                .catch(function (err) {
                _this.success = false;
            });
            if (_this.success) {
                resolve(true);
            }
        });
    };
    EntriesProvider.prototype.deleteEntry = function (id, rev) {
        var _this = this;
        return new Promise(function (resolve) {
            var entry = { _id: id, _rev: rev };
            _this._DB.remove(entry)
                .catch(function (err) {
                _this.success = false;
            });
            if (_this.success) {
                resolve(true);
            }
        });
    };
    EntriesProvider.prototype.errorHandler = function (err) {
        var headsUp = this.alertCtrl.create({
            title: 'Heads Up!',
            subTitle: err,
            buttons: ['Got It!']
        });
        headsUp.present();
    };
    EntriesProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http,
            AlertController])
    ], EntriesProvider);
    return EntriesProvider;
}());
export { EntriesProvider };
//# sourceMappingURL=entries.js.map