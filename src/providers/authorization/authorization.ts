import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RemoteProvider } from '../remote/remote';
import { DatabaseProvider } from '../database/database';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthorizationProvider {
  private db: SQLiteObject;

  constructor(public http: HttpClient,
    private remoteService: RemoteProvider,
    private dbProvider: DatabaseProvider,
    private sqlite: SQLite,
    private storage: Storage) {
    this.setDB();
  }

  setDB() {
    this.db = this.dbProvider.getDb();
  }

  login(email, password) {
    return new Promise((resolve, reject) => {
      this.remoteService.getToken(email, password).then((data) => {
        resolve(data);
      }).catch((err) => {
        console.log(err);
        reject(err);
      });
    });
  }

  /**
   * Queries the local SQLite DB for a specified user. If the user is found it returns its user profile data. 
   * @returns Promise
   */
  loginLocally(email, password) {
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: 'egle.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        console.log('TRYING LOCAL AUTH...');
        db.executeSql('SELECT * FROM userprofiles WHERE email=? AND password=?', [email, password])
          .then((resp) => {
            console.log('RESP:', resp, resp.rows.length);
            if (resp.rows.length > 0) {
              for (var i = 0; i < resp.rows.length; i++) {
                let item = resp.rows.item(i);
                if (resp.rows.item(i).email === email && resp.rows.item(i).password === password) {
                  //return item;
                  resolve(item);
                }
              }
            }
          }).catch(e => reject(e));
      }).catch(e => reject(e));
    });

  }

  /**
   * Logs out the user and deletes the localStorage items: 'email' and 'jwtoken'.
   * @returns True when the localStorage items are removed. 
   */
  logout() {
    //this.token = false;
    console.log('LOGOUT authorization.ts');
    //window.localStorage.removeItem('email');
    //window.localStorage.removeItem('jwtoken');
    this.storage.remove('email');
    this.storage.remove('jwtoken');
    return true;
    //return this.token;
  }

  getToken() {
    //let jwtoken = window.localStorage.getItem('jwtoken');
    return this.storage.get('jwtoken');

    // if(jwtoken === "undefined" || jwtoken === undefined || jwtoken === null) {
    //   return false;
    // } else {
    //   return jwtoken;
    // }
  }

  getEmail() {
    // let email = window.localStorage.getItem('email');
    // if(email === "undefined" || email === undefined || email === null) {
    //   return false;
    // } else {
    //   return email;
    // }
    return this.storage.get('email');
  }

  /**
   * Checks if any user is currently logged in. If both localStorage items 'jwtoken' and 'email' are either null, undefined and/or "undefined" it will return false, otherwise true.
   * 
   * @returns True if the user is logged in. False, otherwise.
   */
  isLoggedIn() {
    if (this.getToken() && this.getEmail()) {
      return true;
    } else {
      return false;
    }
  }
}
