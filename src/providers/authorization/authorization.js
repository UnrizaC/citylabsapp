var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RemoteProvider } from '../remote/remote';
var AuthorizationProvider = /** @class */ (function () {
    function AuthorizationProvider(http, remoteService) {
        this.http = http;
        this.remoteService = remoteService;
        this.BASE_URL = 'https://beta.egle.be/api/';
        this.token = false;
    }
    AuthorizationProvider.prototype.login = function (email, password) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.remoteService.getToken(email, password).then(function (data) {
                _this.token = data['token'];
                resolve(data);
            }).catch(function (err) {
                console.log(err);
                reject(err);
            });
        });
        //this.token = true;
        //return this.token;
    };
    AuthorizationProvider.prototype.logout = function () {
        this.token = false;
        return this.token;
    };
    AuthorizationProvider.prototype.isLoggedIn = function () {
        if (this.token != false) {
            return true;
        }
        else {
            return false;
        }
    };
    AuthorizationProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, RemoteProvider])
    ], AuthorizationProvider);
    return AuthorizationProvider;
}());
export { AuthorizationProvider };
//# sourceMappingURL=authorization.js.map