import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { AlertController, Events, ToastController } from 'ionic-angular';

export enum ConnectionStatusEnum {
    Online,
    Offline
}
@Injectable()
export class NetworkProvider {
	previousStatus: any;

  constructor(public http: HttpClient,
              public alertCtrl: AlertController, 
              public network: Network,
              public eventCtrl: Events,
              private toastCtrl: ToastController) {
    console.log('Hello NetworkProvider Provider');
    this.previousStatus = ConnectionStatusEnum.Online;
  }

  public initializeNetworkEvents(): void {

        this.network.onDisconnect().subscribe(() => {
            if (this.previousStatus === ConnectionStatusEnum.Online) {
                this.eventCtrl.publish('network:offline', 'OFFLINE');
            }
            this.previousStatus = ConnectionStatusEnum.Offline;
            let toast = this.toastCtrl.create({
				message: "Connection Failed! There may be a problem in your internet connection. Please try again!",
				showCloseButton: true
				});
			toast.present();
        });

        this.network.onConnect().subscribe(() => {
           setTimeout(() => {
            if (this.previousStatus === ConnectionStatusEnum.Offline) {
                this.eventCtrl.publish('network:online', 'ONLINE');    
            }
            this.previousStatus = ConnectionStatusEnum.Online;
             }, 3000);

           let toast = this.toastCtrl.create({
				message: "Connection OK. You have internet connection!",
				duration: 3000
				});
			toast.present();
        });
    }
}
