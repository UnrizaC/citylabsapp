import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RemoteProvider } from '../remote/remote';
import { DatabaseProvider } from '../database/database';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Toast } from '@ionic-native/toast';
import { Storage } from '@ionic/storage';

/*	User profile management class	*/

@Injectable()
export class UserProvider {
  DB: any;
  userProfile: any;
  //public storage: SQLite;
  private userEmail: any;

  constructor(public http: HttpClient,
    public dbProvider: DatabaseProvider,
    private remoteService: RemoteProvider,
    private sqlite: SQLite,
    private toast: Toast,
    private storage: Storage) {
    this.getDb();
    this.userEmail = this.storage.get('email');
  }

  /**
   * Return an instance of the database's SQLiteObject to reuse the existing connection
   */
  getDb() {
    this.DB = this.dbProvider.getDb();
  }

  /**
   * Return an instance of the userProfile property
   */
  getUserProfile() {
    return this.userProfile;
  }

  /**
   * Gets a user profile data. First, it will try to retrieve it from the local SQL DB.
   * It that is not successful, it will try to retrieve it from the remote DB. 
   * It returns a Promise.
   */

  getUser() {

    var found: boolean = false;

    return new Promise((resolve, reject) => {
      this.getLocalUser().then((user) => {
        if (user) {
          resolve(user);
        } else {
          this.remoteService.getUserProfile().then((data) => {
            console.log('REMOTE PROFILE: ', data);
            resolve(data);
          }).catch(e => {
            console.log(e);
            reject(e);
          });
        }
      }).catch(e => {
        console.log(e);
        reject(e);
      });
    });

    // return new Promise((resolve, reject) => {
    //   this.sqlite.create({
    //     name: 'egle.db',
    //     location: 'default'
    //   }).then((db: SQLiteObject) => {
    //     db.executeSql('SELECT * FROM userprofiles WHERE email=?', [this.userEmail]).then(res => {
    //       if (res.rows.length > 0) {
    //         console.log(res);
    //         this.userProfile = res.rows.item(0);
    //         found = true;
    //         resolve(res.rows.item(0));
    //       }
    //     }).catch(e => {
    //       console.log(e);
    //     });
    //   }).catch(e => {
    //     console.log(e);
    //   }).then(() => {
    //     if (found === false) {
    //       this.remoteService.getUserProfile().then((data) => {
    //         console.log('BLA PROFILE: ', data);
    //         resolve(data);
    //       }).catch(e => {
    //         console.log(e);
    //         reject(e);
    //       });
    //     }
    //   }).catch(e => {
    //     console.log(e);
    //     reject(e);
    //   });
    // });
  }

  /**
   * 
   */

  getLocalUser() {
    return this.sqlite.create({
      name: 'egle.db',
      location: 'default'
    }).then((db: SQLiteObject) => {
      return db.executeSql('SELECT * FROM userprofiles WHERE email=?', [this.userEmail]).then(res => {
        if (res.rows.length > 0) {
          //console.log(res);
          for (var i = 0; i < res.rows.length; i++) {
            let item = res.rows.item(i);
            return item;
          }
        }
      }).catch(e => {
        console.log(e);
      });
    }).catch(e => {
      console.log(e);
    });

  }

  /**
   * Adds a new user to a local SQL DB.
   * @param user Is an object containing user's data.
   */

  addUser(user) {
    console.log('user.ts - USER:', user);

    return new Promise((resolve, reject) => {
      this.DB.executeSql('INSERT INTO userprofiles VALUES(NULL,?,?,?,?,?,?,?,?,?,?)', [user.email, user.password, user.firstname, user.lastname, user.birthdate, user.gender, user.language, user.condition, user.phone, user.address])
        .then(resp => {
          console.log(resp);
          resolve(resp);
          this.toast.show('New user saved', '2000', 'center').subscribe(
            toast => {
              //this.navCtrl.popToRoot();
              console.log(toast);
            }
          );
        })
        .catch(e => {
          console.log(e);
          reject(e);
          this.toast.show(e, '2000', 'center').subscribe(
            toast => {
              console.log(toast);
            }
          );
        });
    });
  }

  /**
   * Updates a user profile in a local SQL DB and on remote DB via API. User data is sent from the update modal.
   * @param userData represents an object containing updated user data from the modal, entered by a user.
   */

  updateUser(userData) {
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: 'egle.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('UPDATE userprofiles (?) SET WHERE email=?', [this.userEmail]).then(res => {
          if (res.rows.length > 0) {
            console.log(res);
            this.userProfile = res.rows.item(0);
            resolve(res.rows.item(0));
          }
        }).catch(e => {
          console.log(e);
        });
      }).catch(e => {
        console.log(e);
      }).then(() => {
        this.remoteService.updateUserProfile(userData).then((data) => {
          console.log('BLA PROFILE: ', data);
          resolve(data);
        }).catch(e => {
          console.log(e);
          reject(e);
        });
      }).catch(e => {
        console.log(e);
        reject(e);
      });
    });
  }

  /**
   * Returns an ID of a user from the local SQL DB. It returns a Promise.
   */

  getUserId() {
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: 'egle.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
        db.executeSql('SELECT rowid FROM userprofiles WHERE email=?', [this.userEmail]).then(res => {
          resolve(res);
        }).catch(e => {
          console.log(e);
          reject(e);
        });
      }).catch(e => {
        console.log(e);
        reject(e);
      });
    });
  }

  /**
   * Requests a use profile data from remote and then saves it into a local SQL DB. Returns a Promise.
   */

  saveUserLocally() {
    return new Promise((resolve, reject) => {
      this.remoteService.getUserProfile().then((user: any) => {
        this.addUser(user).then((res: any) => {
          resolve(res);
        }).catch(e => console.log(e));
      }).catch(e => {
        console.log(e);
        reject(e);
      });
    });
  }
}
