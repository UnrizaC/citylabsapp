import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map';

/*
  DB Provider Class
*/

@Injectable()
export class DatabaseProvider {

	options: any = {
		name: 'egle.db',
		location: 'default'
	};

	db: SQLiteObject;

	constructor(public http: HttpClient, private sqlite: SQLite, private platform: Platform, private toast: Toast) {
		console.log('Hello DatabaseProvider Provider');
		this.createDb();
	}

	createDb() {
		this.sqlite.create(this.options)
			.then((db: SQLiteObject) => {

				this.db = db;
				var sql = 'create table IF NOT EXISTS userprofiles (rowid INTEGER PRIMARY KEY, email TEXT, password TEXT, firstname TEXT, lastname TEXT, birthdate TEXT, gender TEXT, language TEXT, condition TEXT, phone TEXT, address TEXT)';
				//IF you move the below statment out of here then the db variable will not be initialized
				//before you can use it to execute the SQL. 
				this.db.executeSql(sql, [])
					.then(() => {
						this.toast.show('Executed SQL' + sql, '10000', 'center').subscribe(toast => {
							console.log('Executed SQL' + sql);
						});
					})
					.catch(e => {
						this.toast.show("Error SQL: " + JSON.stringify(e), '10000', 'center').subscribe(toast => {
							console.log("Error SQL: " + JSON.stringify(e));
						});
					});

				sql = 'CREATE TABLE IF NOT EXISTS entries (rowid INTEGER PRIMARY KEY AUTOINCREMENT, user TEXT, value TEXT, type TEXT, title TEXT, date TEXT, time TEXT, unit TEXT, profilepicture TEXT, comment TEXT)';
				this.db.executeSql(sql, []).then(() => {
					this.toast.show('Executed SQL' + sql, '5000', 'center').subscribe(toast => {
						console.log('Executed SQL' + sql);
					});
				}).catch(e => {
					this.toast.show("Error SQL: " + JSON.stringify(e), '5000', 'center').subscribe(toast => {
						console.log("Error SQL: " + JSON.stringify(e));
					});
				});
			})
			.catch(e => {
				this.toast.show("Error: " + JSON.stringify(e), '10000', 'center').subscribe(toast => {
					console.log("Error: " + JSON.stringify(e));
				});
			});
	}

	getDb() {
		return this.db;
	}
}
