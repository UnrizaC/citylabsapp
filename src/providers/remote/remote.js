var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
var RemoteProvider = /** @class */ (function () {
    function RemoteProvider(http) {
        this.http = http;
        this.egleUrl = 'https://beta.egle.be/api/';
        console.log('Hello RemoteProvider Provider');
    }
    RemoteProvider.prototype.getFetchTimestamp = function () {
        return this.lastFetchTimetamps;
    };
    RemoteProvider.prototype.returnToken = function () {
        return this.token;
    };
    RemoteProvider.prototype.getToken = function (email, password) {
        var _this = this;
        //console.log('GET TOKEN CALLED');
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.egleUrl + 'users/signin', { "email": email, "password": password }, { headers: new HttpHeaders().set('Content-Type', 'application/json') })
                .subscribe(function (res) {
                resolve(res);
                console.log(res);
                _this.token = res['token'];
            }, function (err) {
                reject(err);
                console.log(err.error);
                console.log(err.name);
                console.log(err.message);
                console.log(err.status);
            });
        });
    };
    RemoteProvider.prototype.getEntries = function (token) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.egleUrl + 'entries/type/glycaemia/subtype/undefined', {
                headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
            }).subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RemoteProvider.prototype.getGlycaemiaEntries = function (from, to) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.egleUrl + 'charts/newGly/from/' + from + '/to/' + to, {
                headers: new HttpHeaders().set('Authorization', 'Bearer ' + _this.token)
            }).subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RemoteProvider.prototype.postNewEntry = function (entry) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.egleUrl + 'entries', { "type": entry.valueType, "value": entry.value, "datetimeAcquisition": entry.acquisitionDate, "comments": entry.comment, "skipped": "false" }, {
                headers: new HttpHeaders().set('Authorization', 'Bearer ' + _this.token)
            }).subscribe(function (res) {
                resolve(res);
            }, function (err) {
                reject(err);
            });
        });
    };
    RemoteProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient])
    ], RemoteProvider);
    return RemoteProvider;
}());
export { RemoteProvider };
//# sourceMappingURL=remote.js.map