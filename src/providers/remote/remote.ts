import { Injectable } from '@angular/core';

import  { Storage } from '@ionic/storage';

import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class RemoteProvider {
	public egleUrl: string = '';
	public remoteUrl: string = 'https://beta.egle.be/api/';
	public localUrl: string = 'http://localhost:8100/api/';
	public token: any;
	private lastFetchTimetamps: any;
	private localwork = false;

	constructor(public http: HttpClient, private storage: Storage) {
		console.log('Hello RemoteProvider Provider');
		this.setUrl();
	}

	setUrl() {
		if (this.localwork) {
			this.egleUrl = this.localUrl;
		} else {
			this.egleUrl = this.remoteUrl;
		}
	}

	getFetchTimestamp() {
		return this.lastFetchTimetamps;
	}

	returnToken() {
		return this.token;
	}

	getToken(email, password) {
		//console.log('GET TOKEN CALLED');
		return new Promise((resolve, reject) => {
			this.http.post(this.egleUrl + 'users/signin', { "email": email, "password": password },
				{ headers: new HttpHeaders().set('Content-Type', 'application/json') })
				.subscribe(res => {
					resolve(res);
					console.log(res);
					this.token = res['token'];
					this.storage.set('jwtoken', res['token']);
				}, (err: HttpErrorResponse) => {
					reject(err);
					console.log(err.error);
					console.log(err.name);
					console.log(err.message);
					console.log(err.status);
				});
		});
	}

	getFilterEntries(type, from, to) {
		return new Promise((resolve, reject) => {
			this.http.get(this.egleUrl + 'entries/type/' + type + '/subtype/undefined/from/' + from + '/to/' + to, {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				reject(err);
			});
		});
	}

	getEntries() {
		return new Promise((resolve, reject) => {
			this.http.get(this.egleUrl + 'entries/type/glycaemia/subtype/undefined', {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				reject(err);
			});
		});
	}

	getGlycaemiaEntries(from, to) {
		return new Promise((resolve, reject) => {
			this.http.get(this.egleUrl + 'charts/newGly/from/' + from + '/to/' + to, {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				reject(err);
			});
		});
	}

	postNewEntry(entry) {
		var body = {
			"type": entry.valueType,
			"value": entry.value,
			"datetimeAcquisition": entry.acquisitionDate,
			"comments": entry.comment,
			"skipped": "false"
		};
		if (entry.subType !== undefined) { body["subType"] = entry.subType; }
		if (entry.values !== undefined) { body["values"] = entry.values; }

		return new Promise((resolve, reject) => {
			this.http.post(this.egleUrl + 'entries', body, {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				reject(err);
			});
		});
	}

	registerNewUser(user) {
		return new Promise((resolve, reject) => {
			this.http.post(this.egleUrl + 'users/signupa7', {
				"email": user.email, "username": user.firstname, "password": user.password, "condition": user.condition, "language": user.language
			}, {
					headers: new HttpHeaders().set('Content-Type', 'application/json')
				}).subscribe(res => {
					resolve(res);
				}, (err: HttpErrorResponse) => {
					console.log(err.error);
					console.log(err.name);
					console.log(err.message);
					console.log(err.status);
					reject(err);
				})
		});
	}

	getUserProfile() {
		return new Promise((resolve, reject) => {
			this.http.get(this.egleUrl + 'users/profile', {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				console.log(err.error);
				console.log(err.name);
				console.log(err.message);
				console.log(err.status);
				reject(err);
			})
		});
	}

	getGlycemiaStats(lowerb, upperb, timebound, hourlowerb, hourupperb) {
		return new Promise((resolve, reject) => {
			this.http.get(this.egleUrl + 'glystats/' + lowerb + '/' + upperb + '/' + timebound + '/' + hourlowerb + '/' + hourupperb, {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				console.log(err.error);
				console.log(err.name);
				console.log(err.message);
				console.log(err.status);
				reject(err);
			})
		});
	}

	getInsulinLogs(type, from, to) {
		return new Promise((resolve, reject) => {
			this.http.get(this.egleUrl + 'charts/' + type + '/from/' + from + '/to/' + to, {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				console.log(err.error);
				console.log(err.name);
				console.log(err.message);
				console.log(err.status);
				reject(err);
			})
		});
	}

	updateUserProfile(data) {
		return new Promise((resolve, reject) => {
			this.http.put(this.egleUrl + 'users' + data, {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				console.log(err.error);
				console.log(err.name);
				console.log(err.message);
				console.log(err.status);
				reject(err);
			})
		});
	}

	deleteEntry(id) {
		return new Promise((resolve, reject) => {
			this.http.delete(this.egleUrl + 'entries/' + id, {
				headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				console.log(err.error);
				console.log(err.name);
				console.log(err.message);
				console.log(err.status);
				reject(err);
			})
		});
	}

	lostPassword(reset) {
		return new Promise((resolve, reject) => {
			this.http.post(this.egleUrl + 'users/lostPassword', {
				"email": reset.email, "captcha": reset.captcha
			}).subscribe(res => {
				resolve(res);
			}, (err: HttpErrorResponse) => {
				console.log(err.error);
				console.log(err.name);
				console.log(err.message);
				console.log(err.status);
				reject(err);
			})
		});
	}

	changePassword(change) {
		return new Promise((resolve, reject) => {
			this.http.post(this.egleUrl + 'users/changePassword', {
				"oldPassword": change.oldPassword, "newPassword": change.newPassword, "newPasswordConfirmation": change.newPasswordConfirmation
			}, {
					headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.token)
				}).subscribe(res => {
					resolve(res);
				}, (err: HttpErrorResponse) => {
					console.log(err.error);
					console.log(err.name);
					console.log(err.message);
					console.log(err.status);
					reject(err);
				})
		});
	}

	exportChart(exportUrl, data) {
		//console.log('DATA TO SEND', data);
		return new Promise((resolve, reject) => {
			this.http.post(exportUrl, data, {
				headers: new HttpHeaders({
					'Content-Type': 'application/json'
				}), responseType: 'text'
				}).subscribe(res => {
					console.log(res);
					resolve(res);
				}, (err: HttpErrorResponse) => {
					console.log('ERR:',err);
					console.log(err.error);
					console.log(err.name);
					console.log(err.message);
					console.log(err.status);
					reject(err);
				});
			// this.http.post(exportUrl, data, function (data) {
			// 	var url = exportUrl + data;
			// 	window.open(url);
			// });
		});
	}

}
